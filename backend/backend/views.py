from .serializers import  UserSerializerWithToken, GetFullUserSerializer
from django.contrib.auth.models import User
from rest_framework import viewsets,views
from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework.permissions import AllowAny


class CreateUserView(views.APIView):
	permission_classes = (AllowAny, )
	def post(self,request):
		user = request.data.get('user')
		if not user:
			return Response({'response' : 'error', 'message' : 'No data found'})
		serializer = UserSerializerWithToken(data = user)
		if serializer.is_valid():
			saved_user = serializer.save()
		else:
			return Response({"response" : "error", "message" : serializer.errors})
		return Response({"response" : "success", "message" : "user created succesfully", "userId" :serializer.instance.id, "token":serializer.data['token']})