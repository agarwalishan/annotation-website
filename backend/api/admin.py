from django.contrib import admin
from .models import ModelSet, Version, ProblemSet, MasterRepo, Status
# Register your models here.

admin.site.register(Version)
admin.site.register(ModelSet)
admin.site.register(ProblemSet)
admin.site.register(MasterRepo)
admin.site.register(Status)
