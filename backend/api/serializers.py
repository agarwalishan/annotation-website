from rest_framework import serializers
from .models import(
	Version, ModelSet, ProblemSet, MasterRepo, Status
)
from rest_framework.fields import ListField
from .utils import Getparentpath, DOCUMENTS
import os
from django.conf import settings
import csv, json
class VersionSerializer(serializers.ModelSerializer):
	class Meta:
		model = Version
		fields = ('id', 'name', 'precision','recall','f1_score','training_data','model','has_score',
				'score_class_report','score_conf_matrix','docs_used_for_scoring','class_report','conf_matrix','validated_files','total_predicted_files')

class ModelSerializer(serializers.ModelSerializer):
	version = VersionSerializer(many=True, read_only=True)
	class Meta:
		model = ModelSet
		fields = ('id','name', 'desc','hyperparameters', 'version','problem')

	def create(self, data):
		instance = ModelSet.objects.create(**data)
		new_model_path = os.path.join(settings.UPLOAD_DIR + str(instance.problem.master.id), str(instance.problem.id), str(instance.id))
		os.mkdir(new_model_path)

		parameters = instance.hyperparameters
		with open(new_model_path + '/parameters.json', 'w') as fjson:
			json.dump(parameters, fjson)
			fjson.close()

		return instance


class ProblemSerializer(serializers.ModelSerializer):
	model = ModelSerializer(many=True, read_only=True)
	class Meta:
		model = ProblemSet
		fields = ('id', 'name', 'desc', 'labels','annotated_files_count','raw_files_count','used_annotated_files_count', 'model','master', 'documents_added')

	def create(self, data):
		instance = ProblemSet.objects.create(**data)
		new_problem_path = os.path.join(settings.UPLOAD_DIR, str(instance.master.id), str(instance.id))
		os.mkdir(new_problem_path)
		os.mkdir(os.path.join(new_problem_path, DOCUMENTS))

		labels = instance.labels
		new_labels = [[name, desc] for (name, desc)  in zip(labels["names"], labels['descriptions'])]
		with open(new_problem_path + '/list_containing_labels.csv', 'w', newline='\n') as fcsv:
			csvwriter = csv.writer(fcsv, delimiter=',')
			for label in new_labels:
				csvwriter.writerow(label)
		
		return instance

class MasterRepoSerializer(serializers.ModelSerializer):
	problem = ProblemSerializer(many=True, read_only=True)
	class Meta:
		model = MasterRepo
		fields = ('id', 'desc', 'documents_count', 'problem', 'user_id', 'isprocessrunning')

	

	def create(self, validated_data):
		instance = MasterRepo.objects.create(**validated_data)
		#Create master repo in upload folder and documents sub folder
		new_masterrepo_path = os.path.join(settings.UPLOAD_DIR, str(instance.id))
		os.mkdir(new_masterrepo_path)
		os.mkdir(os.path.join(new_masterrepo_path,DOCUMENTS))
		return instance

class StatusSerializer(serializers.ModelSerializer):
	class Meta:
		model = Status
		fields = ('processid', 'tasktype', 'taskstatus', 'starttime', 'endtime', 'message', 'mastername', 'problemname', 'modelname', 'versionname', 'user_id')