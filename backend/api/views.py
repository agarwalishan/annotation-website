from django.shortcuts import render
from rest_framework import viewsets
from .models import ModelSet, Version, ProblemSet, MasterRepo, Status
from .serializers import ( ModelSerializer, VersionSerializer, ProblemSerializer, 
	MasterRepoSerializer, StatusSerializer
)
# Create your views here.
from django.conf import settings
from django.http import HttpResponse
import os
import csv
import json
from pathlib import Path
from django.shortcuts import get_object_or_404, render
from .Scripts import score, no_of_raw_remaining_to_be_predicted, no_of_annotated_docs_present, copymechanism, export_documents_as_csv
from .Scripts import predict, train
from .utils import (Getparentpath, GetPredictionFilesFromVersionFolder, FAILED, SUCCESS, INPROGRESS, TRAINING, PREDICTION, SCORING,
getDocumentsFromMasterRepo, ImageCropper, DOCUMENTS)
from django.core.files.base import ContentFile
from django.core.files.images import ImageFile
from django.views.decorators.csrf import csrf_exempt
import datetime
import shutil
from rest_framework.permissions import IsAuthenticated,AllowAny
from django_filters.rest_framework import DjangoFilterBackend
import sys, traceback

class ModelViewSet(viewsets.ModelViewSet):
	permission_classes = (IsAuthenticated,)
	queryset = ModelSet.objects.all()
	serializer_class = ModelSerializer

class VersionViewSet(viewsets.ModelViewSet):
	permission_classes = (IsAuthenticated,)
	queryset = Version.objects.all()
	serializer_class = VersionSerializer

class ProblemViewSet(viewsets.ModelViewSet):
	permission_classes = (IsAuthenticated,)
	queryset = ProblemSet.objects.all()
	serializer_class = ProblemSerializer


class MasterRepoViewSet(viewsets.ModelViewSet):
	#Change the permission of this 
	permission_classes = (IsAuthenticated,)
	queryset = MasterRepo.objects.all()
	serializer_class = MasterRepoSerializer
	filter_backends = [DjangoFilterBackend]
	filterset_fields = ['user_id']
	

class StatusViewSet(viewsets.ModelViewSet):
	permission_classes = (IsAuthenticated,)
	queryset = Status.objects.all()
	serializer_class = StatusSerializer
	filter_backends = [DjangoFilterBackend]
	filterset_fields = ['user_id']

# To do scoring on all the validated documents for a given version
def Scoring(request):
	# create new status object
	status_obj = Status(tasktype=SCORING)
	status_obj.save()
	# To retrieve version id from GET request (will try to make it POST if possible later)
	versionid = request.GET['versionid']
	v_obj = Version.objects.get(id=versionid)

	#update status object
	status_obj.versionname = v_obj.name
	status_obj.modelname = v_obj.model.name
	status_obj.problemname = v_obj.model.problem.name
	#status_obj.mastername = v_obj.model.problem.master.name
	status_obj.user_id = v_obj.model.problem.master.user_id
	status_obj.save()

	modelid = v_obj.model.id
	problemid = v_obj.model.problem.id
	masterid = v_obj.model.problem.master.id
	cwd = settings.UPLOAD_DIR + str(masterid)

	response = {
		"success": INPROGRESS, 
		"message": ''
	}

	try:
		# call to score.py script
		score_result = score.main(cwd, str(problemid), str(modelid), v_obj.name)
		# It will return tuple which contains result at 0th position and if it's True Scores are calculated otherwise not
		if score_result[0] is None:
			print("ERROR:", score_result[1])
			response['success'] = FAILED
			response['message'] = 'This error should not be there'
		elif score_result[0] == False:
			print("SCORING NOT DONE AGAIN DUE TO: ", score_result[1])
			response['success'] = FAILED
			response['message'] = score_result[1]	
		else:
			v_obj = Version.objects.get(id=versionid)
			v_obj.has_score = True
			v_obj.score_class_report = score_result[1]
			# To retrieve image from buffer
			score_conf_matrix = ImageFile(score_result[2])
			score_conf_matrix_cropped = ImageCropper(score_conf_matrix)
			v_obj.score_conf_matrix.save('confusion_matrix.png', score_conf_matrix_cropped)
			v_obj.docs_used_for_scoring = score_result[3]
			v_obj.save()
			response['success'] = SUCCESS
			response['message'] = "Scoring is completed for {} files".format(score_result[3])
	except Exception as e:
		print("Some unexpected error during Scoring {}".format(e))
		response['success'] = FAILED
		response['message'] = "Sorry there is some problem in server"

	status_obj.taskstatus = response['success']
	status_obj.message = response['message']
	status_obj.endtime = datetime.datetime.now().time()
	status_obj.save()
	return HttpResponse(json.dumps(response))

# To check count of remaining raw files that can be predicted using given version (we will use database for this later)
def check_count_before_prediction(request):
	# To retrieve version id from GET request (will try to make it POST if possible later)
	versionid = request.GET['versionid']
	v_obj = Version.objects.get(id=versionid)

	modelid = v_obj.model.id
	problemid = v_obj.model.problem.id
	masterid = v_obj.model.problem.master.id
	cwd = settings.UPLOAD_DIR + str(masterid)
	# call to respective script
	count_of_raw = no_of_raw_remaining_to_be_predicted.main(cwd, str(problemid), str(modelid), v_obj.name)
	# For debugging
	response = count_of_raw[1]
	print(response)
	if count_of_raw[0] is None:
		print("ERROR:", count_of_raw[1])
		response = -1

	return HttpResponse(response)

# TO-DO return response of prediction
# To predict on raw files previously not predicted by given version based on count provided by user
def Prediction(request):
	# create new status object
	status_obj = Status(tasktype=PREDICTION)
	status_obj.save()

	no_of_files_specified_by_user = int(request.GET['count_raw'])
	versionid = request.GET['versionid']
	v_obj = Version.objects.get(id=versionid)
	
	#update status object
	status_obj.versionname = v_obj.name
	status_obj.modelname = v_obj.model.name
	status_obj.problemname = v_obj.model.problem.name
	#status_obj.mastername = v_obj.model.problem.master.name
	status_obj.user_id = v_obj.model.problem.master.user_id
	status_obj.save()

	master_obj = MasterRepo.objects.get(id=v_obj.model.problem.master.id)
	master_obj.isprocessrunning = True
	master_obj.save()

	modelid = v_obj.model.id
	problemid = v_obj.model.problem.id
	masterid = v_obj.model.problem.master.id
	cwd = settings.UPLOAD_DIR + str(masterid)
	
	response = {
		"success": INPROGRESS, 
		"message": ''
	}

	try:
		#call to predict.py script
		prediction_result = predict.main(cwd, str(problemid), str(modelid), v_obj.name, no_of_files_specified_by_user)
		if prediction_result[0] is None:
			print("ERROR:", prediction_result[1])
			response['success'] = FAILED
			response['message'] = 'This error should not be there'
		# This condition should not execute
		elif prediction_result[0] == False:
			print("PREDICTION NOT DONE DUE TO: ", prediction_result[1])
			response['success'] = FAILED
			response['message'] = 'This condition should not execute {}'.prediction_result[1]	
		else:
			print("FILE NAMES WITH CONFIDENCE RATIOS THAT ARE PREDICTED: ", prediction_result[1])
			# To update total predicted files by current version
			v_obj = Version.objects.get(id=versionid)
			filesPredictedList = v_obj.total_predicted_files['files']
			filesPredictedList.extend(prediction_result[1])
			v_obj.total_predicted_files['files'] = filesPredictedList
			v_obj.save()
			response['success'] = SUCCESS
			response['message'] = 'Prediction has been done for {} files'.format(len(prediction_result[1]))
			# To export predictions as csv file
			try:
				csv_result = export_documents_as_csv.main(cwd, str(problemid), modelid=str(modelid), actualmodelname=v_obj.model.name ,versionname=v_obj.name, tasktype=PREDICTION)
			except Exception as csverror:
				print("some error in making csv {}".format(csverror))
	except Exception as e:
		print("Some unexpected error during Prediction {}".format(e))
		response['success'] = FAILED
		response['message'] = "Sorry there is some problem in server"

	status_obj.taskstatus = response['success']
	status_obj.message = response['message']
	status_obj.endtime = datetime.datetime.now().time()
	status_obj.save()

	master_obj.isprocessrunning = False
	master_obj.save()
	return HttpResponse(json.dumps(response))

# TO-DO return response of training
# To add a new version only if no. of annotated documents is more than max of training_data used for previous versions
# and Atleast 5 annotation files are present if specified model has no versions previously
def Training(request):
	THRESHOLD_ANNOTATED_FILES = 5
	# create new status object
	status_obj = Status(tasktype=TRAINING)
	status_obj.save()

	modelid = request.GET['modelid']
	m_obj = ModelSet.objects.get(id=modelid)
	
	#update status object
	status_obj.modelname = m_obj.name
	status_obj.problemname = m_obj.problem.name
	#status_obj.mastername = m_obj.problem.master.name
	status_obj.user_id = m_obj.problem.master.user_id
	status_obj.save()

	master_obj = MasterRepo.objects.get(id=m_obj.problem.master.id)
	master_obj.isprocessrunning = True
	master_obj.save()
	
	problemid = m_obj.problem.id
	masterid = m_obj.problem.master.id

	# Fetch versions array to check maximum training data used
	versions = Version.objects.filter(model=modelid)
	cwd = settings.UPLOAD_DIR + str(masterid)
	# Fetch how many annotated docs are currently present in specified model's problemrepo (we will use database for this later)
	no_of_annotated_docs = no_of_annotated_docs_present.main(cwd, str(problemid))

	max_training_data = -1
	for version in versions:
		max_training_data = max(max_training_data, version.training_data)

	response = {
		"success": INPROGRESS, 
		"message": ''
	}

	# To check if current no. of annotated docs are equal to any previous version's training_data 
	if max_training_data != -1 and max_training_data == no_of_annotated_docs:
		response['success'] = FAILED
		response['message'] = "{} files are already trained with previous versions. Annotate more files to train a new version.".format(max_training_data)
	# To prevent creating new version if current annotation doc less than threshold no. of annotated files
	elif max_training_data == -1 and no_of_annotated_docs < THRESHOLD_ANNOTATED_FILES:
		response['success'] = FAILED
		response['message'] = "Annotate atleast {} more files before training a new version".format(THRESHOLD_ANNOTATED_FILES - no_of_annotated_docs)
	else:
		m_obj = ModelSet.objects.get(id=modelid)
		noofexistingversions = len(Version.objects.filter(model=modelid)) + 1
		NAME = 'Version' + str(noofexistingversions)
		#newversion = m_obj.version.create(name=NAME)
		newversion = Version(name=NAME, model=m_obj)		
		try:
			os.mkdir(cwd + '/' + str(problemid) + '/' + str(modelid) + '/' + newversion.name)
			# run train.py script for final training
			training_result = train.main(cwd, str(problemid), str(modelid), newversion.name)
			if training_result[0] is None:
				print("ERROR", training_result[1])
				response['success'] = FAILED
				response['message'] = 'This error should not be there {}'.format(training_result[1])
			# This condition should not execute
			elif training_result[0] == False:
				print("TRAINING NOT DONE DUE TO: ", training_result[1])
				response['success'] = FAILED
				response['message'] = 'This condition should not execute {}'.format(training_result[1])
			# if True it returns trainingdata used, classification report, confusion matrix and aggregate measures 
			else:
				print("Training script ran successfully")
				newversion.training_data = training_result[1]
				newversion.precision = training_result[4]["precision"]
				newversion.recall = training_result[4]["recall"]
				newversion.f1_score = training_result[4]["f1_score"]
				newversion.class_report = training_result[2]
				conf_matrix = ImageFile(training_result[3])
				conf_matrix_cropped = ImageCropper(conf_matrix)
				newversion.conf_matrix.save('confusion_matrix.png', conf_matrix_cropped)
				# To update used annotated files in problemrepo
				newversion.model.problem.used_annotated_files_count = training_result[1]
				newversion.save()
				response['success'] = SUCCESS
				response['message'] = "New version is trained on {} documents".format(training_result[1])
		except:
			exc_type, exc_value, exc_traceback = sys.exc_info()
			lines = traceback.format_exception(exc_type, exc_value, exc_traceback)
			e = ''.join('!! ' + line for line in lines)
			print("Some unexpected error during training:\n {}".format(e))
			if os.path.exists(cwd + '/' + str(problemid) + '/' + str(modelid) + '/' + newversion.name):
				shutil.rmtree(cwd + '/' + str(problemid) + '/' + str(modelid) + '/' + newversion.name)
			response['success'] = FAILED
			response['message'] = "Sorry there is some problem in server"
	
	status_obj.taskstatus = response['success']
	status_obj.message = response['message']
	status_obj.endtime = datetime.datetime.now().time()
	status_obj.save()

	master_obj.isprocessrunning = False
	master_obj.save()
	print(response)
	return HttpResponse(json.dumps(response))


def getPredictionFilesByVersionId(request, verId):
	# To retrieve version id from GET request (will try to make it POST if possible later)
	version = Version.objects.get(id=verId)
	problemid = version.model.problem.id
	masterid = version.model.problem.master.id
	versionFolderPath = settings.UPLOAD_DIR + str(masterid) + '/' + str(problemid) + '/' + str(version.model.id) + '/' + version.name
	response = GetPredictionFilesFromVersionFolder(versionFolderPath, version.validated_files['files'], version.total_predicted_files["files"])
	return HttpResponse(json.dumps(response), content_type="application/json")

# This method is used for handling file
@csrf_exempt
def FileHandling(request):
	fileData = json.loads(request.body)
	filePath = fileData["filePath"]
	probId = fileData["probId"]
	doexport = fileData["doexport"]
	response = ""
	if request.method == "POST":
		fileContent = fileData["fileContent"]
		f = open(filePath, "w", encoding='utf-8')
		f.write(fileContent)
		f.close()
		response = "File is saved Successfully"

	elif request.method =="DELETE":
		if os.path.exists(filePath):
			os.remove(filePath)
			response = "File is deletd Successfully"
			if filePath.find('raw.json'):
				problemSet = ProblemSet.objects.get(id=probId)
				problemSet.annotated_files_count += 1
				problemSet.raw_files_count -=1
				problemSet.save()
		else:
			response = "The file does not exist"

	# [Ishan] I added one more argument in request.data which can tell when to update csv file
	# It will be true only when we post a completely annotated document
	if doexport:
		try:
			p_obj = ProblemSet.objects.get(id=probId)
			problemid = probId
			masterid = p_obj.master.id
			cwd = settings.UPLOAD_DIR + str(masterid)
			csv_result = export_documents_as_csv.main(cwd, str(problemid), tasktype='annotate')
		except Exception as csverror:
			print("some error in making csv {}".format(csverror))

	return HttpResponse(response)


# After file is validated the filename is saved in the version.validated_files column and its annotated file which is it's ground truth file is saved in grand
# paent directory
@csrf_exempt
def validatePredictionFile(request):
	fileData = json.loads(request.body)
	filePath = fileData["filePath"]
	fileContent = fileData["fileContent"]
	versionId = fileData["versionId"]
	probId = fileData["probId"]

	#Add this filename to validated files
	fileName = Path(filePath)
	v_obj = Version.objects.get(id=versionId)
	filesValidatedList = v_obj.validated_files['files']
	filesValidatedList.append(fileName.name)
	v_obj.validated_files['files'] = list(set(filesValidatedList))
	v_obj.save()

	#After file is validated it is saved in grandparent directory and suffix  is changed from predictions.json -> annotated.json
	annotatedFileDir = os.path.join(fileName.parent.parent.parent, 'documents')
	annotatedFileName = fileName.name
	annotatedFileName = annotatedFileName.replace('predictions.json','annotated.json')
	annotatedFilePath = os.path.join(annotatedFileDir, annotatedFileName)
	f = open(annotatedFilePath, "w", encoding='utf-8')
	f.write(fileContent)
	f.close()

	#Delete the raw.json file since its corresponding annotated file is created
	rawFilePath = annotatedFilePath.replace('annotated.json','raw.json')
	if os.path.exists(rawFilePath):
		os.remove(rawFilePath)
		response = "File is deletd Successfully"
		problemSet = ProblemSet.objects.get(id=probId)
		problemSet.annotated_files_count += 1
		problemSet.raw_files_count -=1
		problemSet.save()
	else:
		print("Raw file does not exist", rawFilePath)

	# After validation as new annotated file is created so we need to update csv file again.
	try:
		problemid = v_obj.model.problem.id
		masterid = v_obj.model.problem.master.id
		cwd = settings.UPLOAD_DIR + str(masterid)
		csv_result = export_documents_as_csv.main(cwd, str(problemid), tasktype='annotate')
	except Exception as csverror:
		print("some error in making csv {}".format(csverror))	

	response = "File is saved Successfully"
	return HttpResponse(response)

# Returns path of all the documents in the JSON form for rendering. Already copied files will be excluded from the response.
def GetDocumentsFromMasterRepo(request, probId):
	# To retrieve version id from GET request (will try to make it POST if possible later)
	problemObj = ProblemSet.objects.get(id=probId)
	masterRepo = problemObj.master
	documentsBaseUrl = settings.UPLOAD_DIR + str(masterRepo.id) + '/documents'
	response = getDocumentsFromMasterRepo(documentsBaseUrl,problemObj.documents_added)
	return HttpResponse(json.dumps(response),content_type="application/json")


@csrf_exempt
def MoveFilesFromMasterToProblem(request):
	fileData = json.loads(request.body)
	probId = fileData["probId"]
	filesToMove = fileData["files"]
	problemObj = ProblemSet.objects.get(id=probId)
	problemObj.documents_added.extend(list(set(filesToMove)))
	problemObj.save()
	#ISHAN add your script HERE
	cwd = settings.UPLOAD_DIR
	masterid = problemObj.master.id
	copy_result = copymechanism.main(cwd, filesToMove, str(probId), str(masterid))
	if copy_result[0] is None:
		print("This error should not be there")
	else:
		print("Files converted successfully:",copy_result[1])
		problemObj.raw_files_count+= copy_result[1]
		problemObj.save()
		
	return HttpResponse()

# To add more labels to a problem
def Addnewlabels(request):
	problemid = request.GET['problemid']
	p_obj = ProblemSet.objects.get(id=problemid)
	masterid = p_obj.master.id
	problem_path = settings.UPLOAD_DIR + str(masterid) + '/' +  str(problemid)
	
	labels = p_obj.labels
	new_labels = [[name, desc] for (name, desc)  in zip(labels["names"], labels['descriptions'])]

	with open(problem_path + '/list_containing_labels.csv', 'w', newline='\n') as fcsv:
		csvwriter = csv.writer(fcsv)
		for label in new_labels:
			csvwriter.writerow(label)
	# for debug
	print("Upload dir:", settings.UPLOAD_DIR)
	return HttpResponse()