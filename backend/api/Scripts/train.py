# Returns no of docs used for training, classification report & confusion matrix of testing results and aggregate results
#import subprocess
#import tensorflow
import argparse
import json
import os
from . import testing_results
from .import make_train_data
from .import bert_train_function
from ..utils import DOCUMENTS
from django.conf import settings

max_seq_length = 512
train_batch_size = 6
learning_rate = 2e-5
num_train_epochs = 1
CRF = False
warmup_proportion = 0.1

def main(cwd, reponame, modelname, versionname):
	init_model_dir = settings.BASE_DIR + '/api/Scripts/model/'
	path = cwd + '/' + reponame +'/'+ modelname + '/' + versionname
	path_to_labels = cwd + '/' + reponame + '/list_containing_labels.csv'
	path_to_parameters = cwd + '/' + reponame + '/' + modelname + '/' + 'parameters.json'
	inpath = cwd + '/' + reponame + '/' + DOCUMENTS

	if not os.path.exists(inpath):
		#print("Invalid path {}".format(inpath))
		return (None, "Invalid path {}".format(inpath))

	if not os.path.exists(path_to_labels):
		#print("File does not exist {}".format(path_to_labels))
		return (None, "File does not exist {}".format(path_to_labels))

	if os.stat(path_to_labels).st_size == 0:
		#print('Empty file {}'.format(path_to_labels))
		return (None, 'Empty file {}'.format(path_to_labels))

	Condition = make_train_data.make_data(cwd, reponame, modelname, versionname)
	# This means either error or no files to be trained
	if Condition[0] != True:
		return Condition

	DATA = Condition[1]
	NO_OF_DOCS_USED_FOR_TRAINING = Condition[2]

	print("DATA is prepared")

	labels_dir = cwd + '/' + reponame

	
	if not os.path.exists(path+'/output_model'):
		os.mkdir(path+'/output_model')
	if not os.path.exists(path+'/output_model/middle_output'):
		os.mkdir(path+'/output_model/middle_output')
	if not os.path.exists(path+'/output_train'):
		os.mkdir(path+'/output_train')

	model_output_dir = path + '/output_model'
	middle_output = path + '/output_model/middle_output'
	train_output_dir = path + '/output_train'


	if os.path.exists(path_to_parameters) and os.stat(path_to_parameters).st_size != 0:
		with open(path_to_parameters, 'r', encoding='utf-8') as fjson:
			parameters = json.load(fjson)

	max_seq_length = parameters['max_seq_length']
	train_batch_size = parameters['train_batch_size']
	learning_rate = parameters['learning_rate']
	num_train_epochs = parameters['num_train_epochs']
	#CRF = parameters['crf']
	warmup_proportion = parameters['warmup_proportion']
	
	print("Run train function(BERT)")
	
	bert_train_function.runfunc(Data_dir=DATA, Bert_config_file= init_model_dir+'bert_config.json', Task_name='NER', 
	Vocab_file= init_model_dir+'vocab.txt',
	Model_output_dir=model_output_dir, Train_output_dir=train_output_dir, Labels_dir=labels_dir,
	Init_checkpoint= init_model_dir+'bert_model.ckpt', Do_lower_case=False, Max_seq_length=max_seq_length, Do_train=True, 
	Do_eval=True,
	Do_predict=True, Train_batch_size=train_batch_size, Eval_batch_size=8, Predict_batch_size=8, Learning_rate=learning_rate, 
	Num_train_epochs=num_train_epochs, Warmup_proportion=warmup_proportion, Save_checkpoints_steps=5000, Iterations_per_loop=1000,
	Use_tpu=False, Tpu_name=None, Gcp_project=None, Master=None, Num_tpu_cores=8, Middle_output=middle_output, Crf=CRF)
	
	print("Train function finished(BERT)")
	
	TRAINING_RESULTS = testing_results.compute_f1score(cwd, reponame, modelname, versionname)

	return (True, NO_OF_DOCS_USED_FOR_TRAINING, TRAINING_RESULTS[0], TRAINING_RESULTS[1], TRAINING_RESULTS[2])
# main()
