#!/usr/bin/env python
# coding: utf-8

# In[ ]:

import os
import pandas as pd
import numpy as np
import csv
from sklearn.metrics import confusion_matrix, classification_report
from sklearn.metrics import precision_recall_fscore_support
from io import StringIO, BytesIO
import json
import matplotlib.pyplot as plt
plt.switch_backend('agg')
import numpy as np
import itertools

ImageFormat = 'png'
# In[ ]:

def make_classification_report_if_exception(labels):
    class_report = {}
    entry = {'precision': 0.0, 'recall': 0.0, 'f1-score': 0.0, 'support': 0}

    for label in labels:
        class_report[label] = entry

    extra_columns = ['micro avg', 'macro avg', 'weighted avg']
    for column in extra_columns:
        class_report[column] = entry

    return class_report
           

def read_labels_file(path):
    temp = []
    with open(path+'/list_containing_labels.csv') as filep:
        csvreader = csv.reader(filep)
        for row in csvreader:
            temp.append(str(row[0].strip()))
    return temp        


# In[ ]:


def plot_confusion_matrix(cm,
                          target_names,
                          save_results_path,
                          title='Confusion matrix',
                          cmap=None,
                          normalize=False):
  
    #import matplotlib.pyplot as plt
    #import numpy as np
    #import itertools

    accuracy = np.trace(cm) / np.sum(cm).astype('float')
    misclass = 1 - accuracy

    if cmap is None:
        cmap = plt.get_cmap('Blues')

    plt.figure(figsize=(15, 8))
    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    #plt.colorbar()

    if target_names is not None:
        tick_marks = np.arange(len(target_names))
        plt.xticks(tick_marks, target_names, rotation=45)
        plt.yticks(tick_marks, target_names)

    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]


    thresh = cm.max() / 1.5 if normalize else cm.max() / 2
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        if normalize:
            plt.text(j, i, "{:0.4f}".format(cm[i, j]),
                     horizontalalignment="center",
                     color="white" if cm[i, j] > thresh else "black")
        else:
            plt.text(j, i, "{:,}".format(cm[i, j]),
                     horizontalalignment="center",
                     color="white" if cm[i, j] > thresh else "black")


    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label\naccuracy={:0.4f}; misclass={:0.4f}'.format(accuracy, misclass))
    plt.savefig(save_results_path+'/confusion_matrix.jpg')
    fig = BytesIO()
    plt.savefig(fig, format=ImageFormat)
    return fig


# In[ ]:


def remove_other_and_SEP(actual_label, predicted_label):
    input_array = np.array(actual_label)
    predict_array = np.array(predicted_label)
    print(len(input_array),len(predict_array))
    new_input_array = []
    new_predict_array = []
    for i in range(len(input_array)):
        if input_array[i]=='O' and predict_array[i]=='O':
            continue
        if input_array[i]=='[SEP]':
            continue  
        new_input_array.append(input_array[i])
        new_predict_array.append(predict_array[i])
    print(len(new_input_array),len(new_predict_array))    
    return new_input_array, new_predict_array


# In[ ]:


def remove_prefix_suffix(data):

    text = []
    actual_label = []
    predicted_label = []

    flag = 0
    for i in range(len(data[0])):
        if flag==1 and data[0].iloc[i] == '[SEP]':
            text.append(data[0].iloc[i])
            actual_label.append(data[1].iloc[i])
            predicted_label.append(data[2].iloc[i])
            flag = 0
        elif flag==0 and data[0].iloc[i] == '[SEP]':
            flag = 1
        elif flag == 1:
            text.append(data[0].iloc[i])
            actual_label.append(data[1].iloc[i])
            predicted_label.append(data[2].iloc[i])
    
    return text, actual_label, predicted_label

# In[ ]:


def compute_f1score(cwd, reponame, modelname, versionname):
    
    path = cwd + '/' + reponame + '/' + modelname + '/' + versionname
    data = pd.read_csv(path + '/output_train/label_test.txt', header=None, sep='\t', keep_default_na=False)
    text, actual_label, predicted_label = remove_prefix_suffix(data)
    y_true, y_pred = remove_other_and_SEP(actual_label, predicted_label)
    
    labels = read_labels_file(cwd + '/' + reponame)
    labels.append('O')
    
    save_results_path = path + '/output_train'
    
    conf_matrix = confusion_matrix(y_true, y_pred, labels = labels)
    confusion_fig = plot_confusion_matrix(conf_matrix, labels, save_results_path)
    
    temp = labels.pop()
    
    #class_report = classification_report(y_true, y_pred, labels = labels)
    #print(class_report)
    try:
        class_report = classification_report(y_true, y_pred, labels = labels, output_dict=True)
    except:
        class_report = make_classification_report_if_exception(labels)
        
    df_report = pd.DataFrame(class_report).transpose()

    report_json = df_report.to_json(orient='split')
    report_json = json.loads(report_json)

    # Aggregate measures that we show on tables
    aggregate_measures = { 'precision': 0, 'recall':0, 'f1_score': 0 }
    micro_average = report_json['data'][-3]
    # -3 because report_json has key:data which stores micro average scores at last 3rd position
    aggregate_measures['precision'] = micro_average[0]
    aggregate_measures['recall'] = micro_average[1]
    aggregate_measures['f1_score'] = micro_average[2]

    df_report.to_csv(save_results_path + '/classification_report.csv', header=True)

    return (report_json, confusion_fig, aggregate_measures)
