import os
import json
import pandas as pd
from sklearn.metrics import precision_recall_fscore_support

results_filename = 'scoring_results.csv'

def update_results_file(cwd, reponame, modelname, versionname, count_doc, y_true, y_pred, labels, curr_time):
    
    path_to_results_file = cwd + '/' + reponame + '/' + results_filename
    
    path_to_parameters = cwd + '/' + reponame + '/' + modelname + '/' + 'parameters.json'
    if not os.path.exists(path_to_parameters):
        print("Invalid path to parameters file {}".format(path_to_parameters))
        return 
    else:
        with open(path_to_parameters, 'r', encoding='utf-8') as fjson:
            parameters = json.load(fjson)

    path_to_docs_used_for_training = cwd + '/' + reponame + '/' + modelname + '/' + versionname + '/' + '/data_train/noofdocsused.txt'
    if not os.path.exists(path_to_docs_used_for_training):
        print("Invalid path to file containing no. of documnets used for training {}".format(path_to_docs_used_for_training))
        return
    else:
        with open(path_to_docs_used_for_training , 'r', encoding='utf-8') as fr:
            train_docs_used = fr.readline().strip()
            fr.close()

    ATTRIBUTES = [key.upper() for key, value in parameters.items()]
    COLUMNS = ['MODELNAME', 'VERSIONNAME', 'NO_OF_DOCUMENTS_USED_FOR_TRAINING']
    COLUMNS.extend(ATTRIBUTES)
    COLUMNS.extend(['NO_OF_DOCUMENTS_USED_FOR_SCORING', 'TIME', 'PRECISION', 'RECALL', 'F1-SCORE'])

    if os.path.exists(path_to_results_file):
        results = pd.read_csv(path_to_results_file, sep=',', header=0)
    else:
        results = pd.DataFrame(columns = COLUMNS)

    measures = precision_recall_fscore_support(y_true, y_pred, average='micro', labels = labels)

    new_row = {}
    new_row['MODELNAME'] = modelname
    new_row['VERSIONNAME'] = versionname
    new_row['NO_OF_DOCUMENTS_USED_FOR_TRAINING'] = train_docs_used
    for key, value in parameters.items():
    	new_row[key.upper()] = value
    new_row['NO_OF_DOCUMENTS_USED_FOR_SCORING'] = str(count_doc)
    new_row['TIME'] = curr_time
    new_row['PRECISION'], new_row['RECALL'], new_row['F1-SCORE'] = measures[0], measures[1], measures[2]	

    results = results.append(new_row, ignore_index=True)
    results.to_csv(path_to_results_file, sep=',', header=True, index=False)

    return True