import os

from ..utils import DOCUMENTS

def tobeused_raw_documents(inpath, outpath):
    raw_docname_list = []
    annotated_docname_list = []
    
    filesinpath = next(os.walk(inpath))[2]
    for filename in sorted(filesinpath, key=str.lower, reverse=False):
            if filename.endswith('.annotated.json'):
                annotated_docname_list.append(filename)
            if filename.endswith('.raw.json'):
                raw_docname_list.append(filename)
    
    if len(raw_docname_list) == 0:
        return []
    
    new_raw_docname_list = []
    for i in range(len(raw_docname_list)):
        filename = raw_docname_list[i]
        annotated_filename = filename[0:filename.rfind('.raw.json')] + '.annotated.json'
        if annotated_filename not in annotated_docname_list:
            new_raw_docname_list.append(filename)
    
    if len(new_raw_docname_list) == 0:
        return []
    
    predicted_docname_list = []        
    filesoutpath = next(os.walk(outpath))[2]
    for filename in sorted(filesoutpath, key=str.lower, reverse=False):
        if filename.endswith('.predictions.json'):
            predicted_docname_list.append(filename)
            
    if len(predicted_docname_list) == 0:
        return new_raw_docname_list
    
    unpredicted_raw_docname_list = []
    for i in range(len(new_raw_docname_list)):
        filename = new_raw_docname_list[i]
        predicted_filename = filename[0:filename.rfind('.raw.json')] + '.predictions.json'
        if predicted_filename not in predicted_docname_list:
            unpredicted_raw_docname_list.append(filename)
    #print(unpredicted_raw_docname_list)
    return unpredicted_raw_docname_list


def main(cwd, reponame, modelname, versionname):
    inpath = cwd + '/' + reponame + '/' + DOCUMENTS
    if not os.path.exists(inpath):
        #print("Invalid path {}".format(inpath))
        return (None, "Invalid path {}".format(inpath))
    
    outpath = cwd + '/'+ reponame +'/' + modelname + '/' + versionname
    if not os.path.exists(outpath):
        #print("Invalid path {}".format(outpath))
        return (None, "Invalid path {}".format(outpath))
    
    raw_docname_list = tobeused_raw_documents(inpath, outpath)
    print(raw_docname_list)
    
    return (True, len(raw_docname_list))
