#!/usr/bin/env python
# coding: utf-8
from io import StringIO, BytesIO
import os
import json
import re
import sys 
import argparse
import copy
import pickle
import csv
from datetime import datetime
import random
import numpy as np
import pandas as pd
from sklearn.metrics import confusion_matrix, classification_report
from sklearn.metrics import precision_recall_fscore_support

from ..Scripts import make_results_file
from ..Scripts import text_to_word_level
from ..utils import DOCUMENTS

import matplotlib.pyplot as plt
plt.switch_backend('agg')
import numpy as np
import itertools

ImageFormat = 'png'

def make_classification_report_if_exception(labels):
    class_report = {}
    entry = {'precision': 0.0, 'recall': 0.0, 'f1-score': 0.0, 'support': 0}

    for label in labels:
        class_report[label] = entry

    extra_columns = ['micro avg', 'macro avg', 'weighted avg']
    for column in extra_columns:
        class_report[column] = entry

    return class_report
           

def generate_current_time():
    now = datetime.now()
    now_str = now.strftime("%d-%m-%Y--%H-%M-%S")
    return now_str


## may be used later
# def generate_unique_string(length):
#     password_length = length
#     possible_characters = "abcdefghijklmnopqrstuvwxyz1234567890"
#     random_character_list = [random.choice(possible_characters) for i in range(password_length)]
#     random_password = "".join(random_character_list)
#     return random_password


def read_labels_file(path):
    temp = []
    with open(path+'/list_containing_labels.csv') as filep:
        csvreader = csv.reader(filep)
        for row in csvreader:
            temp.append(str(row[0].strip()))
    return temp


def plot_confusion_matrix(cm,
                          target_names,
                          save_results_path,
                          title='Confusion matrix',
                          cmap=None,
                          normalize=False):
  
    #import matplotlib.pyplot as plt
    #import numpy as np
    #import itertools

    accuracy = np.trace(cm) / np.sum(cm).astype('float')
    misclass = 1 - accuracy

    if cmap is None:
        cmap = plt.get_cmap('Blues')

    plt.figure(figsize=(15, 8))
    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    #plt.colorbar()

    if target_names is not None:
        tick_marks = np.arange(len(target_names))
        plt.xticks(tick_marks, target_names, rotation=45)
        plt.yticks(tick_marks, target_names)

    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]


    thresh = cm.max() / 1.5 if normalize else cm.max() / 2
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        if normalize:
            plt.text(j, i, "{:0.4f}".format(cm[i, j]),
                     horizontalalignment="center",
                     color="white" if cm[i, j] > thresh else "black")
        else:
            plt.text(j, i, "{:,}".format(cm[i, j]),
                     horizontalalignment="center",
                     color="white" if cm[i, j] > thresh else "black")


    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label\naccuracy={:0.4f}; misclass={:0.4f}'.format(accuracy, misclass))
    plt.savefig(save_results_path+'/confusion_matrix.jpg')
    #plt.show()
    fig = BytesIO()
    plt.savefig(fig, format=ImageFormat)
    return fig


def remove_other_and_SEP(actual_label, predicted_label):
    input_array = np.array(actual_label)
    predict_array = np.array(predicted_label)
    #print(len(input_array),len(predict_array))
    new_input_array = []
    new_predict_array = []
    for i in range(len(input_array)):
        if input_array[i]=='O' and predict_array[i]=='O':
            continue
        if input_array[i]=='[SEP]':
            continue  
        new_input_array.append(input_array[i])
        new_predict_array.append(predict_array[i])
    #print(len(new_input_array),len(new_predict_array))    
    return new_input_array, new_predict_array


def combine_annotated_and_predicted_sentence(TEXTA, ACTUAL_LABELS, TEXTB, PREDICTED_LABELS):
    #text = []
    actual = []
    predicted = []
    a = 0
    b = 0
    
    while b < len(TEXTB):
        texta = TEXTA[a]
        labela = ACTUAL_LABELS[a]
        textb = TEXTB[b]
        labelb = PREDICTED_LABELS[b]
        
        if textb == '[SEP]':
            if texta != '[SEP]':
                while texta != '[SEP]':
                    a = a + 1
                    texta = TEXTA[a]
                    labela = ACTUAL_LABELS[a]
            #text.append(texta)
            actual.append(labela)
            predicted.append(labelb)
            a = a + 1
            b = b + 1
        else:
            if texta!=textb and textb.find(texta) != 0:
                a = a + 1
            else:
                #text.append(texta)
                actual.append(labela)
                predicted.append(labelb)
                a = a + 1
                b = b + 1
                
    return actual, predicted            


def sort_l(sub_li):
    return(sorted(sub_li, key = lambda x: x[0]))

def sort_labels(t_l):
    for i in range(len(t_l)):
        t_l[i]['labels'] = sort_l(t_l[i]['labels'])
    return t_l


def extract_text_labels(data):
    text_labels = []
    for i in range(len(data)):
        dic = {}
        dic['text'] = data[i]['text']
        dic['labels'] = data[i]['labels']
        text_labels.append(dic)
    return text_labels


def read_json_file(docname_list, path):
    data = []
    for i in range(len(docname_list)):
        filename = docname_list[i]
        print('filename used for scoring now [for debug]',filename)
        # for line in open(path + '/' + filename, 'r',encoding='utf-8'):
        #     print(line)
        #     j = json.loads(line)
        #     print(j)
        json_list = [json.loads(line) for line in open(path + '/' + filename, 'r',encoding='utf-8')]
        data.extend(json_list)
        json_list = []
    return data    


def documents_to_be_used(path_to_annotated, path_to_predicted):
    predicted_docname_list = []
    
    predictedfiles = next(os.walk(path_to_predicted))[2]
    for filename in sorted(predictedfiles, key=str.lower, reverse=False):
        if filename.endswith('.predictions.json'):
            predicted_docname_list.append(filename)
    
    if len(predicted_docname_list) == 0:
        return [], []
    
    annotated_docname_list = []
    annotatedfiles = next(os.walk(path_to_annotated))[2]
    for filename in sorted(annotatedfiles, key=str.lower, reverse=False):
        if filename.endswith('.annotated.json'):
            annotated_docname_list.append(filename)
    
    final_predicted_docname_list = []
    final_annotated_docname_list = []
    for i in range(len(predicted_docname_list)):
        filename = predicted_docname_list[i]
        annotated_filename = filename[0:filename.rfind('.predictions.json')] + '.annotated.json'
        if annotated_filename in annotated_docname_list:
            final_annotated_docname_list.append(annotated_filename)
            final_predicted_docname_list.append(filename)
            
    if len(final_annotated_docname_list) == 0:
        return [], predicted_docname_list
    
    return sorted(final_annotated_docname_list, key=str.lower, reverse=False), sorted(final_predicted_docname_list, key=str.lower, reverse=False)        


def main(cwd, reponame, modelname, versionname):
    path_to_annotated = cwd + '/' + reponame + '/' + DOCUMENTS
    if not os.path.exists(path_to_annotated):
        #print("Invalid path {}".format(path_to_annotated))
        return (None,"Invalid path {}".format(path_to_annotated))
    
    path_to_predicted = cwd + '/'+ reponame +'/' + modelname + '/' + versionname
    if not os.path.exists(path_to_predicted):
        #print("Invalid path {}".format(path_to_predicted))
        return (None,"Invalid path {}".format(path_to_predicted))
    
    already_scored_count = '0'
    if not os.path.exists(path_to_predicted+'/alreadyscoredcount.txt'): 
        with open(path_to_predicted+'/alreadyscoredcount.txt','w') as f:
            f.write('0')
            f.close()
    else:
        if os.stat(path_to_predicted+'/alreadyscoredcount.txt').st_size != 0:
            with open(path_to_predicted+'/alreadyscoredcount.txt','r') as f:
                already_scored_count = f.readline().strip() 
                f.close()
    
    annotated_docname_list, predicted_docname_list = documents_to_be_used(path_to_annotated, path_to_predicted)
    
    if len(annotated_docname_list) == 0 and len(predicted_docname_list) == 0:
        #print("No new predictions files to be scored")
        return (False, "No new predictions files to be scored")
    if len(annotated_docname_list) == 0 and len(predicted_docname_list) != 0:
        #print("Respective annotation files not present")
        return (False, "Respective annotation files not present")
    
    #scored_docname_list.extend(predicted_docname_list)
    count_doc = len(predicted_docname_list)
    if count_doc <= int(already_scored_count):
    	#print("Scores are already calculated for {} files".format(count_doc))
    	return (False, "Scores are already calculated for {} files. You must annotate more".format(count_doc))

    already_scored_count = str(count_doc)

    annotated_data = read_json_file(annotated_docname_list, path_to_annotated)
    predicted_data = read_json_file(predicted_docname_list, path_to_predicted)
    
    if len(annotated_data) != len(predicted_data):
        #print("ERROR: Length of annotated files and predictions files don't match {} , {}".format(len(annotated_data), len(predicted_data)))
        return (None, "ERROR: Length of annotated files and predictions files don't match {} , {}".format(len(annotated_data), len(predicted_data)))
    
    annotated_data = extract_text_labels(annotated_data)
    annotated_data = sort_labels(annotated_data)
    
    TEXTA, ACTUAL_LABELS = text_to_word_level.convert_text_to_word_level(annotated_data)
    print("Length of annotated data : {}".format(len(TEXTA)))

    TEXTP, PREDICTED_LABELS = text_to_word_level.convert_text_to_word_level(predicted_data)
    print("Length of predicted data : {}".format(len(TEXTP)))

    actual_label, predicted_label = combine_annotated_and_predicted_sentence(TEXTA, ACTUAL_LABELS, TEXTP, PREDICTED_LABELS)
    print("Length of combined data : {}".format(len(actual_label)))

    y_true, y_pred = remove_other_and_SEP(actual_label, predicted_label)
    
    labels = read_labels_file(cwd + '/' + reponame)
    #labels.append('O')
    
    curr_time = generate_current_time()

    #TO DO return global results file in json format 
    # ifsuccessful = make_results_file.update_results_file(cwd, reponame, modelname, versionname, count_doc, y_true, y_pred, labels, curr_time)
    # if (ifsuccessful == None):
    #     return (None, "Error in making results file in problem repository")

    path = path_to_predicted

    #save_results_path = path + '/scores_' + str(count_doc) + '_' + curr_time
    # if os.path.exists(save_results_path):
    #     print("ERROR : Path already exists")
    #     return
    #os.mkdir(save_results_path)

    save_results_path = path + '/scores'
    if not os.path.exists(save_results_path):
        os.mkdir(save_results_path)

    labels.append('O')
    conf_matrix = confusion_matrix(y_true, y_pred, labels = labels)
    confusion_fig = plot_confusion_matrix(conf_matrix, labels, save_results_path)
    
    temp = labels.pop()
    
    #class_report = classification_report(y_true, y_pred, labels = labels)
    #print(class_report)
    
    columns_list = ['precision', 'recall', 'f1-score', 'support']
    
    try:
        class_report = classification_report(y_true, y_pred, labels = labels, output_dict=True)
    except:
        class_report = make_classification_report_if_exception(labels)
    
    print(class_report)
    
    df_report = pd.DataFrame(class_report).transpose()
    df_report = df_report.reindex(columns = columns_list)
    
    report_json = df_report.to_json(orient='split')
    report_json = json.loads(report_json)
    #print(report_json)
    df_report.to_csv(save_results_path + '/classification_report.csv', header=True)
    
    # update already scored count
    with open(path_to_predicted + '/alreadyscoredcount.txt','w') as f:
    	f.write(already_scored_count)
    	f.close()

    return (True, report_json, confusion_fig, int(already_scored_count))

#main()
