from sys import platform

HAS_MSWORD = False

if HAS_MSWORD and platform == 'win32':
  try:
    from win32com import client
    word = client.gencache.EnsureDispatch("Word.application")
    word.Visible = False
  except AttributeError:
      # Corner case dependencies.
      import os
      import re
      import sys
      import shutil
      # Remove cache and try again.
      MODULE_LIST = [m.__name__ for m in sys.modules.values()]
      for module in MODULE_LIST:
          if re.match(r'win32com\.gen_py\..+', module):
              del sys.modules[module]
      shutil.rmtree(os.path.join(os.environ.get('LOCALAPPDATA'), 'Temp', 'gen_py'))
      from win32com import client
      word = client.gencache.EnsureDispatch("Word.application")
      word.Visible = False
    

import tempfile, os
import pandas as pd
import json
import nltk
import re
import sys 
import argparse
import numpy as np
#nltk.download('punkt')
from nltk.tokenize import word_tokenize
import unicodedata
import six
import shutil
import time
import random
from .run_docx_to_json import convert_files
from ..utils import DOCUMENTS, Getparentpath

NOT_FOUND_FILES_PATH = []
NOT_CONVERTED_FILES = []
DOCX = '.docx'
PDF = '.pdf'
DOC = '.doc'
JSON = '.json'
TEXT = '.txt'
rootPathforTempFolder = ''
isSlash = ''  # it's value depends on whether files path starts from '/' or not (currently /upload/)

# To generate random suffix for duplicate doc name
def generate_unique_string(length=5):
    password_length = length
    possible_characters = "1234567890"
    random_character_list = [random.choice(possible_characters) for i in range(password_length)]
    random_password = "".join(random_character_list)
    return random_password

# To write final processed data in specified path
def write_dataframe_to_json(data, filename):
    JSON_LIST = []
    TEXT = list(data['text'])
    for i in TEXT:
        dic = {}
        dic['text'] = i
        dic['labels'] = []
        JSON_LIST.append(dic)
        
    with open(filename, 'w', encoding='utf-8') as outfile:
        for line in JSON_LIST:
            json.dump(line, outfile)
            outfile.write('\n')
        outfile.close()    


# we are using nltk for tokenization
# but We are separately tokenizing on these characters ( : , - – ) 
def _run_split_on_punc(text):
    """Splits punctuation on a piece of text."""
    chars = list(text)
    i = 0
    start_new_word = True
    output = []
    while i < len(chars):
      char = chars[i]
      if char == ':' or char == ',' or char == '-' or char == '–':
        output.append([char])
        start_new_word = True
      else:
        if start_new_word:
          output.append([])
        start_new_word = False
        output[-1].append(char)
      i += 1

    return ["".join(x) for x in output]

def whitespace_tokenize(text):
  """Runs basic whitespace cleaning and splitting on a piece of text."""
  text = text.strip()
  if not text:
    return []
  tokens = text.split()
  return tokens

def _is_whitespace(char):
  """Checks whether `chars` is a whitespace character."""
  # \t, \n, and \r are technically contorl characters but we treat them
  # as whitespace since they are generally considered as such.
  if char == " " or char == "\t" or char == "\n" or char == "\r":
    return True
  cat = unicodedata.category(char)
  if cat == "Zs":
    return True
  return False


def _is_control(char):
  """Checks whether `chars` is a control character."""
  # These are technically control characters but we count them as whitespace
  # characters.
  if char == "\t" or char == "\n" or char == "\r":
    return False
  # I have added these characters after seeing/analyzing text data that we get after original document conversion
  if char == '*' or char == '|':
    return True

  cat = unicodedata.category(char)
  if cat.startswith("C"):
    return True
  return False


def _clean_text(text):
    """Performs invalid character removal and whitespace cleanup on text."""
    output = []
    for char in text:
      cp = ord(char)
      if cp == 0 or cp == 0xfffd or _is_control(char):
        continue
      if _is_whitespace(char):
        output.append(" ")
      else:
        output.append(char)
    return "".join(output)


def convert_to_unicode(text):
  """Converts `text` to Unicode (if it's not already), assuming utf-8 input."""
  if six.PY3:
    if isinstance(text, str):
      return text
    elif isinstance(text, bytes):
      return text.decode("utf-8", "ignore")
    else:
      raise ValueError("Unsupported string type: %s" % (type(text)))
  elif six.PY2:
    if isinstance(text, str):
      return text.decode("utf-8", "ignore")
    elif isinstance(text, unicode):
      return text
    else:
      raise ValueError("Unsupported string type: %s" % (type(text)))
  else:
    raise ValueError("Not running on Python2 or Python 3?")

# we are trying to follow same preprocessing steps
# done by bert so that we do not lose unexpected words
def bert_preprocess(text):
    text = convert_to_unicode(text)
    text = _clean_text(text)
    text_tokens = whitespace_tokenize(text)
    split_tokens = []
    for token in text_tokens:
      split_tokens.extend(_run_split_on_punc(token))
    text = " ".join(split_tokens)

    return text

#to word tokenize sentence and rejoin it
def tokenize_sentence(text):
    text_tok = word_tokenize(text)
    text = ' '.join(text_tok)
    return str(text)

# to remove extra spaces and strip sentence
def remove_extra_spaces_strip(text):
    text = re.sub(' +', ' ', text)
    text = text.strip()
    return text

# to remove sentences which neither contains aplhabet nor numeric character 
def remove_invalidlines(text):
    flag=0
    for i in text:
        if i.isalnum():
            flag=1
            break
    if flag==0:
        text=''
    return text    

# to split text on ':' character because word tokenizer mostly ignores it
def splitting_sentence(text):
    text = text.split(':')
    text = ' : '.join(text)
    return text

# to preprocess all the sentences
def preprocessing_sentence(data):
    #data['text'] = data['text'].apply(lambda x: splitting_sentence(x))
    data['text'] = data['text'].apply(lambda x: remove_invalidlines(x))
    data['text'] = data['text'].apply(lambda x: bert_preprocess(x))
    #data['text'] = data['text'].apply(lambda x: remove_extra_spaces_strip(x))
    
    data.replace('', np.nan, inplace=True)
    data.dropna(subset=['text'], inplace=True)
    data['text'] = data['text'].apply(lambda x: tokenize_sentence(x))
    return data


# to extract sentences from json object of a document (unpreprocessed sentences)
def read_sentence_from_json_object(jfile):
    text_tuple = []
    for jobj in jfile['content']:
        sentence = jobj['sentence']
        text_tuple.append(sentence)
    return text_tuple

# to get filename from full path    
def getfilename(path):
	if path[-1]=='/' or path[-1]=='\\':
		path = path[0:-1]
	if path.rfind('\\') != -1 and path.rfind('\\') > path.rfind('/'):
		f = path[path.rfind('\\')+1:]
	else:
		f = path[path.rfind('/')+1:]
	return f

# to process .docx file in temporary folder
def handleTempFileLikeDocx(cwd, temp_file_path, destinationPath, curr_file_name):
    json_object = convert_files(temp_file_path)
    if json_object is None:
      print("Parse Error: can't convert temporary {} file to raw format".format(temp_file_path))
      return (None, "Parse Error: can't convert temporary {} file to raw format".format(temp_file_path))
    text_rows = read_sentence_from_json_object(json_object)
    data = pd.DataFrame({ 'text': text_rows })
    data = preprocessing_sentence(data)
    destFilePath = destinationPath + '/' + curr_file_name + '.raw.json'
    if os.path.isfile(destFilePath):
    	destFilePath = destinationPath + '/' + curr_file_name + generate_unique_string() + '.raw.json'
    write_dataframe_to_json(data, destFilePath)
    return (True,'')

# to process .doc and .pdf file
def handlePdfDocFile(cwd, filePath, destinationPath, tempFolderPathname):
    curr_file_name = getfilename(filePath)
    temp_file = curr_file_name + '.docx'
    temp_file_path = tempFolderPathname + '/' + temp_file
    # check if temp docx file already exists (remove it if exists)
    if os.path.isfile(temp_file_path): 
        os.remove(temp_file_path)
    try:
      filePath = os.path.abspath(filePath)
      wordDoc = word.Documents.Open(filePath)
      wordDoc.SaveAs2(temp_file_path, FileFormat = 16)
      wordDoc.Close()
      return handleTempFileLikeDocx(cwd, temp_file_path, destinationPath, curr_file_name)
    except Exception as e:
      print('Failed to Convert: {0}'.format(filePath))
      print(e)
      return (None, 'Failed to Convert {} to .docx using MSWord bcz {}'.format(filePath, e))
    return (None, "Uncaught error (not expected)")

# to process .docx file
def handleDocxFile(cwd, filePath, destinationPath):
	curr_file_name = getfilename(filePath)
	json_object = convert_files(filePath)
	if json_object is None:
		print("Parse Error can't convert {} file to raw format".format(filePath))
		return (None, "Parse Error can't convert {} file to raw format".format(filePath))
	text_rows = read_sentence_from_json_object(json_object)
	data = pd.DataFrame({ 'text': text_rows })
	data = preprocessing_sentence(data)
	destFilePath = destinationPath + '/' + curr_file_name + '.raw.json'
	if os.path.isfile(destFilePath):
		destFilePath = destinationPath + '/' + curr_file_name + generate_unique_string() + '.raw.json'
	write_dataframe_to_json(data, destFilePath)
	return (True,'')

def main(cwd, filesArray, reponame, mastername):
    global DOCX, PDF, DOC, JSON, TEXT, HAS_MSWORD, isSlash
    # creating temporary folder
    try:
        tempFolderPathname = tempfile.mkdtemp(dir=Getparentpath(cwd))
    except Exception as e:
        print("Cant't make temp folder due to : {}".format(e))
        return (None, "Can't make temp folder")

    destinationPath = cwd + mastername + '/' + reponame + '/' + DOCUMENTS
    if not os.path.exists(destinationPath):
      return (None, "Invalid Path {}".format(destinationPath))

    startTime = time.time()
    for filePath in filesArray:
        if not os.path.exists(filePath):
            NOT_FOUND_FILES_PATH.append(filePath)            
            continue

        # check type of file
        if filePath.endswith(DOCX):
          result = handleDocxFile(cwd, filePath, destinationPath)
          if result[0] is None:
            NOT_CONVERTED_FILES.append((filePath, result[1]))

        elif platform == 'win32' and HAS_MSWORD and (filePath.endswith(PDF) or filePath.endswith(DOC)):
          result = handlePdfDocFile(cwd, filePath, destinationPath, tempFolderPathname)
          if result[0] is None:
            NOT_CONVERTED_FILES.append((filePath, result[1]))

        else:
            NOT_CONVERTED_FILES.append((filePath, "This format is not supported yet"))

    try:
        shutil.rmtree(tempFolderPathname)
    except Exception as e:
        print("Can't remove temp folder due to: {}".format(e))

    if platform == 'win32' and HAS_MSWORD:
        word.Quit()
        #pass    

    if len(NOT_FOUND_FILES_PATH) > 0:
        print("These files does not exist:\n {}".format(NOT_FOUND_FILES_PATH)) 
    if len(NOT_CONVERTED_FILES) > 0:
        print("These files are not converted to raw.json with their respective reason \n {}".format(NOT_CONVERTED_FILES))

    print(time.time()-startTime)

    return (True, len(filesArray) - len(NOT_FOUND_FILES_PATH) - len(NOT_CONVERTED_FILES))

#main()