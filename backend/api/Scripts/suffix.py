import copy

def make_suffixes(docname_list, data):
    JSON_LIST = []

    for i in range(len(data)):
        TEXT = ''
        INT_LABELS = []
        for j in range(i+1,len(data)):
            if docname_list[j] != docname_list[i]:
                break
            text = data[j]['text']
            labels = data[j]['labels']
            if j == i+1:
                TEXT = copy.deepcopy(text)
                INT_LABELS.extend(labels)
            else:
                for k in range(len(labels)):
                    start = labels[k][0] + len(TEXT) + 1
                    end = labels[k][1] + len(TEXT) + 1
                    tag = labels[k][2]
                    INT_LABELS.append([start,end,tag])
                TEXT = TEXT + ' ' + text
        if len(TEXT) == 0:
            TEXT = '~'          # for last sentence of resume
        dic = {}
        dic['text'] = copy.deepcopy(TEXT)
        dic['labels'] = copy.deepcopy(INT_LABELS)
        JSON_LIST.append(dic)
     
    return JSON_LIST