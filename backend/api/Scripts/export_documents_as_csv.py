# For now it will make csv file for all the files for both annotations and predictions
# Returns output file path

import os
import pandas as pd
import json
import csv
#from ..utils import PREDICTION 

# Inserting substrings for single document (every substring goes to next row of the maximum length of all the entities)
def make_csv_for_single_file__(col_map, array, filename, data):
    Max_len = len(array[0])
    for i in range(len(data)):
        text = data[i]['text']
        labels = data[i]['labels']
        
        if len(labels) > 0:
            start_index = 0
            while start_index < len(labels):
                start = labels[start_index][0]
                end = labels[start_index][1]
                tag = labels[start_index][2]
                index = start_index + 1
                while index < len(labels) and labels[index][2]==tag:
                    end = labels[index][1]
                    index = index + 1

                if len(array[col_map[tag]]) != Max_len:
                    ct = Max_len - len(array[col_map[tag]])
                    array[col_map[tag]].extend(ct*[""])
                    array[col_map[tag]][-1] = text[start:end]
                else:        
                    array[col_map[tag]].append(text[start:end])
                Max_len = len(array[col_map[tag]])
                
                start_index = index

    # appending '' to every column(not filename column) whose length is less than 'max_len'
    for k in range(1,len(array)):
        if len(array[k]) != Max_len:
            ct = Max_len - len(array[k])
            array[k].extend(ct*[""])

    # for first column (i.e. filename) we want only one entry in first row corresponding to current document
    old_len = len(array[0])
    array[0].extend([""]*(Max_len-len(array[0])))
    if len(array[0]) > old_len:
        array[0][old_len] = filename    
    else:
        '''if there is no label for the current document then we simply make one row with it's name'''
        for j in range(len(array)):
            array[j].append("")
        array[0][-1] = filename   

    # for separation between documents
    for j in range(len(array)):
        array[j].append("")
    return array

# Inserting substrings for single document (original: fill directly in respective column without seeing any other columns length)
def make_csv_for_single_file(col_map, array, filename, data):
    for i in range(len(data)):
        text = data[i]['text']
        labels = data[i]['labels']
        
        if len(labels) > 0:
            start_index = 0
            while start_index < len(labels):
                start = labels[start_index][0]
                end = labels[start_index][1]
                tag = labels[start_index][2]
                index = start_index + 1
                while index < len(labels) and labels[index][2]==tag:
                    end = labels[index][1]
                    index = index + 1
                array[col_map[tag]].append(text[start:end])
                start_index = index
        
    # finding column with maximum length
    max_len = len(array[0])
    for k in range(1,len(array)):
        max_len = max(max_len,len(array[k]))
    # for first column i.e. doc. no. would be same for every row of just finished doc    
    #array[0].extend([filename]*(max_len-len(array[0])))
    old_len = len(array[0])
    array[0].extend([""]*(max_len-len(array[0])))
    if len(array[0]) > old_len:
        array[0][old_len] = filename    
    # appending '' to every other column whose length is less than 'max_len'
    for k in range(1,len(array)):
        if len(array[k]) != max_len:
            ct = max_len - len(array[k])
            array[k].extend(ct*[""])

    return array

# To read single json file at a time
def read_json_file(filename, path):
    data = [json.loads(line) for line in open(path + '/' + filename, 'r', encoding='utf-8')]
    return data

# Inserting all documents in 2-D array
def make_csv_for_all_files(col_map, docname_list, path, array):    
    for filename in docname_list:
        data = read_json_file(filename, path)
        array = make_csv_for_single_file__(col_map, array, filename, data)

    return array

def read_labels_file(path):
    temp = []
    with open(path) as filep:
        csvreader = csv.reader(filep)
        for row in csvreader:
            temp.append(str(row[0].strip()))
    return temp

# for now we will always make new csv file by ourselves reading all the documents in the specified path
# i.e. we are not appending to previous state of csv file bcz 'user may modify a file or he may add new label'
def load_previous_csv_data_to_2d_array(path_to_labels, outpath):
    MY_LABELS = read_labels_file(path_to_labels)

    # our first column should be document name to which the text/phrase belongs to
    COL_NAMES = ['FILE_NAME']
    COL_NAMES.extend(MY_LABELS)

    # actually we will create a 2-D matrix whose index correponds to doc name and labels
    col_map = {}
    for i in range(len(COL_NAMES)):
        col_map[COL_NAMES[i]] = i

    '''making 2d list with empty lists for all columns of excel file'''
    array = []
    for i in range(len(COL_NAMES)):
        array.append([])

    return COL_NAMES, col_map, array

def documents_to_be_used(inpath, filetype):
    filesArray = []
    
    files = next(os.walk(inpath))[2]
    for filename in sorted(files, key=str.lower, reverse=False):
        if filename.endswith(filetype):
            filesArray.append(filename)
    
    return filesArray        

def main(cwd, reponame, modelid=None, actualmodelname=None, versionname=None, tasktype='PREDICTION'):
    path_to_labels = cwd + '/' + reponame + '/list_containing_labels.csv'
    if tasktype == 'PREDICTION':
        filetype = '.predictions.json'
        inpath = cwd + '/' + reponame +'/'+ modelid + '/' + versionname
        outpath = cwd + '/' + reponame + '/documents/' + '__Predictions_' + actualmodelname + '_' + versionname + '.csv'
    else:
        filetype = '.annotated.json'
        inpath = cwd + '/' + reponame + '/documents'
        outpath = cwd + '/' + reponame + '/documents/' + '__Annotations.csv'
    
    filesArray = documents_to_be_used(inpath, filetype)

    if len(filesArray) == 0:
        return (False, "No files available")
        
    COL_NAMES, col_map, array = load_previous_csv_data_to_2d_array(path_to_labels, outpath)

    dataArray = make_csv_for_all_files(col_map, filesArray, inpath, array)

    output_frame = pd.DataFrame()
    for i in range(len(dataArray)):
        output_frame[COL_NAMES[i]] = dataArray[i]

    output_frame.to_csv(outpath, header=True, index=False, sep=',', encoding='utf-8')
    
    return (True, outpath)

# def execute():
#     cwd = 'D:/annotationtool/nlp-tool/upload/3'
#     #cwd = 'D:/masterrepo'
#     result = main(cwd, '29',modelid='15', actualmodelname='model1',versionname='Version1',tasktype='PREDICTION')
#     print(result)
# execute()