
import os
import pandas as pd
import json
import nltk
import re
import sys 
import argparse
import numpy as np
#nltk.download('punkt')
from nltk.tokenize import word_tokenize
import unicodedata
import six
import shutil

from run_docx_to_json import convert_files


cwd = os.getcwd()
if cwd.rfind('\\') != -1 and cwd.rfind('\\') > cwd.rfind('/'):
    cwd = cwd[0:cwd.rfind('\\')]
else:
    cwd = cwd[0:cwd.rfind('/')]


ap = argparse.ArgumentParser()
ap.add_argument("-dn", "--docnamepath",type=str,help="full path to document name")
ap.add_argument("-dp", "--docpath",type=str,help="path to folder containing documents")
ap.add_argument("-rn", "--reponame",required=True,type=str,help="path to problem repository where documents are to be transferred")
args = vars(ap.parse_args())

reponame = args['reponame']


def write_dataframe_to_json(data, filename):
    JSON_LIST = []
    TEXT = list(data['text'])
    for i in TEXT:
        dic = {}
        dic['text'] = i
        dic['labels'] = []
        JSON_LIST.append(dic)
        
    with open(filename, 'w',encoding='utf-8') as outfile:
        for line in JSON_LIST:
            json.dump(line, outfile)
            outfile.write('\n')
        outfile.close()    
        
        
# In[ ]:

# def _run_split_on_punc(text):
#     """Splits punctuation on a piece of text."""
#     chars = list(text)
#     i = 0
#     start_new_word = True
#     output = []
#     while i < len(chars):
#       char = chars[i]
## Originally it was all the punctuations but we won't do that
#       if char == ':' or char == ',' or char == '-':
#         output.append([char])
#         start_new_word = True
#       else:
#         if start_new_word:
#           output.append([])
#         start_new_word = False
#         output[-1].append(char)
#       i += 1

#     return ["".join(x) for x in output]

def whitespace_tokenize(text):
  """Runs basic whitespace cleaning and splitting on a piece of text."""
  text = text.strip()
  if not text:
    return []
  tokens = text.split()
  return tokens

def _is_whitespace(char):
  """Checks whether `chars` is a whitespace character."""
  # \t, \n, and \r are technically contorl characters but we treat them
  # as whitespace since they are generally considered as such.
  if char == " " or char == "\t" or char == "\n" or char == "\r":
    return True
  cat = unicodedata.category(char)
  if cat == "Zs":
    return True
  return False


def _is_control(char):
  """Checks whether `chars` is a control character."""
  # These are technically control characters but we count them as whitespace
  # characters.
  if char == "\t" or char == "\n" or char == "\r":
    return False
  # I have added these characters after seeing/analyzing text data that we get after original document conversion
  # if char == '*' or char == '|':
  #   return True
  cat = unicodedata.category(char)
  if cat.startswith("C"):
    return True
  return False


def _clean_text(text):
    """Performs invalid character removal and whitespace cleanup on text."""
    output = []
    for char in text:
      cp = ord(char)
      if cp == 0 or cp == 0xfffd or _is_control(char):
        continue
      if _is_whitespace(char):
        output.append(" ")
      else:
        output.append(char)
    return "".join(output)


def convert_to_unicode(text):
  """Converts `text` to Unicode (if it's not already), assuming utf-8 input."""
  if six.PY3:
    if isinstance(text, str):
      return text
    elif isinstance(text, bytes):
      return text.decode("utf-8", "ignore")
    else:
      raise ValueError("Unsupported string type: %s" % (type(text)))
  elif six.PY2:
    if isinstance(text, str):
      return text.decode("utf-8", "ignore")
    elif isinstance(text, unicode):
      return text
    else:
      raise ValueError("Unsupported string type: %s" % (type(text)))
  else:
    raise ValueError("Not running on Python2 or Python 3?")


def bert_preprocess(text):
    text = convert_to_unicode(text)
    text = _clean_text(text)
    text_tokens = whitespace_tokenize(text)
    # Remove this line in updated script
    text = " ".join(text_tokens)
    # split_tokens = []
    # for token in text_tokens:
    #   split_tokens.extend(_run_split_on_punc(token))
    # text = " ".join(split_tokens)

    return text

#to word tokenize sentence and rejoin it
def tokenize_sentence(text):
    text_tok = word_tokenize(text)
    text = ' '.join(text_tok)
    return str(text)

# to remove extra spaces and strip sentence
def remove_extra_spaces_strip(text):
    text = re.sub(' +', ' ', text)
    text = text.strip()
    return text

# to remove sentences which neither contains aplhabet nor numeric character 
def remove_invalidlines(text):
    flag=0
    for i in text:
        if i.isalnum():
            flag=1
            break
    if flag==0:
        text=''
    return text    

# to split text on ':' character because word tokenizer mostly ignores it
def splitting_sentence(text):
    text = text.split(':')
    text = ' : '.join(text)
    return text

# to preprocess all the sentences
def preprocessing_sentence(data):
    # comment this line in updated script
    data['text'] = data['text'].apply(lambda x: splitting_sentence(x))
    data['text'] = data['text'].apply(lambda x: remove_invalidlines(x))
    data['text'] = data['text'].apply(lambda x: bert_preprocess(x))
    #data['text'] = data['text'].apply(lambda x: remove_extra_spaces_strip(x))
    
    data.replace('', np.nan, inplace=True)
    data.dropna(subset=['text'], inplace=True)
    data['text'] = data['text'].apply(lambda x: tokenize_sentence(x))
    return data


# In[ ]:


# to extract sentences from json object of a document
def read_sentence_from_json_object(jfile):
    text_tuple = []
    for jobj in jfile['content']:
        sentence = jobj['sentence']
        text_tuple.append(sentence)
    return text_tuple


def read_docx_files(inpath, destination, filetype):
	docx_files = []
	for pack in os.walk(inpath):
		for f in pack[2]:
			if f.endswith(filetype):
				destpath = destination + '/' + f
				if not os.path.exists(destpath):
					docx_files.append((pack[0],f))

	return docx_files

def getfilename(path):
	if path[-1]=='/' or path[-1]=='\\':
		path = path[0:-1]
	if path.rfind('\\') != -1 and path.rfind('\\') > path.rfind('/'):
		f = path[path.rfind('\\')+1:]
	else:
		f = path[path.rfind('/')+1:]
	return f


def main():
	destination = cwd + '/' + reponame
	if not os.path.exists(destination):
		print("Invalid repository name {}".format(destination))
		return

	if args['docnamepath']:
		filename = cwd + '/' + args['docnamepath']
		if not os.path.exists(filename):
			print("Invalid path {}".format(filename))
		elif not filename.endswith('.docx'):
			print("File should end with .docx")
		else:
			f = getfilename(filename)
			print(f)
			destpath = destination + '/' + f
			print(destpath)
			if os.path.exists(destpath):
				print("document already exists in {}".format(reponame))
				return
			json_object = convert_files(filename)
			if json_object is None:
				print("Parse Error")
				return
			text_rows = read_sentence_from_json_object(json_object)
			data = pd.DataFrame({'text':text_rows})
			data = preprocessing_sentence(data)
			destpath = destpath + '.raw.json'
			write_dataframe_to_json(data, destpath)
			shutil.copy(filename, destination)
	else:
		inpath = cwd + '/documents/'
		if args['docpath']:
			if args['docpath'][-1] != '/':
				inpath = cwd + '/' + args['docpath'] + '/'
			else:
				inpath = cwd + '/' + args['docpath']
		if not os.path.exists(inpath):
			print("Invalid path {}".format(inpath))
			return

		docx_list = read_docx_files(inpath, destination, filetype='.docx')
		if len(docx_list)==0:
			print("No new documents to be copied to {}".format(reponame))
			return
		for i in range(len(docx_list)):
			curr_document = docx_list[i][0] + '/' + docx_list[i][1]
			json_object = convert_files(curr_document)
			if json_object is None:
				print("Parse Error can't convert {} file to raw format".format(curr_document))
				continue
			text_rows = read_sentence_from_json_object(json_object)
			data = pd.DataFrame({'text':text_rows})
			data = preprocessing_sentence(data)
			newpath = destination + '/' + docx_list[i][1] + '.raw.json'
			write_dataframe_to_json(data, newpath)
			shutil.copy(curr_document, destination)

main()