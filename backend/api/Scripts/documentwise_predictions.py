#!/usr/bin/env python
# coding: utf-8

# In[1]:


import os
import pandas as pd
import numpy as np
import pickle
import csv
import json


# In[2]:


def separate_documents(data_list, docname_list, confidence_ratio_list):
    DOCNAMES = []
    DATA = []
    CONFIDENCE_RATIO = []
    
    data = []
    confidence_ratio = []
    prev_docname = docname_list[0]
    
    for i in range(len(docname_list)):
        if docname_list[i] != prev_docname:
            DOCNAMES.append(prev_docname)
            DATA.append(data)
            CONFIDENCE_RATIO.append(sum(confidence_ratio)/len(confidence_ratio))
            data = []
            confidence_ratio = []
        data.append(data_list[i])
        confidence_ratio.extend(confidence_ratio_list[i])
        prev_docname = docname_list[i]
        
    DATA.append(data)
    DOCNAMES.append(prev_docname)
    CONFIDENCE_RATIO.append(sum(confidence_ratio)/len(confidence_ratio))

    return DATA, DOCNAMES, CONFIDENCE_RATIO


# In[3]:


def concat_labels(array):
    if len(array) < 2:
        return array
    index = 0
    while index < len(array)-1:
        while index < len(array)-1 and array[index][2] == array[index+1][2] and array[index][1]+1 == array[index+1][0]:
            array[index][1] = array[index+1][1]
            temp = array.pop(index+1)
        index = index + 1
    return array


# In[4]:


def joining_sentences_and_labels(TEXT, LABELS, MY_LABELS):
    
    JSON_LIST = []
    for i in range(len(TEXT)):
        dic = {}
        sent = ' '.join(TEXT[i])
        dic['text'] = sent
        JSON_LIST.append(dic)
    
    for i in range(len(TEXT)):
        tags_list = []
        start = 0
        for j in range(len(TEXT[i])):
            end = start + len(TEXT[i][j])
            if LABELS[i][j] in MY_LABELS:
                tags_list.append([start, end, LABELS[i][j]])
            start = end + 1
        JSON_LIST[i]['labels'] = tags_list
        
    for i in range(len(JSON_LIST)):
        JSON_LIST[i]["labels"] = concat_labels(JSON_LIST[i]["labels"])
        
    return JSON_LIST    


# In[5]:


def read_labels_file(path):
    temp = []
    with open(path+'/list_containing_labels.csv') as filep:
        csvreader = csv.reader(filep)
        for row in csvreader:
            temp.append(str(row[0].strip()))
    return temp        


# In[6]:


def make_sentences(data_text, data_predicted_label, data_confidence_ratio):
    TEXT = []
    LABELS = []
    CONFIDENCE_RATIOS = []

    text = []
    label = []
    confidence_ratio = []

    for i in range(len(data_text)):
        word = data_text[i]
        tag = data_predicted_label[i]
        conf_ratio = data_confidence_ratio[i]

        if word == '[SEP]':
            TEXT.append(text)
            LABELS.append(label)
            CONFIDENCE_RATIOS.append(confidence_ratio)
            text = []
            label = []
            confidence_ratio = []
        else:
            text.append(word)
            label.append(tag)
            confidence_ratio.append(conf_ratio)
            
    return TEXT, LABELS, CONFIDENCE_RATIOS


# In[7]:


def combine_predicted_and_test_data(data_test, data_predict):
    TEXT = []
    #ACTUAL_LABELS = []
    PREDICTED_LABELS = []
    CONFIDENCE_RATIOS = []

    a = 0
    b = 0
    ct = 0
    chop =0
    #lineno = 0

    while b < len(data_predict[0]):
        text_b = data_predict[0].iloc[b]
        predicted_label = data_predict[2].iloc[b]
        confidence_ratio = data_predict[3].iloc[b]
        text_a = data_test[0].iloc[a]
        #actual_label = data_test[1].iloc[a]

        if text_b == '[SEP]':
            if text_a == '[SEP]':
                TEXT.append(text_a)
                #ACTUAL_LABELS.append(actual_label)
                PREDICTED_LABELS.append(predicted_label)
                CONFIDENCE_RATIOS.append(confidence_ratio)
                b = b + 1
                a = a + 1
            else:
                ct = ct + 1
                while text_a != '[SEP]':
                    TEXT.append(text_a)
                    #ACTUAL_LABELS.append(actual_label)
                    PREDICTED_LABELS.append('O')
                    CONFIDENCE_RATIOS.append(0)
                    a = a + 1
                    text_a = data_test[0].iloc[a]
                    #actual_label = data_test[1].iloc[a]
                    chop = chop + 1
                TEXT.append(text_a)
                #ACTUAL_LABELS.append(actual_label)
                PREDICTED_LABELS.append(predicted_label)
                CONFIDENCE_RATIOS.append(confidence_ratio)
                a = a + 1
                b = b + 1
        else:
            if text_a.find(text_b)!=0:
                print(text_a,'\t',text_b,'\t\t', a,'\t',b)
                print()    
            TEXT.append(text_a)
            #ACTUAL_LABELS.append(actual_label)
            PREDICTED_LABELS.append(predicted_label)
            CONFIDENCE_RATIOS.append(confidence_ratio)
            b = b + 1
            a = a + 1
                
    return TEXT, PREDICTED_LABELS, CONFIDENCE_RATIOS


# In[8]:


def remove_prefix_suffix(data):

    text = []
    actual_label = []
    predicted_label = []
    confidence_ratio = []

    flag = 0
    for i in range(len(data[0])):
        if flag==1 and data[0].iloc[i] == '[SEP]':
            text.append(data[0].iloc[i])
            actual_label.append(data[1].iloc[i])
            predicted_label.append(data[2].iloc[i])
            confidence_ratio.append(data[3].iloc[i])
            flag = 0
        elif flag==0 and data[0].iloc[i] == '[SEP]':
            flag = 1
        elif flag == 1:
            text.append(data[0].iloc[i])
            actual_label.append(data[1].iloc[i])
            predicted_label.append(data[2].iloc[i])
            confidence_ratio.append(data[3].iloc[i])
    
    return pd.DataFrame({0:text, 1:actual_label, 2:predicted_label, 3:confidence_ratio})


# In[9]:


def make_prediction_files(cwd, reponame, modelname, versionname):
    path = cwd + '/' + reponame + '/'+ modelname + '/' + versionname
    
    data_predict = pd.read_csv(path + '/output_test/output_predictions.txt', header=None, sep='\t', keep_default_na=False)
    data_predict = remove_prefix_suffix(data_predict)
    data_test = pd.read_csv(path + '/data_test/test.txt', header=None, sep=' ',keep_default_na=False)
    TEXT, PREDICTED_LABELS, CONFIDENCE_RATIOS = combine_predicted_and_test_data(data_test, data_predict)
    TEXT, LABELS, CONFIDENCE_RATIOS = make_sentences(TEXT, PREDICTED_LABELS, CONFIDENCE_RATIOS)
    #print(len(TEXT),len(LABELS))
    with open(path + '/data_test/' + 'list_containing_document_names.txt','rb') as fpickle:
        docname_list = pickle.load(fpickle)
    #print(len(docname_list))
    
    path_to_labels = cwd + '/' + reponame
    MY_LABELS = read_labels_file(path_to_labels)
    #print(MY_LABELS)
    
    data_list = joining_sentences_and_labels(TEXT, LABELS, MY_LABELS)
    #print(len(data_list))
    
    DATA, DOCNAMES, CONFIDENCERATIOS = separate_documents(data_list, docname_list, CONFIDENCE_RATIOS)
    #print(len(DOCNAMES), len(DATA))
    
    OUTPUT_FILENAMES = []
    for i in range(len(DOCNAMES)):
        filename = DOCNAMES[i]
        filename = filename[0:filename.rfind('.raw.json')] + '.predictions.json'
        OUTPUT_FILENAMES.append(filename)
        with open(path + '/' + filename, 'w') as outfile:
            for line in DATA[i]:
                json.dump(line, outfile)
                outfile.write('\n')
            outfile.close()    

    FILES_WITH_CONFIDENCE = []
    for i in range(len(OUTPUT_FILENAMES)):
        FILES_WITH_CONFIDENCE.append(dict({"filename": OUTPUT_FILENAMES[i], "confidence_ratio": CONFIDENCERATIOS[i]}))

    return FILES_WITH_CONFIDENCE

# In[ ]:




