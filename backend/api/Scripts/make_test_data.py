#!/usr/bin/env python
# coding: utf-8

# In[1]:


# to make test data for raw documents in reponame whose annotated documents are not present 
# and whose predictions are not done already by the specified modelname and versionname


# In[2]:


import os
import json
import re
import sys 
import argparse
import copy
import pickle


# In[3]:


from . import prefix, suffix, text_to_word_level
from ..utils import DOCUMENTS

# In[4]:


# cwd = os.getcwd()
# if cwd.rfind('\\') != -1:
#     cwd = cwd[0:cwd.rfind('\\')]
# else:
#     cwd = cwd[0:cwd.rfind('/')]


# In[5]:

# In[6]:


def displaydata(docname_list, data):
    if len(docname_list)!=len(data):
        print("Length does not matches")
        return
    for i in range(len(data)):
        print(i,data[i],"\t",docname_list[i])


# In[7]:


def read_raw_documents(raw_docname_list, inpath):
    data = []
    docname_list = []
    
    for i in range(len(raw_docname_list)):
        filename = raw_docname_list[i]
        json_list = [json.loads(line) for line in open(inpath + '/' + filename, 'r',encoding='utf-8')]
        data.extend(json_list)
        docname_list.extend([filename for i in range(len(json_list))])
        json_list = []     
    return docname_list, data        


# In[8]:


def tobeused_raw_documents(inpath, outpath, count_raw):
    raw_docname_list = []
    annotated_docname_list = []
    
    filesinpath = next(os.walk(inpath))[2]
    for filename in sorted(filesinpath, key=str.lower, reverse=False):
            if filename.endswith('.annotated.json'):
                annotated_docname_list.append(filename)
            if filename.endswith('.raw.json'):
                raw_docname_list.append(filename)
    
    if len(raw_docname_list) == 0:
        return []
    
    new_raw_docname_list = []
    for i in range(len(raw_docname_list)):
        filename = raw_docname_list[i]
        annotated_filename = filename[0:filename.rfind('.raw.json')] + '.annotated.json'
        if annotated_filename not in annotated_docname_list:
            new_raw_docname_list.append(filename)
    
    if len(new_raw_docname_list) == 0:
        return []
    
    predicted_docname_list = []        
    filesoutpath = next(os.walk(outpath))[2]
    for filename in sorted(filesoutpath, key=str.lower, reverse=False):
        if filename.endswith('.predictions.json'):
            predicted_docname_list.append(filename)
            
    if len(predicted_docname_list) == 0:
        if count_raw and len(new_raw_docname_list) > count_raw:
            return new_raw_docname_list[0:count_raw]
        return new_raw_docname_list
    
    unpredicted_raw_docname_list = []
    for i in range(len(new_raw_docname_list)):
        filename = new_raw_docname_list[i]
        predicted_filename = filename[0:filename.rfind('.raw.json')] + '.predictions.json'
        if predicted_filename not in predicted_docname_list:
            unpredicted_raw_docname_list.append(filename)
    
    if count_raw and len(unpredicted_raw_docname_list) > count_raw:
        return unpredicted_raw_docname_list[0:count_raw]
    return unpredicted_raw_docname_list


# In[13]:


def make_data(cwd, reponame, modelname, versionname, count_raw=0):
    inpath = cwd + '/' + reponame + '/' + DOCUMENTS
    if not os.path.exists(inpath):
        #print("Invalid path {}".format(inpath))
        return (None, "Invalid path {}".format(inpath))
    
    outpath = cwd + '/'+ reponame +'/' + modelname + '/' + versionname
    if not os.path.exists(outpath):
        #print("Invalid path {}".format(outpath))
        return (None, "Invalid path {}".format(outpath))
    
    raw_docname_list = tobeused_raw_documents(inpath, outpath, count_raw)
    #print(len(raw_docname_list))
    
    if len(raw_docname_list) == 0:
        #print("No new raw documents available to be predicted by {} {} {}".format(reponame, modelname, versionname))
        return (False, "No new raw documents available to be predicted by {} {} {}".format(reponame, modelname, versionname))
    
    if not os.path.exists(outpath+'/data_test'):
        os.mkdir(outpath+'/data_test')
        
    with open(outpath + '/data_test/' + 'noofdocs.txt','w', encoding="utf-8") as fp:
        fp.write("No. of raw documents to be predicted {}".format(len(raw_docname_list)))
        fp.write('\n')
    
    docname_list, data = read_raw_documents(raw_docname_list, inpath)
    prefix_data = prefix.make_prefixes(docname_list, data)
    suffix_data = suffix.make_suffixes(docname_list, data)

    my_data = [data, prefix_data, suffix_data]
    
    my_test_data = []
    for i in my_data:
        TEXT, LABELS = text_to_word_level.convert_text_to_word_level(i)
        my_test_data.append((TEXT, LABELS))
        
    MY_DATA = {}
    MY_DATA['VALIDATE'], MY_DATA['TEST'] = [], []
    MY_DATA['TEST'].extend(my_test_data)
    MY_DATA['VALIDATE'].extend([([],[]), ([],[]), ([],[])])

    my_output_filenames = ['test.txt']
    
    for i in range(len(my_output_filenames)):
        TEXT, LABELS = my_test_data[i][0], my_test_data[i][1] 
        with open(outpath + '/data_test/' + my_output_filenames[i], 'w', encoding="utf-8") as f:
            for i in range(len(TEXT)):
                f.write(TEXT[i])
                f.write(' ')
                f.write(LABELS[i])
                f.write('\n')
        f.close()
        
    with open(outpath + '/data_test/' + 'list_containing_document_names.txt', 'wb') as fpickle:
        pickle.dump(docname_list,fpickle)
        fpickle.close()

    return (True, MY_DATA)

    # my_output_filenames = ['test.txt', 'prefix_test.txt', 'suffix_test.txt']

    # for i in range(len(my_output_filenames)):
    #     TEXT, LABELS = my_test_data[i][0], my_test_data[i][1] 
    #     with open(outpath+'/data_test/'+my_output_filenames[i],'w', encoding="utf-8") as f:
    #         for i in range(len(TEXT)):
    #             f.write(TEXT[i])
    #             f.write(' ')
    #             f.write(LABELS[i])
    #             f.write('\n')
    #     f.close()

    # # Empty validation files for eval=True
    # with open(outpath+'/data_test/validate.txt','w',encoding='utf-8') as f:
    #     f.close()    
    # with open(outpath+'/data_test/prefix_validate.txt','w',encoding='utf-8') as f:
    #     f.close()
    # with open(outpath+'/data_test/suffix_validate.txt','w',encoding='utf-8') as f:
    #     f.close()
