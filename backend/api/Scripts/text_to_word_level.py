def convert_text_to_word_level(t_l):
    TEXT = []
    LABELS = []

    for i in range(len(t_l)):
        text = t_l[i]['text']
        label = t_l[i]['labels']
        n = len(text)
        m = len(label)
        if m==0:
            TEXT.extend(text.split(' '))
            LABELS.extend(['O']*len(text.split(' ')))
        else:
            a = 0
            l = 0
            b1 = label[l][0]
            b2 = label[l][1]
            curr_label = label[l][2]
            s = ''

            while(a < n):
                if a < b1:
                    if text[a] != ' ':
                        s = s + text[a]
                    else:
                        if len(s) > 0 and s != ' ':
                            TEXT.append(s)
                            LABELS.append('O')
                            s = ''
                elif a == b1:
                    if len(s) > 0 and s != ' ':
                        TEXT.append(s)
                        LABELS.append('O')
                        s = ''
                    if text[a] != ' ':
                        s = s + text[a]
                elif a > b1 and a < b2:
                    if text[a] != ' ':
                        s = s + text[a]
                    else:
                        TEXT.append(s)
                        LABELS.append(curr_label)
                        s = ''
                elif a == b2:
                    if len(s) > 0 and s != ' ':
                        TEXT.append(s)
                        LABELS.append(curr_label)
                    s = ''

                    # this is a special condition when our 'b2'th' pos contains some character and is not 'space'
                    # it means it's a part of another word so we decrement 'a' to step on the same character again 
                    # when 'a' will increment at the end of the loop
                    if text[a] != ' ':
                        a = a - 1

                    # checking if we are not standing at last label in the list
                    if l < m-1:
                        l = l + 1
                        b1 = label[l][0]
                        b2 = label[l][1]
                        curr_label = label[l][2]
                    else:
                        # incrementing bcz of the special condition
                        a = a + 1

                        while a < n:    
                            if text[a] != ' ':
                                s = s + text[a]
                            else:
                                TEXT.append(s)
                                LABELS.append('O')
                                s = ''
                            a = a + 1
                        # boundary condition for last word    
                        if a == n and len(s) > 0 and s!=' ':
                            TEXT.append(s)
                            LABELS.append('O')
                            s = ''
                a = a + 1
            # boundary condition for last word who's 'b2' is greater than 'n' so it's not appended yet    
            if b2 == n and len(s) > 0 and s!=' ':
                TEXT.append(s)
                LABELS.append(curr_label)
        # end of every sentence to distinguish        
        TEXT.append('[SEP]')
        LABELS.append('[SEP]')
        
    return TEXT, LABELS