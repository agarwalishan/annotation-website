import os

from ..utils import DOCUMENTS

def count_annotated_documents(inpath):
    count_annotated_doc = 0
    files = next(os.walk(inpath))[2]
    #print(files)
    for filename in sorted(files, key=str.lower, reverse=False):
        if filename.endswith('.annotated.json'):
            count_annotated_doc = count_annotated_doc + 1
    
    print(count_annotated_doc)       
    return count_annotated_doc

def main(cwd, reponame):
    inpath = cwd + '/' + reponame + '/' + DOCUMENTS
    return count_annotated_documents(inpath)