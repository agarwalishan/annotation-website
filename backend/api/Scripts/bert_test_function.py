#! usr/bin/env python3
# -*- coding:utf-8 -*-
"""
# Copyright 2018 The Google AI Language Team Authors.
# Copyright 2019 The BioNLP-HZAU Kaiyin Zhou
# Time:2019/04/08
"""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from datetime import datetime
import collections
import os
import pickle
from absl import logging
from .bert import modeling
from .bert import optimization
from .bert import tokenization
import tensorflow as tf
from . import metrics
import numpy as np
import csv



class InputExample(object):
  """A single training/test example for simple sequence classification."""

  def __init__(self, guid, texta, textb, textc, labela=None, labelb=None, labelc=None):
    """Constructs a InputExample.

    Args:
      guid: Unique id for the example.
      text_a: string. The untokenized text of the first sequence. For single
        sequence tasks, only this sequence must be specified.
      label: (Optional) string. The label of the example. This should be
        specified for train and dev examples, but not for test examples.
    """
    self.guid = guid
    self.text_a = texta
    self.text_b = textb
    self.text_c = textc
    self.label_a = labela
    self.label_b = labelb
    self.label_c = labelc

class PaddingInputExample(object):
  """Fake example so the num input examples is a multiple of the batch size.

  When running eval/predict on the TPU, we need to pad the number of examples
  to be a multiple of the batch size, because the TPU requires a fixed batch
  size. The alternative is to drop the last batch, which is bad because it means
  the entire output data won't be generated.

  We use this class instead of `None` because treating `None` as padding
  battches could cause silent errors.
  """

class InputFeatures(object):
  """A single set of features of data."""

  def __init__(self,
               input_ids,
               mask,
               segment_ids,
               label_ids,
               is_real_example=True):
    self.input_ids = input_ids
    self.mask = mask
    self.segment_ids = segment_ids
    self.label_ids = label_ids
    self.is_real_example = is_real_example

class DataProcessor(object):
    """Base class for data converters for sequence classification data sets."""

    def get_train_examples(self, data_dir):
        """Gets a collection of `InputExample`s for the train set."""
        raise NotImplementedError()

    def get_dev_examples(self, data_dir):
        """Gets a collection of `InputExample`s for the dev set."""
        raise NotImplementedError()

    def get_labels(self):
        """Gets the list of labels for this data set."""
        raise NotImplementedError()

    @classmethod
    def _read_data_orig(cls,input_file):
        """Read a BIO data!"""
        rf = open(input_file,'r')
        lines = [];words = [];labels = []
        for line in rf:
            word = line.strip().split(' ')[0]
            label = line.strip().split(' ')[-1]
            # here we dont do "DOCSTART" check
            if word == '[SEP]':
              #words.append(word)
              #labels.append(label)
              
              #l = ' '.join([label for label in labels if len(label) > 0])
              #w = ' '.join([word for word in words if len(word) > 0])
              
              l = ' '.join(labels)
              w = ' '.join(words)
              lines.append((l,w))
              words=[]
              labels = []
            else:  
                words.append(word)
                labels.append(label)
        rf.close()
        return lines
      
    @classmethod
    def _read_data(cls, input_data):
        TEXT = input_data[0]
        LABELS = input_data[1]
        
        lines = []
        words= []
        labels = []

        for (word, label) in zip(TEXT, LABELS):
            if word == '[SEP]':
                l = ' '.join(labels)
                w = ' '.join(words)
                lines.append((l,w))
                words=[]
                labels = []
            else:  
                words.append(word)
                labels.append(label)

        return lines
      

class NerProcessor(DataProcessor):
    
    def __init__(self):
        self.LABELS = ["[PAD]", "[SEP]", "[CLS]","O", "X"]

    def get_train_examples(self, data):
        return self._create_example(self._read_data(data[0]),
            self._read_data(data[1]), 
            self._read_data(data[2]), "train"
        )

    def get_dev_examples(self, data):
        return self._create_example(self._read_data(data[0]),
            self._read_data(data[1]),
            self._read_data(data[2]), "dev"
        )

    def get_test_examples(self,data):
        return self._create_example(self._read_data(data[0]),
            self._read_data(data[1]),
            self._read_data(data[2]), "test"
        )

    def set_labels(self, Labels_dir):
        temp = []
        with open(Labels_dir+'/list_containing_labels.csv') as filep:
            csvreader = csv.reader(filep)
            for row in csvreader:
                temp.append(str(row[0].strip()))
        self.LABELS.extend(temp)

    def get_labels(self):
        """
        here "X" used to represent "##eer","##soo" and so on!
        "[PAD]" for padding
        :return:
        """
        return self.LABELS

    def _create_example(self, lines, prefix, suffix, set_type):
        
        if len(prefix) != len(lines):
            print("ERROR length of input files don't match",set_type)

        if len(suffix) != len(lines):
            print("ERROR length of input files don't match",set_type)

        examples = []
        for i in range(len(lines)):
            guid = "%s-%s" % (set_type, i)

            texta = tokenization.convert_to_unicode(prefix[i][1])
            labela = tokenization.convert_to_unicode(prefix[i][0])

            textb = tokenization.convert_to_unicode(lines[i][1])
            labelb = tokenization.convert_to_unicode(lines[i][0])

            textc = tokenization.convert_to_unicode(suffix[i][1])
            labelc = tokenization.convert_to_unicode(suffix[i][0])

            examples.append(InputExample(guid=guid, texta=texta, textb=textb, textc=textc, labela=labela, labelb=labelb, labelc=labelc))
        return examples    


def convert_single_example(ex_index, example, label_list, max_seq_length, tokenizer, mode, Middle_output):
    """
    :param ex_index: example num
    :param example:
    :param label_list: all labels
    :param max_seq_length:
    :param tokenizer: WordPiece tokenization
    :param mode:
    :return: feature

    IN this part we should rebuild input sentences to the following format.
    example:[Jim,Hen,##son,was,a,puppet,##eer]
    labels: [I-PER,I-PER,X,O,O,O,X]

    """
    label_map = {}
    #here start with zero this means that "[PAD]" is zero
    for (i,label) in enumerate(label_list):
        label_map[label] = i
    with open(Middle_output+"/label2id.pkl",'wb') as w:
        pickle.dump(label_map,w)

    textlista = example.text_a.split(' ')
    labellista = example.label_a.split(' ')
    #remta, remla = textlista.pop(), labellista.pop()

    token_a = []
    label_a = []

    for i,(word,label) in enumerate(zip(textlista,labellista)):
        token = tokenizer.tokenize(word)
        token_a.extend(token)
        for i,_ in enumerate(token):
            if i==0:
                label_a.append(label)
            else:
                label_a.append("X")

    textlistb = example.text_b.split(' ')
    labellistb = example.label_b.split(' ')

    token_b = []
    label_b = []

    for i,(word,label) in enumerate(zip(textlistb,labellistb)):
        token = tokenizer.tokenize(word)
        token_b.extend(token)
        for i,_ in enumerate(token):
            if i==0:
                label_b.append(label)
            else:
                label_b.append("X")
    
    textlistc = example.text_c.split(' ')
    labellistc = example.label_c.split(' ')
    #remtc, remlc = textlistc.pop(), labellistc.pop()

    token_c = []
    label_c = []

    for i,(word,label) in enumerate(zip(textlistc,labellistc)):
        token = tokenizer.tokenize(word)
        token_c.extend(token)
        for i,_ in enumerate(token):
            if i==0:
                label_c.append(label)
            else:
                label_c.append("X")
    
    token_a, label_a, token_b, label_b, token_c, label_c = _truncate_seq_pair(token_a, label_a, token_b, label_b, token_c, label_c, (max_seq_length-3), ex_index)

    ntokens = []
    segment_ids = []
    label_ids = []

    ntokens.append('[CLS]')
    segment_ids.append(1)
    label_ids.append(label_map['[CLS]'])
    for i, token in enumerate(token_a):
        ntokens.append(token)
        segment_ids.append(1)
        label_ids.append(label_map[label_a[i]])

    ntokens.append('[SEP]')
    segment_ids.append(1)
    label_ids.append(label_map['[SEP]'])

    for i, token in enumerate(token_b):
        ntokens.append(token)
        segment_ids.append(0)
        label_ids.append(label_map[label_b[i]])

    ntokens.append('[SEP]')
    segment_ids.append(0)
    label_ids.append(label_map['[SEP]'])

    for i, token in enumerate(token_c):
        ntokens.append(token)
        segment_ids.append(1)
        label_ids.append(label_map[label_c[i]])


    input_ids = tokenizer.convert_tokens_to_ids(ntokens)
    mask = [1]*len(input_ids)

    while len(input_ids) < max_seq_length:
        input_ids.append(0)
        mask.append(0)
        segment_ids.append(0)
        label_ids.append(0)
        ntokens.append("[PAD]")
    
    assert len(input_ids) == max_seq_length
    assert len(mask) == max_seq_length
    assert len(segment_ids) == max_seq_length
    assert len(label_ids) == max_seq_length
    assert len(ntokens) == max_seq_length
    if ex_index < 3:
        logging.info("*** Example ***")
        logging.info("guid: %s" % (example.guid))
        logging.info("tokens: %s" % " ".join(
            [tokenization.printable_text(x) for x in ntokens]))
        logging.info("input_ids: %s" % " ".join([str(x) for x in input_ids]))
        logging.info("input_mask: %s" % " ".join([str(x) for x in mask]))
        logging.info("segment_ids: %s" % " ".join([str(x) for x in segment_ids]))
        logging.info("label_ids: %s" % " ".join([str(x) for x in label_ids]))
    feature = InputFeatures(
        input_ids=input_ids,
        mask=mask,
        segment_ids=segment_ids,
        label_ids=label_ids,
    )
    # we need ntokens because if we do predict it can help us return to original token.
    return feature,ntokens,label_ids

def filed_based_convert_examples_to_features(examples, label_list, max_seq_length, tokenizer, output_file, Middle_output, mode=None):
    writer = tf.python_io.TFRecordWriter(output_file)
    batch_tokens = []
    batch_labels = []
    for (ex_index, example) in enumerate(examples):
        if ex_index % 5000 == 0:
            logging.info("Writing example %d of %d" % (ex_index, len(examples)))
        feature,ntokens,label_ids = convert_single_example(ex_index, example, label_list, max_seq_length, tokenizer, mode, Middle_output)
        batch_tokens.extend(ntokens)
        batch_labels.extend(label_ids)
        def create_int_feature(values):
            f = tf.train.Feature(int64_list=tf.train.Int64List(value=list(values)))
            return f

        features = collections.OrderedDict()
        features["input_ids"] = create_int_feature(feature.input_ids)
        features["mask"] = create_int_feature(feature.mask)
        features["segment_ids"] = create_int_feature(feature.segment_ids)
        features["label_ids"] = create_int_feature(feature.label_ids)
        tf_example = tf.train.Example(features=tf.train.Features(feature=features))
        writer.write(tf_example.SerializeToString())
    # sentence token in each batch
    writer.close()
    return batch_tokens,batch_labels

def file_based_input_fn_builder(input_file, seq_length, is_training, drop_remainder):
    name_to_features = {
        "input_ids": tf.FixedLenFeature([seq_length], tf.int64),
        "mask": tf.FixedLenFeature([seq_length], tf.int64),
        "segment_ids": tf.FixedLenFeature([seq_length], tf.int64),
        "label_ids": tf.FixedLenFeature([seq_length], tf.int64),

    }
    def _decode_record(record, name_to_features):
        example = tf.parse_single_example(record, name_to_features)
        for name in list(example.keys()):
            t = example[name]
            if t.dtype == tf.int64:
                t = tf.to_int32(t)
            example[name] = t
        return example

    def input_fn(params):
        batch_size = params["batch_size"]
        d = tf.data.TFRecordDataset(input_file)
        if is_training:
            d = d.repeat()
            d = d.shuffle(buffer_size=100)
        d = d.apply(tf.data.experimental.map_and_batch(
            lambda record: _decode_record(record, name_to_features),
            batch_size=batch_size,
            drop_remainder=drop_remainder
        ))
        return d
    return input_fn

# all above are related to data preprocess
ct = [0]
def _truncate_seq_pair(token_a, label_a, token_b, label_b, token_c, label_c, max_seq_length, index):

    if len(token_b) > max_seq_length:
        ct[0] = ct[0] + 1
        print("TOTAL COUNT TILL NOW : ",ct[0])
        print("SEQUENCE LENGTH SHOULD BE ATLEAST GREATER THAN ACTUAL SEQUENCE b")
        print("INDEX OF SENTENCE ", index)
        #print("TOKENIZED SENTENCE ", token_b)
        print()
        token_a = []
        label_a = []
        token_b = token_b[:max_seq_length]
        label_b = label_b[:max_seq_length]
        #token_b[-1] = '^'
        #label_b[-1] = 'O'
        token_c = []
        label_c = []
        return token_a, label_a, token_b, label_b, token_c, label_c

    while len(token_a) + len(token_b) + len(token_c) > max_seq_length:
        if len(token_a) >= len(token_c):
            remta = token_a.pop(0)
            remla = label_a.pop(0)
        else:
            remtc = token_c.pop()
            remlc = label_c.pop()
    return token_a, label_a, token_b, label_b, token_c, label_c

# Following i about the model

def hidden2tag(hiddenlayer,numclass):
    linear = tf.keras.layers.Dense(numclass,activation=None)
    return linear(hiddenlayer)

# def crf_loss(logits,labels,mask,num_labels,mask2len):
#     """
#     :param logits:
#     :param labels:
#     :param mask2len:each sample's length
#     :return:
#     """
#     #TODO
#     with tf.variable_scope("crf_loss"):
#         trans = tf.get_variable(
#                 "transition",
#                 shape=[num_labels,num_labels],
#                 initializer=tf.contrib.layers.xavier_initializer()
#         )
    
#     log_likelihood,transition = tf.contrib.crf.crf_log_likelihood(logits,labels,transition_params =trans ,sequence_lengths=mask2len)
#     loss = tf.math.reduce_mean(-log_likelihood)
   
#     return loss,transition

def softmax_layer(logits,labels,num_labels,mask):
    logits = tf.reshape(logits, [-1, num_labels])
    labels = tf.reshape(labels, [-1])
    mask = tf.cast(mask,dtype=tf.float32)
    one_hot_labels = tf.one_hot(labels, depth=num_labels, dtype=tf.float32)
    loss = tf.losses.softmax_cross_entropy(logits=logits,onehot_labels=one_hot_labels)
    loss *= tf.reshape(mask, [-1])
    loss = tf.reduce_sum(loss)
    total_size = tf.reduce_sum(mask)
    total_size += 1e-12 # to avoid division by 0 for all-0 weights
    loss /= total_size
    # predict not mask we could filtered it in the prediction part.
    probabilities = tf.math.softmax(logits, axis=-1)
    predict = tf.math.argmax(probabilities, axis=-1)
    return loss, predict, probabilities


def create_model(bert_config, is_training, input_ids, mask,
                 segment_ids, labels, num_labels, use_one_hot_embeddings, Crf, Max_seq_length):
    model = modeling.BertModel(
        config = bert_config,
        is_training=is_training,
        input_ids=input_ids,
        input_mask=mask,
        token_type_ids=segment_ids,
        use_one_hot_embeddings=use_one_hot_embeddings
        )

    output_layer = model.get_sequence_output()
    #output_layer shape is
    if is_training:
        output_layer = tf.keras.layers.Dropout(rate=0.1)(output_layer)
    logits = hidden2tag(output_layer,num_labels)
    # TODO test shape
    logits = tf.reshape(logits,[-1,Max_seq_length,num_labels])
    # if Crf:
    #     # mask2len = tf.reduce_sum(mask,axis=1)
    #     # loss, trans = crf_loss(logits,labels,mask,num_labels,mask2len)
    #     # predict,viterbi_score = tf.contrib.crf.crf_decode(logits, trans, mask2len)
    #     # return (loss, logits,predict, tf.range(10))
    # else:
    loss,predict,probabilities  = softmax_layer(logits, labels, num_labels, mask)

    return (loss, logits, predict, probabilities)

def model_fn_builder(bert_config, num_labels, init_checkpoint, learning_rate,
                     num_train_steps, num_warmup_steps, use_tpu,
                     use_one_hot_embeddings, Crf, Max_seq_length):
    def model_fn(features, labels, mode, params):
        logging.info("*** Features ***")
        for name in sorted(features.keys()):
            logging.info("  name = %s, shape = %s" % (name, features[name].shape))
        input_ids = features["input_ids"]
        mask = features["mask"]
        segment_ids = features["segment_ids"]
        label_ids = features["label_ids"]
        is_training = (mode == tf.estimator.ModeKeys.TRAIN)
        # if Crf:
        #     (total_loss, logits,predicts, probabilities) = create_model(bert_config, is_training, input_ids,
        #                                                     mask, segment_ids, label_ids,num_labels, 
        #                                                     use_one_hot_embeddings, Crf, Max_seq_length)

        # else:
        (total_loss, logits, predicts, probabilities) = create_model(bert_config, is_training, input_ids,
                                                            mask, segment_ids, label_ids,num_labels, 
                                                            use_one_hot_embeddings, Crf, Max_seq_length)
        tvars = tf.trainable_variables()
        scaffold_fn = None
        initialized_variable_names=None
        if init_checkpoint:
            (assignment_map, initialized_variable_names) = modeling.get_assignment_map_from_checkpoint(tvars,init_checkpoint)
            tf.train.init_from_checkpoint(init_checkpoint, assignment_map)
            if use_tpu:
                def tpu_scaffold():
                    tf.train.init_from_checkpoint(init_checkpoint, assignment_map)
                    return tf.train.Scaffold()
                scaffold_fn = tpu_scaffold
            else:

                tf.train.init_from_checkpoint(init_checkpoint, assignment_map)
        logging.info("**** Trainable Variables ****")
        for var in tvars:
            init_string = ""
            if var.name in initialized_variable_names:
                init_string = ", *INIT_FROM_CKPT*"
            logging.info("  name = %s, shape = %s%s", var.name, var.shape,
                            init_string)

        

        if mode == tf.estimator.ModeKeys.TRAIN:
            train_op = optimization.create_optimizer(total_loss, learning_rate, num_train_steps, num_warmup_steps, use_tpu)
            output_spec = tf.contrib.tpu.TPUEstimatorSpec(
                mode=mode,
                loss=total_loss,
                train_op=train_op,
                scaffold_fn=scaffold_fn)

        elif mode == tf.estimator.ModeKeys.EVAL:
            def metric_fn(label_ids, logits,num_labels,mask):
                predictions = tf.math.argmax(logits, axis=-1, output_type=tf.int32)
                cm = metrics.streaming_confusion_matrix(label_ids, predictions, num_labels, weights=mask)
                return {
                    "confusion_matrix":cm
                }
                #
            eval_metrics = (metric_fn, [label_ids, logits, num_labels, mask])
            output_spec = tf.contrib.tpu.TPUEstimatorSpec(
                mode=mode,
                loss=total_loss,
                eval_metrics=eval_metrics,
                scaffold_fn=scaffold_fn)
        else:
            output_spec = tf.contrib.tpu.TPUEstimatorSpec(
                mode=mode, predictions={"predicts": predicts, "probabilities": probabilities}, scaffold_fn=scaffold_fn
            )
        return output_spec

    return model_fn


# def _write_base(batch_tokens,id2label,prediction,batch_labels,wf,i):
#     token = batch_tokens[i]
#     predict = id2label[prediction]
#     true_l = id2label[batch_labels[i]]
#     #if token!="[PAD]" and token!="[CLS]" and true_l!="X":
#     if token!="[PAD]" and true_l!="X":    
#         #
#         if predict=="X" and not predict.startswith("##"):
#             predict="O"
#         line = "{}\t{}\t{}\n".format(token,true_l,predict)
#         wf.write(line)

def _write_base_(batch_tokens,id2label,prediction,batch_labels,wf,i,probabilities):
    token = batch_tokens[i]
    predict = id2label[prediction]
    true_l = id2label[batch_labels[i]]

    #output_line = "\t".join(str(class_probability) for class_probability in probabilities)
    pro = np.sort(probabilities)[::-1]
    ratio = pro[0]/pro[1]
    output_line = str(ratio)

    #if token!="[PAD]" and token!="[CLS]" and true_l!="X":
    if token!="[PAD]" and true_l!="X":    
        #
        if predict=="X" and not predict.startswith("##"):
            predict="O"
        line = "{}\t{}\t{}\t{}\n".format(token,true_l,predict,output_line)
        wf.write(line)

def Writer(output_predict_file,result,batch_tokens,batch_labels,id2label, Crf):
    with open(output_predict_file,'w') as wf:

        # if  Crf:
        #     predictions  = []
        #     for m,pred in enumerate(result):
        #         predictions.extend(pred["predicts"])
        #     for i,prediction in enumerate(predictions):
        #         _write_base(batch_tokens,id2label,prediction,batch_labels,wf,i)
                
        # else:
        for i,prediction in enumerate(result):
            _write_base_(batch_tokens,id2label,prediction["predicts"],batch_labels,wf,i,prediction['probabilities'])
            
# def display():
#     print("data_dir:",FLAGS.data_dir)
#     print("labels_dir:",FLAGS.labels_dir)
#     print("task name:",FLAGS.task_name)
#     print("do lower case:",FLAGS.do_lower_case)
#     print("bert_config_file:",FLAGS.bert_config_file)
#     print("model_output_dir:",FLAGS.model_output_dir)
#     print("test_output_dir:",FLAGS.test_output_dir)
#     print("vocab_file:",FLAGS.vocab_file)
#     print("init_checkpoint:",FLAGS.init_checkpoint)
#     print("max_seq_length:",FLAGS.max_seq_length)
#     print("do_train:",FLAGS.do_train)
#     print("do_eval:",FLAGS.do_eval)
#     print("do_predict:",FLAGS.do_predict)
#     print("train_batch_size:",FLAGS.train_batch_size)
#     print("predict_batch_size:",FLAGS.predict_batch_size)
#     print("crf:",FLAGS.crf)
#     print("middle_output:",FLAGS.middle_output)
#     print("learning_rate:",FLAGS.learning_rate)
#     print("num_train_epochs:",FLAGS.num_train_epochs)
#     print("warmup_proportion:",FLAGS.warmup_proportion)

def runfunc(Data_dir=None, Bert_config_file=None, Task_name='NER', Vocab_file=None, Model_output_dir=None, Test_output_dir=None, 
    Labels_dir=None, Init_checkpoint=None, Do_lower_case=False, Max_seq_length=256, Do_train=False, Do_eval=True, Do_predict=True,
    Train_batch_size=12, Eval_batch_size=8, Predict_batch_size=8, Learning_rate=5e-5, Num_train_epochs=3.0, Warmup_proportion=0.1,
    Save_checkpoints_steps=4000, Iterations_per_loop=1000, Use_tpu=False, Tpu_name=None, Gcp_project=None, Master=None,
    Num_tpu_cores=8, Middle_output=None, Crf=False):

    #display()
    
    logging.set_verbosity(logging.INFO)
    processors = {"ner": NerProcessor}
    # if not FLAGS.do_train and not FLAGS.do_eval:
    #     raise ValueError("At least one of `do_train` or `do_eval` must be True.")
    bert_config = modeling.BertConfig.from_json_file(Bert_config_file)
    if Max_seq_length > bert_config.max_position_embeddings:
        raise ValueError(
            "Cannot use sequence length %d because the BERT model "
            "was only trained up to sequence length %d" %
            (Max_seq_length, bert_config.max_position_embeddings))
    task_name = Task_name.lower()
    if task_name not in processors:
        raise ValueError("Task not found: %s" % (task_name))
    processor = processors[task_name]()

    processor.set_labels(Labels_dir)

    label_list = processor.get_labels()
    
    tokenizer = tokenization.FullTokenizer(
        vocab_file=Vocab_file, do_lower_case=Do_lower_case)
    tpu_cluster_resolver = None
    if Use_tpu and Tpu_name:
        tpu_cluster_resolver = tf.contrib.cluster_resolver.TPUClusterResolver(
            Tpu_name, zone=Tpu_zone, project=Gcp_project)
    is_per_host = tf.contrib.tpu.InputPipelineConfig.PER_HOST_V2
    run_config = tf.contrib.tpu.RunConfig(
        cluster=tpu_cluster_resolver,
        master=Master,
        model_dir=Model_output_dir,
        save_checkpoints_steps=Save_checkpoints_steps,
        tpu_config=tf.contrib.tpu.TPUConfig(
            iterations_per_loop=Iterations_per_loop,
            num_shards=Num_tpu_cores,
            per_host_input_for_training=is_per_host))
    train_examples = None
    num_train_steps = None
    num_warmup_steps = None

    if Do_train:
        train_examples = processor.get_train_examples(Data_dir['TRAIN'])

        num_train_steps = int(
            len(train_examples) / Train_batch_size * Num_train_epochs)
        num_warmup_steps = int(num_train_steps * Warmup_proportion)
    model_fn = model_fn_builder(
        bert_config=bert_config,
        num_labels=len(label_list),
        init_checkpoint=Init_checkpoint,
        learning_rate=Learning_rate,
        num_train_steps=num_train_steps,
        num_warmup_steps=num_warmup_steps,
        use_tpu=Use_tpu,
        use_one_hot_embeddings=Use_tpu,
        Crf=Crf,
        Max_seq_length=Max_seq_length)
    estimator = tf.contrib.tpu.TPUEstimator(
        use_tpu=Use_tpu,
        model_fn=model_fn,
        config=run_config,
        train_batch_size=Train_batch_size,
        eval_batch_size=Eval_batch_size,
        predict_batch_size=Predict_batch_size)


    if Do_train:
        train_file = os.path.join(Test_output_dir, "train.tf_record")
        _,_ = filed_based_convert_examples_to_features(
            train_examples, label_list, Max_seq_length, tokenizer, train_file, Middle_output)
        logging.info("***** Running training *****")
        logging.info("  Num examples = %d", len(train_examples))
        logging.info("  Batch size = %d", Train_batch_size)
        logging.info("  Num steps = %d", num_train_steps)
        train_input_fn = file_based_input_fn_builder(
            input_file=train_file,
            seq_length=Max_seq_length,
            is_training=True,
            drop_remainder=True)
        current_time = datetime.now()
        estimator.train(input_fn=train_input_fn, max_steps=num_train_steps)
        print("******************************************")
        print("Training took time ", datetime.now() - current_time)
        print("******************************************")
    if Do_eval:
        eval_examples = processor.get_dev_examples(Data_dir['VALIDATE'])
        eval_file = os.path.join(Test_output_dir, "eval.tf_record")
        batch_tokens,batch_labels = filed_based_convert_examples_to_features(
            eval_examples, label_list, Max_seq_length, tokenizer, eval_file, Middle_output)

        logging.info("***** Running evaluation *****")
        logging.info("  Num examples = %d", len(eval_examples))
        logging.info("  Batch size = %d", Eval_batch_size)
        # if FLAGS.use_tpu:
        #     eval_steps = int(len(eval_examples) / FLAGS.eval_batch_size)
        # eval_drop_remainder = True if FLAGS.use_tpu else False
        eval_input_fn = file_based_input_fn_builder(
            input_file=eval_file,
            seq_length=Max_seq_length,
            is_training=False,
            drop_remainder=False)
        result = estimator.evaluate(input_fn=eval_input_fn)
        output_eval_file = os.path.join(Test_output_dir, "eval_results.txt")
        with open(output_eval_file,"w") as wf:
            logging.info("***** Eval results *****")
            confusion_matrix = result["confusion_matrix"]
            p,r,f = metrics.calculate(confusion_matrix,len(label_list))
            wf.write("Precision: {}".format(str(p)))
            wf.write('\n')
            wf.write("Recall: {}".format(str(r)))
            wf.write('\n')
            wf.write("F1: {}".format(str(f)))
            wf.write('\n')
            logging.info("***********************************************")
            logging.info("********************P = %s*********************",  str(p))
            logging.info("********************R = %s*********************",  str(r))
            logging.info("********************F = %s*********************",  str(f))
            logging.info("***********************************************")


    if Do_predict:
        current_time = datetime.now()
        with open(Middle_output+'/label2id.pkl', 'rb') as rf:
            label2id = pickle.load(rf)
            id2label = {value: key for key, value in label2id.items()}

        predict_examples = processor.get_test_examples(Data_dir['TEST'])

        predict_file = os.path.join(Test_output_dir, "predict.tf_record")
        batch_tokens,batch_labels = filed_based_convert_examples_to_features(predict_examples, label_list,
                                                 Max_seq_length, tokenizer,
                                                 predict_file, Middle_output)

        logging.info("***** Running prediction*****")
        logging.info("  Num examples = %d", len(predict_examples))
        logging.info("  Batch size = %d", Predict_batch_size)

        predict_input_fn = file_based_input_fn_builder(
            input_file=predict_file,
            seq_length=Max_seq_length,
            is_training=False,
            drop_remainder=False)

        result = estimator.predict(input_fn=predict_input_fn)
        output_predict_file = os.path.join(Test_output_dir, "output_predictions.txt")

        #here if the tag is "X" means it belong to its before token, here for convenient evaluate use
        # conlleval.pl we  discarding it directly
        Writer(output_predict_file,result,batch_tokens,batch_labels,id2label, Crf)
        print("******************************************")
        print("Testing took time ", datetime.now() - current_time)
        print("******************************************")
