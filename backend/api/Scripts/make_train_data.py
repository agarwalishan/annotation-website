#!/usr/bin/env python
# coding: utf-8

# In[1]:


import os
import json

# In[2]:


from . import prefix, suffix, text_to_word_level
from ..utils import DOCUMENTS

# In[3]:


# cwd = os.getcwd()
# if cwd.rfind('\\') != -1:
#     cwd = cwd[0:cwd.rfind('\\')]
# else:
#     cwd = cwd[0:cwd.rfind('/')]

# In[4]:

# In[5]:


def displaydata(docname_list, data):
    if len(docname_list)!=len(data):
        print("Length does not matches")
        return
    for i in range(len(data)):
        print(i,data[i],"\t",docname_list[i])


# In[6]:


def update_labels(TEXT, LABELS):
    for i in range(len(TEXT)):
        if TEXT[i] == ',' or TEXT[i] == '(' or TEXT[i] == ')' or TEXT[i] == '~' or TEXT[i] == 'and' or TEXT[i] == '&':
            LABELS[i] = 'O'
    return TEXT, LABELS    


# In[7]:


def split_data(data_length, data, prefix_data, suffix_data, trainingpercent, validationpercent):
    
    train_length = int((trainingpercent/100)*data_length)
    valid_length = int((validationpercent/100)*data_length) + train_length
    
    train_data = data[0:train_length]
    train_prefix_data = prefix_data[0:train_length]
    train_suffix_data = suffix_data[0:train_length]
    
    valid_data = data[train_length:valid_length]
    valid_prefix_data = prefix_data[train_length:valid_length]
    valid_suffix_data = suffix_data[train_length:valid_length]
    
    test_data = data[valid_length:]
    test_prefix_data = prefix_data[valid_length:]
    test_suffix_data = suffix_data[valid_length:]
    
    return [train_data, train_prefix_data, train_suffix_data, valid_data, valid_prefix_data, valid_suffix_data, test_data, test_prefix_data, test_suffix_data]


# In[8]:


def sort_l(sub_li):
    return(sorted(sub_li, key = lambda x: x[0]))

def sort_labels(t_l):
    for i in range(len(t_l)):
        t_l[i]['labels'] = sort_l(t_l[i]['labels'])
    return t_l    


# In[9]:


def extract_text_labels(data):
    text_labels = []
    for i in range(len(data)):
        dic = {}
        dic['text'] = data[i]['text']
        dic['labels'] = data[i]['labels']
        text_labels.append(dic)
    return text_labels


# In[10]:


def read_annotated_documents(inpath):
    docname_list = []
    data = []
    count_doc = 0
    files = next(os.walk(inpath))[2]
    #print(files)
    for filename in sorted(files, key=str.lower, reverse=False):
        if filename.endswith('.annotated.json'):
            json_list = [json.loads(line) for line in open(inpath + '/' + filename, 'r',encoding='utf-8')]
            data.extend(json_list)
            docname_list.extend([filename for i in range(len(json_list))])
            json_list = []
            count_doc = count_doc + 1
    #print(len(docname_list),len(data))
    return docname_list, data, count_doc


# In[11]:


def make_data(cwd, reponame, modelname, versionname):
    inpath = cwd + '/' + reponame + '/' + DOCUMENTS
    
    outpath = cwd + '/'+ reponame +'/' + modelname + '/' + versionname
    if not os.path.exists(outpath):
        #print("Invalid path {}".format(outpath))
        return (None, "Invalid path {}".format(outpath))
        
    docname_list, data, count_doc = read_annotated_documents(inpath)
    #displaydata(docname_list,data)
    if len(data)==0:
        #print("NO annotated documents in {}".format(inpath))
        return (False, "NO annotated documents in {}".format(inpath))
    
    path_to_parameters = cwd + '/' + reponame + '/' + modelname + '/' + 'parameters.json'
    if not os.path.exists(path_to_parameters):
        #print("Invalid path to parameters file {}".format(path_to_parameters))
        return (None, "Invalid path to parameters file {}".format(path_to_parameters))
    else:
        with open(path_to_parameters, 'r', encoding='utf-8') as fjson:
            parameters = json.load(fjson)

    trainingpercent = parameters['trainingpercent']
    validationpercent = parameters['validationpercent']
    
    if not os.path.exists(outpath + '/data_train'):
        os.mkdir(outpath+'/data_train')
    
    with open(outpath + '/data_train/' + 'noofdocsused.txt','w', encoding="utf-8") as fp:
        fp.write("{}".format(count_doc))
        fp.write('\n')


    #data = extract_text_labels(data)
    data = sort_labels(data)
    prefix_data = prefix.make_prefixes(docname_list, data)
    suffix_data = suffix.make_suffixes(docname_list, data)
    
    #TO DO add shuffing of data

    my_splitted_data_list = split_data(len(data), data, prefix_data, suffix_data, trainingpercent, validationpercent)
    

    word_level_splitted_data = []
    for i in my_splitted_data_list:
        TEXT, LABELS = text_to_word_level.convert_text_to_word_level(i)
        TEXT, LABELS = update_labels(TEXT, LABELS)
        word_level_splitted_data.append((TEXT, LABELS))
    
    my_data = {}
    my_data['TRAIN'], my_data['VALIDATE'], my_data['TEST'] = [], [], []
    my_data['TRAIN'].extend(word_level_splitted_data[0:3])
    my_data['VALIDATE'].extend(word_level_splitted_data[3:6])
    my_data['TEST'].extend(word_level_splitted_data[6:])

    return (True, my_data, str(count_doc))

    # my_output_filenames = ['train.txt', 'prefix_train.txt', 'suffix_train.txt', 'validate.txt', 'prefix_validate.txt',
    #                       'suffix_validate.txt', 'test.txt', 'prefix_test.txt', 'suffix_test.txt']
    
    # for i in range(len(my_output_filenames)):
    #     TEXT, LABELS = word_level_splitted_data[i][0], word_level_splitted_data[i][1] 
    #     with open(outpath+'/data_train/'+my_output_filenames[i],'w', encoding="utf-8") as f:
    #         for i in range(len(TEXT)):
    #             f.write(TEXT[i])
    #             f.write(' ')
    #             f.write(LABELS[i])
    #             f.write('\n')
    #     f.close()


