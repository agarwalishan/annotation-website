import zipfile
import json
import os
import re
import itertools
from lxml import etree


ns = {'w': 'http://schemas.openxmlformats.org/wordprocessingml/2006/main', }

FONT_AR = 0.5
SPACE_WR = 0.45
FAULTS = {'parse': set(),
          'table': set(),
          }


def docx_to_json(filename):
     document = zipfile.ZipFile(filename)
     obj_list = parser(document)
     if obj_list:
          return write_json_file(filename, obj_list)  
     else:
          return None


def fault_files():
    with open('faults.txt', 'w') as filep:
        filep.write('File Parse Faults:\n\n')
        for filename in sorted(FAULTS['parse']):
            filep.write(filename)
            filep.write('\n')
        filep.write('\n\n')
        filep.write('Table Faults:\n\n')
        for filename in sorted(FAULTS['table']):
            filep.write(filename)
            filep.write('\n')


def convert_files(filename):
     jsonfile = docx_to_json(filename)
     return jsonfile
     # if fname:
     #      FAULTS['parse'].add(fname)
     # fault_files()


def read_json_file(filename):
    with open(filename) as filep:
        obj_list = json.load(filep)
    return obj_list


def write_json_file(filename, obj_list):
    json_file = {'file': filename, 'content': obj_list}
    return json_file

def ns_tag(xml_tag):
    uri = xml_tag.split(':')[0]
    tag = xml_tag.split(':')[1]
    return '{{{0}}}{1}'.format(ns[uri], tag)


def int_property(root, attrib, default=0):
    if root is not None:
        return int(float(root.get(ns_tag(attrib), default=default)))
    else:
        return default


def str_property(root, attrib):
    if root is not None:
        return root.get(ns_tag(attrib))
    else:
        return None


def bool_property(root):
    if root is not None:
        if root.attrib:
            value = root.get(ns_tag('w:val'))
            if value == '0' or value == 'false':
                return -1
        return 1
    else:
        return 0


def get_default_size(document, default=0):
    tree = etree.parse(document.open('word/styles.xml'))
    root = tree.getroot()
    sz = root.find('w:docDefaults/w:rPrDefault/w:rPr/w:sz', namespaces=ns)
    return int_property(sz, 'w:val', default)


def get_page_margin(document):
    tree = etree.parse(document.open('word/document.xml'))
    root = tree.getroot()
    left_margin = root.find('w:body/w:sectPr/w:pgMar', namespaces=ns)
    return int_property(left_margin, 'w:left')


def update_indent(obj_list, document):
    left_margin = get_page_margin(document)
    for obj in obj_list:
        if obj['type'] != 'table':
            obj['indent'] = int(left_margin + obj['indent'])
        else:
            obj['indent'] = int(left_margin)
    return obj_list


def set_line_numbers(obj_list):
    index = 0
    table_start_index = 0
    for obj in obj_list:
        index += 1
        obj['line_no'] = index
        if obj['type'] == 'table':
            if 'start' in obj and obj['start']:
                table_start_index = obj['line_no'] - 1
                obj.pop('start', None)
            if obj['parent_no']:
                obj['parent_no'] += table_start_index
        else:
            table_start_index = 0
    return obj_list


def get_indent_parent(indent_line_tuple, current_indent, fontsize):
    line_no = 0
    possible_line = [line for (indent, line) in indent_line_tuple
                     if (current_indent - indent) > ((fontsize / 2) * 20 *
                                                     FONT_AR * SPACE_WR * 2)]
    if possible_line:
        line_no = max(possible_line)
    return line_no


def set_parent_by_indent(obj_list):
    indent = set()
    for obj in obj_list:
        if obj['type'] != 'table':
            indent.add(obj['indent'])
    indent_dict = {}
    for obj in obj_list:
        if obj['type'] != 'table':
            indent_dict[obj['indent']] = obj['line_no']
            obj['parent_no'] = get_indent_parent(indent_dict.items(),
                                                 obj['indent'],
                                                 obj['size'])
    return obj_list


def post_processing(obj_list, document):
    obj_list = set_line_numbers(obj_list)
    obj_list = update_indent(obj_list, document)
    obj_list = set_parent_by_indent(obj_list)
    return obj_list


def style_properties(document, style_id):
    par_prop = {}
    run_prop = {}
    style_tree = etree.parse(document.open('word/styles.xml'))
    style_root = style_tree.getroot()
    for style in style_root.findall('w:style', namespaces=ns):
        if style.get(ns_tag('w:styleId')) == style_id:
            basedOn = style.find('w:basedOn', namespaces=ns)
            if basedOn is not None:
                base_style = str_property(basedOn, 'w:val')
                par_prop, run_prop = style_properties(document, base_style)
            ppr = style.find('w:pPr', namespaces=ns)
            rpr = style.find('w:rPr', namespaces=ns)
            parpr, runpr = get_paragraph_properties(document, ppr)
            runpr2 = get_run_properties(document, rpr)
            runpr2 = check_properties(document, runpr2, 'run')
            runpr = update_run_boolean_properties(runpr, runpr2)
            par_prop.update(parpr)
            run_prop.update(runpr)
    return par_prop, run_prop


def update_run_boolean_properties(run_prop, runpr):
    boolean_prop = ['bold', 'italic', 'underline']
    for prop in boolean_prop:
        if prop in run_prop:
            if not runpr[prop]:
                if run_prop[prop] > 0:
                    run_prop[prop] = True
                else:
                    run_prop[prop] = False
            elif runpr[prop] > 0:
                run_prop[prop] = True
            elif runpr[prop] < 0:
                run_prop[prop] = False
        else:
            if runpr[prop] > 0:
                run_prop[prop] = True
            else:
                run_prop[prop] = False
    if runpr['size']:
        run_prop['size'] = runpr['size']
    return run_prop


def get_run_properties(document, root):
    run_prop = {}
    if root is not None:
        size = root.find('w:sz', namespaces=ns)
        rstyle = root.find('w:rStyle', namespaces=ns)
        if rstyle is not None:
            style_id = str_property(rstyle, 'w:val')
            parpr, runpr = style_properties(document, style_id)
            run_prop.update(runpr)
        run_prop['bold'] = bool_property(root.find('w:b', namespaces=ns))
        run_prop['italic'] = bool_property(root.find('w:i', namespaces=ns))
        run_prop['underline'] = bool_property(root.find('w:u', namespaces=ns))
        if size is not None:
            run_prop['size'] = int_property(size, 'w:val')
    return run_prop


def get_numbering_properties(document, num_id, ilvl=0):
    par_prop = {}
    run_prop = {}
    abstract_id = None
    num_tree = etree.parse(document.open('word/numbering.xml'))
    num_root = num_tree.getroot()
    for num in num_root.findall('w:num', namespaces=ns):
        if int_property(num, 'w:numId') == num_id:
            abstractnumid = num.find('w:abstractNumId', namespaces=ns)
            if abstractnumid is not None:
                abstract_id = int_property(abstractnumid, 'w:val')
    if abstract_id is not None:
        for abstractnum in num_root.findall('w:abstractNum', namespaces=ns):
            if int_property(abstractnum, 'w:abstractNumId') == abstract_id:
                for lvl in abstractnum.findall('w:lvl', namespaces=ns):
                    if int_property(lvl, 'w:ilvl') == ilvl:
                        ppr = lvl.find('w:pPr', namespaces=ns)
                        rpr = lvl.find('w:rPr', namespaces=ns)
                        parpr, runpr = get_paragraph_properties(document, ppr)
                        runpr2 = get_run_properties(document, rpr)
                        runpr2 = check_properties(document, runpr2, 'run')
                        runpr = update_run_boolean_properties(runpr, runpr2)
                        par_prop.update(parpr)
                        run_prop.update(runpr)
    return par_prop, run_prop


def get_paragraph_properties(document, root):
    par_prop = {}
    run_prop = {}
    indent = {}
    if root is not None:
        jc = root.find('w:jc', namespaces=ns)
        ind = root.find('w:ind', namespaces=ns)
        tab = root.find('w:tab', namespaces=ns)
        rpr = root.find('w:rPr', namespaces=ns)
        numpr = root.find('w:numPr', namespaces=ns)
        pstyle = root.find('w:pStyle', namespaces=ns)
        if pstyle is not None:
            style_id = str_property(pstyle, 'w:val')
            parpr, runpr = style_properties(document, style_id)
            par_prop.update(parpr)
            run_prop.update(runpr)
        if numpr is not None:
            id = numpr.find('w:numId', namespaces=ns)
            lvl = numpr.find('w:ilvl', namespaces=ns)
            numid = int_property(id, 'w:val')
            ilvl = int_property(lvl, 'w:val')
            parpr, runpr = get_numbering_properties(document, numid, ilvl)
            par_prop.update(parpr)
            run_prop.update(runpr)
        if jc is not None:
            par_prop['alignment'] = str_property(jc, 'w:val')
        if ind is not None:
            if ns_tag('w:left') in ind.attrib:
                indent['left'] = int_property(ind, 'w:left')
            if ns_tag('w:firstLine') in ind.attrib:
                indent['firstLine'] = int_property(ind, 'w:firstLine')
            if 'left' in indent or 'firstLine' in indent:
                par_prop['indent'] = indent
        if tab is not None:
            if str_property(tab, 'w:val') == 'left':
                par_prop['tab'] = int_property(tab, 'w:pos')
        runpr = get_run_properties(document, rpr)
        runpr = check_properties(document, runpr, 'run')
        run_prop = update_run_boolean_properties(run_prop, runpr)
    return par_prop, run_prop


def check_properties(document, properties, prop_type):
    if prop_type == 'paragraph':
        if 'alignment' not in properties:
            properties['alignment'] = 'left'
        if 'indent' in properties:
            if 'left' not in properties['indent']:
                properties['indent']['left'] = 0
            if 'firstLine' not in properties['indent']:
                properties['indent']['firstLine'] = 0
        else:
            properties['indent'] = {
                'left': 0,
                'firstLine': 0,
            }
        if 'tab' not in properties:
            properties['tab'] = 0
    elif prop_type == 'run':
        if 'bold' not in properties:
            properties['bold'] = 0
        if 'italic' not in properties:
            properties['italic'] = 0
        if 'underline' not in properties:
            properties['underline'] = 0
        if 'size' not in properties:
            properties['size'] = get_default_size(document)
    return properties


def parse_paragraph_tag(root, document):
    par_prop = {}
    runpr = {}
    run_list = []
    important = []
    text = ''
    text_type = 'sentence'
    ppr = root.find('w:pPr', namespaces=ns)
    if ppr is not None:
        parpr, runpr = get_paragraph_properties(document, ppr)
        par_prop.update(parpr)
    for run in iter(root.iterdescendants()):
        if run.tag == ns_tag('w:r'):
            run_text = ''
            run_prop = {}
            run_prop.update(runpr)
            rpr = run.find('w:rPr', namespaces=ns)
            if rpr is not None:
                runpr2 = get_run_properties(document, rpr)
                runpr2 = check_properties(document, runpr2, 'run')
                run_prop = update_run_boolean_properties(run_prop, runpr2)
            par_prop = check_properties(document, par_prop, 'paragraph')
            run_prop = check_properties(document, run_prop, 'run')
            if run.find('w:tab', namespaces=ns) is not None:
                if not text.strip():
                    if par_prop['tab']:
                        par_prop['indent']['left'] += par_prop['tab']
                    else:
                        par_prop['indent']['left'] += 720
                run_text += ' '
            t_tag = run.find('w:t', namespaces=ns)
            if t_tag is not None:
                if t_tag.text:
                    run_text += t_tag.text
            if run_text:
                whitespace = re.match('(^[\t ]*)(.*)', run_text)
                tab_count = whitespace.groups()[0].count('\t')
                space_count = whitespace.groups()[0].count(' ')
                if not run_prop['size']:
                    run_prop['size'] = get_default_size(document, default=22)
                if not text.strip():
                    custom_indent = (tab_count * 720)
                    custom_indent += (space_count * (run_prop['size'] / 2) *
                                      FONT_AR * SPACE_WR * 20)
                    par_prop['indent']['left'] += custom_indent
                text += run_text
            if run_prop['bold'] or run_prop['italic'] or run_prop['underline']:
                important += list(filter(None, re.split('[, "!?]+', run_text)))
            run_list.append(run_prop)
    par_prop = check_properties(document, par_prop, 'paragraph')
    indent = sum(par_prop['indent'].values())
    sentence = text.strip()
    sentence = ' '.join(sentence.split())
    if run_list:
        size = run_list[0]['size']
    if sentence:
        obj = {'sentence': sentence,
               'type': text_type,
               'important': important,
               'size': size,
               'indent': indent,
               'alignment': par_prop['alignment'],
               'parent_no': 0,
               }
        return obj
    else:
        return None


def table_data(root, document):
    table = []
    for row in iter(root.findall('w:tr', namespaces=ns)):
        row_list = []
        for column in iter(row.findall('w:tc', namespaces=ns)):
            col_list = []
            for paragraph in iter(column.findall('w:p', namespaces=ns)):
                obj = parse_paragraph_tag(paragraph, document)
                if obj:
                    obj['type'] = 'table'
                    obj['indent'] = 0
                    col_list.append(obj)
            row_list.append(col_list)
        table.append(row_list)
    return table


def remove_single_cell_rows(table):
    sent_list = []
    row_faults = []
    for index, row in enumerate(table):
        if len(row) < 2:
            row_faults += table.pop(index)
    for col_list in row_faults:
        for obj in col_list:
            obj['type'] = 'sentence'
            sent_list.append(obj)
    return table, sent_list


def remove_empty_rows(table):
    for index, row in enumerate(table):
        empty = True
        for column in row:
            if column:
                empty = False
        if empty:
            table.pop(index)
    return table


def remove_empty_columns(table):
    table = list(map(list, itertools.zip_longest(*table, fillvalue=[])))
    table = remove_empty_rows(table)
    table = list(map(list, itertools.zip_longest(*table, fillvalue=[])))
    return table


def effective_table(table):
    table, sent_list = remove_single_cell_rows(table)
    table = remove_empty_rows(table)
    table = remove_empty_columns(table)
    return table, sent_list


def table_indexing(table):
    index = 1
    for row in table:
        for column in row:
            for obj in column:
                if obj:
                    obj['line_no'] = index
                    index += 1
    return table


def table_orientation(table):
    orientation = ''
    if table:
        row_len = len(table)
        col_len = len(table[0])
        if row_len <= col_len:
            orientation = 'vertical'
        else:
            orientation = 'horizontal'
    return orientation


def table_headers(table, orientation):
    header_flag = False
    for row_id, row in enumerate(table):
        for col_id, column in enumerate(row):
            if orientation == 'vertical' and row_id > 0:
                try:
                    if len(table[0][col_id]) == 1:
                        for obj in column:
                            temp = table[0][col_id][0]
                            obj['parent_no'] = temp['line_no']
                            header_flag = True
                except IndexError:
                    pass
            elif orientation == 'horizontal' and col_id > 0:
                try:
                    if len(table[row_id][0]) == 1:
                        for obj in column:
                            temp = table[row_id][0][0]
                            obj['parent_no'] = temp['line_no']
                            header_flag = True
                except IndexError:
                    pass
    return table, header_flag


def remove_single_dimension_tables(table, sent_list):
    table_flag = True
    if table:
        row_len = len(table)
        col_len = len(table[0])
        if row_len < 2 or col_len < 2 or (row_len < 3 and col_len < 3):
            for row in table:
                for column in row:
                    for obj in column:
                        obj['type'] = 'sentence'
                        sent_list.append(obj)
            table = []
            table_flag = False
    return table, sent_list, table_flag


def mark_table_starting(table):
    try:
        table[0][0][0]['start'] = True
    except IndexError:
        pass
    return table


def collect_table_data(table, sent_list):
    for row in table:
        for column in row:
            for obj in column:
                if obj:
                    sent_list.append(obj)
    return sent_list


def parse_table_tag(root, document):
    table = table_data(root, document)
    table, sent_list = effective_table(table)
    orientation = table_orientation(table)
    table = table_indexing(table)
    table, hflag = table_headers(table, orientation)
    table, sent_list, tflag = remove_single_dimension_tables(table,
                                                             sent_list)
    table = mark_table_starting(table)
    if table and tflag and not hflag:
        FAULTS['table'].add(re.split('[/.]', document.filename)[-2])
    sentences = collect_table_data(table, sent_list)
    return sentences


def parser(document):
    obj_list = []
    count = 1
    tree = etree.parse(document.open('word/document.xml'))
    body = tree.getroot().find('w:body', namespaces=ns)
    for bodychild in body:
        if bodychild.tag == ns_tag('w:tbl'):
            obj_list += parse_table_tag(bodychild, document)
        elif bodychild.tag == ns_tag('w:p'):
            obj = parse_paragraph_tag(bodychild, document)
            if obj:
                obj_list.append(obj)
        count += 1
    obj_list = post_processing(obj_list, document)
    return obj_list

    