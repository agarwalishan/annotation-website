#It returns a list containing dictionary with key names "filename" and "confidence_ratio" if ran successfully

import argparse
import csv
import json
import os
from . import documentwise_predictions
from . import make_test_data
from . import bert_test_function
from django.conf import settings

max_seq_length = 512
train_batch_size = 6
learning_rate = 2e-5
num_train_epochs = 1
CRF = False
warmup_proportion = 0.1

def main(cwd, reponame, modelname, versionname, count_raw):
	init_model_dir = settings.BASE_DIR + '/api/Scripts/model/'
	path = cwd + '/' + reponame +'/'+ modelname + '/' + versionname
	path_to_labels = cwd + '/' + reponame + '/list_containing_labels.csv'
	path_to_parameters = cwd + '/' + reponame + '/' + modelname + '/' + 'parameters.json'

	if not os.path.exists(path):
		#print("Invalid path {}".format(path))
		return (None, "Invalid path {}".format(path))

	# if not os.path.exists(path+'/data_test'):
	# 	print("Testing data must be present in {}".format(path+'/data_test'))
	# 	return

	if not os.path.exists(path_to_labels):
		#print("File does not exist {}".format(path_to_labels))
		return (None, "File does not exist {}".format(path_to_labels))

	if os.stat(path_to_labels).st_size == 0:
		#print('Empty file {}'.format(path_to_labels))
		return (None, 'Empty file {}'.format(path_to_labels))

	labels_dir = cwd + '/' + reponame
	
	if not os.path.exists(path+'/output_model'):
		#print("Saved model path is not present {}".format(path+'/output_model'))
		return (None, "Saved model path is not present {}".format(path+'/output_model'))

	if not os.path.exists(path+'/output_model/middle_output'):
		#print("Labels data saved during training are not present {}".format(path+'/output_model/middle_output'))
		return (None, "Labels data saved during training are not present {}".format(path+'/output_model/middle_output'))

	Condition = make_test_data.make_data(cwd, reponame, modelname, versionname, count_raw)
	# This means either error or no files to be predicted
	if Condition[0] != True:
		return Condition

	DATA = Condition[1]

	if not os.path.exists(path + '/output_test'):
		os.mkdir(path + '/output_test')

	model_output_dir = path + '/output_model'
	middle_output = path + '/output_model/middle_output'
	test_output_dir = path + '/output_test'

	if os.path.exists(path_to_parameters) and os.stat(path_to_parameters).st_size != 0:
		with open(path_to_parameters, 'r', encoding='utf-8') as fjson:
			parameters = json.load(fjson)

	max_seq_length = parameters['max_seq_length']
	train_batch_size = parameters['train_batch_size']
	learning_rate = parameters['learning_rate']
	num_train_epochs = parameters['num_train_epochs']
	#CRF = parameters['crf']
	warmup_proportion = parameters['warmup_proportion']

	print("Run Test function(BERT)")
	
	bert_test_function.runfunc(Data_dir=DATA, Bert_config_file= init_model_dir+'bert_config.json', Task_name='NER', 
	Vocab_file= init_model_dir+'vocab.txt',
	Model_output_dir=model_output_dir, Test_output_dir=test_output_dir, Labels_dir=labels_dir,
	Init_checkpoint= init_model_dir+'bert_model.ckpt', Do_lower_case=False, Max_seq_length=max_seq_length, Do_train=False, 
	Do_eval=True,
	Do_predict=True, Train_batch_size=train_batch_size, Eval_batch_size=8, Predict_batch_size=8, Learning_rate=learning_rate, 
	Num_train_epochs=num_train_epochs, Warmup_proportion=warmup_proportion, Save_checkpoints_steps=5000, Iterations_per_loop=1000,
	Use_tpu=False, Tpu_name=None, Gcp_project=None, Master=None, Num_tpu_cores=8, Middle_output=middle_output, Crf=CRF)
	
	print("Test function finished(BERT)")

	FILES_WITH_CONFIDENCE = documentwise_predictions.make_prediction_files(cwd, reponame, modelname, versionname)
	return (True, FILES_WITH_CONFIDENCE)
#main()	
