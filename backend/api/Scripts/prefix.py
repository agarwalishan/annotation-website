import copy

def make_prefixes(docname_list,data):
    prev_docname = 'UNIQUE' 

    JSON_LIST = []
    TEXT = ''
    INT_LABELS = []

    for i in range(len(data)):
        if docname_list[i] != prev_docname:
            TEXT = '~'             # changed - to ~
            INT_LABELS = []

        # for appending current prefix

        dic = {}
        dic['text'] = TEXT
        dic['labels'] = copy.deepcopy(INT_LABELS)
        JSON_LIST.append(dic)

        text = data[i]['text']
        labels = data[i]['labels']

        # adding i'th sentence to 'TEXT' and 'INT_LABELS'
        # now the start and end position of labels will be shifted by length of the 'TEXT' + 1
        for j in range(len(labels)):
            start = labels[j][0] + len(TEXT) + 1
            end = labels[j][1] + len(TEXT) + 1
            tag = labels[j][2]
            INT_LABELS.append([start,end,tag])
        TEXT = TEXT + ' ' + text   # to add space between different lines

        # updating previous resume no
        prev_docname = docname_list[i]
    return JSON_LIST