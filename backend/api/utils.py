from os import listdir
from os.path import isfile, join
import os
import json
from PIL import Image
from io import StringIO, BytesIO
from django.core.files.images import ImageFile

FAILED = 'FAILED'
SUCCESS = 'DONE'
INPROGRESS = 'INPROGRESS'
TRAINING = 'TRAINING'
PREDICTION = 'PREDICTION'
SCORING = 'SCORING'

DEFAULT_FILES = { "files": []}
DEFAULT_LABELS = { "names": [], "descriptions": [] }

DOCUMENTS = 'documents'

def Getparentpath(path):
	if path[-1]=='/' or path[-1]=='\\':
		path = path[0:-1]
	if path.rfind('\\') != -1 and path.rfind('\\') > path.rfind('/'):
		f = path[0:path.rfind('\\')]
	else:
		f = path[0:path.rfind('/')]
	return f

def GetPredictionFilesFromVersionFolder(versionFolderPath, already_validated_files, total_predicted_files):
	#prediction Files are retreived from the version folder 
	#predictionFiles = [pos_json for pos_json in listdir(versionFolderPath) if pos_json.endswith('predictions.json')]
	# for validated_file in already_validated_files:
	# 	if validated_file in predictionFiles:
	# 		predictionFiles.remove(validated_file)

	unvalidated_files = []
	for i in range(len(total_predicted_files)):
		if total_predicted_files[i]["filename"] not in already_validated_files:
			unvalidated_files.append(total_predicted_files[i])

	unvalidated_files = sorted(unvalidated_files, key = lambda x: x["confidence_ratio"])		
	
	if len(unvalidated_files) > 0:
		file_with_least_confidence_score = unvalidated_files[0]["filename"]
	
		data = {}
		with open(versionFolderPath +'/'+ file_with_least_confidence_score, 'r', encoding='utf-8') as JSONFile:
			data['content'] = JSONFile.read()
			data['fileName'] = file_with_least_confidence_score
			data['filePath'] = join(versionFolderPath, file_with_least_confidence_score)
			#data['predictionFileNumber'] = len(predictionFiles)
		return data

	return {}

def globalvariables():
	global FAILED
	global SUCCESS
	global INPROGRESS
	global TRAINING
	global PREDICTION
	global SCORING
	global DEFAULT_FILES
	global DEFAULT_LABELS
	global DOCUMENTS

#Selects all those files which have not been previously added to the problem repo
def getDocumentsFromMasterRepo(masterRepoPath, alreadyAddedFiles):
	def path_to_dict(path, d):
		name = os.path.basename(path)

		if os.path.isdir(path):
			d['value'] = name
			d['label'] = name
			for x in os.listdir(path):
				childrenPaths = os.path.join(path,x)
				if childrenPaths not in alreadyAddedFiles:
					d['children'].append(path_to_dict(childrenPaths, {'value': '', 'children':[]}))
		else:
			del d['children']
			del d['value']
			if (path not in alreadyAddedFiles):
				d['value'] = path
				d['label'] = name
		return d
	mydict = path_to_dict(masterRepoPath, d = {'value': '', 'children':[]})
	return mydict

# to crop image
def ImageCropper(original_image):
	im = Image.open(original_image)
	width, height = im.size 
	left = 250
	top = 0
	right = width - 420
	bottom = height
	im1 = im.crop((left, top, right, bottom))
	output = BytesIO()
	im1.save(output, format="png")
	cropped_image = ImageFile(output)
	return cropped_image