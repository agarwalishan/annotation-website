from django.db import models
from .customfields import JSONField
from django_mysql.models import ListCharField
from .utils import INPROGRESS, DEFAULT_FILES, DEFAULT_LABELS
# Create your models here.
class MasterRepo(models.Model):
    id = models.AutoField(primary_key=True)
    desc = models.CharField(default="", blank=True, max_length=20)
    documents_count = models.IntegerField(default=0,blank=True)
    user_id = models.IntegerField(default=0,blank=True)
    isprocessrunning = models.BooleanField(default=False,blank=True)

class ProblemSet(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=30,default="")
    desc = models.CharField(default="", blank=True, max_length=100)
    labels = JSONField(default=DEFAULT_LABELS, blank=True)
    annotated_files_count =  models.IntegerField(default=0,blank=True)
    raw_files_count = models.IntegerField(default=0,blank=True)
    # Count of annotated files already used for training
    used_annotated_files_count = models.IntegerField(default=0, blank=True)
    master = models.ForeignKey(MasterRepo, related_name='problem', on_delete=models.CASCADE, blank=True, null=True)
    #Documents added from master repo
    documents_added = ListCharField(base_field=models.CharField(max_length=100),max_length=200,default=list,blank=True)


class ModelSet(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=30,default="")
    desc = models.CharField(default="", blank=True, max_length=20)
    problem = models.ForeignKey(ProblemSet, related_name='model', on_delete=models.CASCADE, blank=True, null=True)
    hyperparameters = JSONField(null=True, blank=True)


class Version(models.Model):

    id = models.AutoField(primary_key=True)
    #Name it will be auto incremented for each model
    name = models.CharField(max_length=20, default="",blank=True)
    precision = models.FloatField(default=0.0, blank=True)
    recall = models.FloatField(default=0.0, blank=True)
    f1_score = models.FloatField(default=0.0,blank=True)
    training_data = models.IntegerField(default=0,blank=True)
    model = models.ForeignKey(ModelSet, related_name='version', on_delete=models.CASCADE, blank=True, null=True)
    # to check if scores are calculated using this version
    has_score = models.BooleanField(default=False,blank=True)
    # to store confusion matrix of scoring(image)
    score_conf_matrix = models.ImageField(blank=True, null=True)
    # to store classification report of scoring(json)
    score_class_report = JSONField(null=True, blank=True)
    # to store total no of predicted documents used for scoring till now
    docs_used_for_scoring = models.IntegerField(default=0,blank=True)
    # to store confusion matrix of training result(image)
    conf_matrix = models.ImageField(blank=True, null=True)
    # to store classification report of training result(json)
    class_report = JSONField(null=True, blank=True)
    # Prediction Files validated
    validated_files = JSONField(default=DEFAULT_FILES, blank=True)
    # Total Predicted files names (validated and unvalidated both)
    total_predicted_files = JSONField(default=DEFAULT_FILES, blank=True)

class Status(models.Model):
    processid = models.AutoField(primary_key=True)
    tasktype = models.CharField(max_length=20, default="",blank=True)
    taskstatus = models.CharField(max_length=20, default=INPROGRESS,blank=True)
    starttime = models.TimeField(auto_now_add=True)
    endtime = models.TimeField(null=True,blank=True)
    message = models.CharField(default="", blank=True, max_length=100)
    mastername = models.CharField(max_length=30, default="",blank=True,null=True)
    problemname = models.CharField(max_length=30, default="",blank=True,null=True)
    modelname = models.CharField(max_length=30, default="",blank=True,null=True)
    versionname = models.CharField(max_length=30, default="",blank=True,null=True)
    user_id = models.IntegerField(default=0,blank=True)
