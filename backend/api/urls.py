from rest_framework import routers
from . import views
from .views import ( ModelViewSet, VersionViewSet, ProblemViewSet, MasterRepoViewSet, StatusViewSet, Scoring, check_count_before_prediction, Prediction, Training
, getPredictionFilesByVersionId, FileHandling, validatePredictionFile, GetDocumentsFromMasterRepo, Addnewlabels)
from django.urls import path, include

router = routers.DefaultRouter()
router.register(r'model', ModelViewSet)
router.register(r'version', VersionViewSet)
router.register(r'problem', ProblemViewSet)
router.register(r'master', MasterRepoViewSet)
router.register(r'status', StatusViewSet)


urlpatterns = [
    path('api/', include(router.urls)),
    path('scoring/', Scoring, name='scoring'),
    path('checkrawcount/', check_count_before_prediction, name='checkrawcount'),
    path('prediction/', Prediction, name='prediction'),
    path('training/', Training, name='training'),
    path('version/prediction/files/<str:verId>',getPredictionFilesByVersionId),
    path('file', FileHandling),
    path('validate',validatePredictionFile),
    path('document/files/<str:probId>',GetDocumentsFromMasterRepo),
    path('move/document/files',views.MoveFilesFromMasterToProblem),
    path('addnewlabels/',Addnewlabels, name='addnewlabels')
]
