import * as actionTypes from './actions';

const initialState = {
    isnewproblemadded: false,
    isnewmodeladded: false,
    isnewversionadded: false,
    isdocumentsimported: false,
    isannotationdone: false,
    ispredictiondone: false,
    isvalidationdone: false,
    isscoringdone: false
} 

const reducer = ( state=initialState, action ) => {
    switch(action.type) {
        case actionTypes.NEW_PROBLEM_ADDED:
            return {
                ...state,
                isnewproblemadded: !state.isnewproblemadded
            }
        case actionTypes.NEW_MODEL_ADDED:
            return {
                ...state,
                isnewmodeladded: !state.isnewmodeladded
            }
        case actionTypes.DOCUMENTS_IMPORTED:
            return {
                ...state,
                isdocumentsimported: !state.isdocumentsimported
            }
        case actionTypes.NEW_VERSION_ADDED:
            return {
                ...state,
                isnewversionadded: !state.isnewversionadded
            }
        case actionTypes.ANNOTATION:
            return {
                ...state,
                isannotationdone: !state.isannotationdone
            }
        case actionTypes.PREDICTION:
            return {
                ...state,
                ispredictiondone: !state.ispredictiondone
            }
        case actionTypes.VALIDATION:
            return {
                ...state,
                isvalidationdone: !state.isvalidationdone
            }
        case actionTypes.SCORING:
            return {
                ...state,
                isscoringdone: !state.isscoringdone
            }    
    }
    return state
}

export default reducer;