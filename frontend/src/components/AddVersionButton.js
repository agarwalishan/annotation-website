// To add train/add version for current model
import React, { Component } from 'react';
import classes from './UI/Toolbar.module.css'
import {useState} from 'react';
import axios from 'axios';
import { withRouter } from 'react-router';
import Lclasses from './UI/Layout.module.css';
import { connect } from 'react-redux';
import * as actionTypes from '../store/actions';
import ResourceNotAvailableDialog from './ModelPage/ModelDashboard/ResourceNotAvailableDialog'

function AddVersionButton(props) {
    
    const [openResourcenotavailabledialog, setResourcenotavailabledialog] = useState(false)
    const [resourcedialogmessage, setResourcemessage] = useState('')

    const handleResourcenotavailableDialogClose = () => {
        setResourcenotavailabledialog(false)
        //setResourcemessage('')
    }

    const addnewversion = () => {
        axios.get('http://127.0.0.1:8000/api/master/?user_id=' + localStorage.getItem('UserId'))
        .then(res => {
            if (res.data[0].isprocessrunning) {
                setResourcemessage('Training')
                setResourcenotavailabledialog(true)
            }
            else {
                let newpath = '?modelid='+ props.match.params.moid;
                axios.get('http://127.0.0.1:8000/training/' + newpath)
                .then(response => {
                    props.onVersionAdded();
                })
                .catch(err => {
                    console.log(err);
                });
            }
        })
    }

    let addversiondialog = (
        <div>
            <div onClick={addnewversion} className={Lclasses.Addpagebutton}>
            {/* <span className={Lclasses.Subtoolbarplusicon}>+</span> Train new version */}
                <div style={{float: 'left'}}>
                    <span className={Lclasses.Subtoolbarplusicon}>+</span>
                </div> 
                <div style={{float: 'left', marginLeft: '2px', marginTop: '2px'}}>
                    Train new version
                </div>
            </div>
        </div>
        )
    return (
        <React.Fragment>
            {addversiondialog}
            <ResourceNotAvailableDialog 
              open={openResourcenotavailabledialog} 
              handleClose={handleResourcenotavailableDialogClose}
              message={resourcedialogmessage} 
            />
        </React.Fragment>
    );
}

const mapDispatchToProps = dispatch => {
    return {
        onVersionAdded: () => dispatch({type: actionTypes.NEW_VERSION_ADDED})
    }
};
  
export default connect(null, mapDispatchToProps)(withRouter(AddVersionButton));