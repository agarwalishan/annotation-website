import React from 'react';
//import Configs from 'json-loader../../../../../globalconfig.json';

const superagent = require('superagent');
//const route =  Configs.apiRoot + '/files/';

export const SaveAnnotated = (params) => {
    const File = params.File;           // file object
    const Id = params.id;               // id of raw file
    let Fname = params.name;          // new name if .raw then change to .annotated 
    const ParentId = params.parentId;   // parent id
    const route = params.route;

    superagent.delete(route + Id)
    .then(res => {
        console.log("DELETED",res);
    })
    .catch(err => {
        console.log(err);
    });

    if (Fname.lastIndexOf('.raw.json')!== -1) {
        Fname = Fname.replace('raw','annotated');
    }

    superagent.post(route).field('type', 'file').field('parentId', ParentId)
    .attach('files', File, Fname)
    .then(res => {
        console.log("POST", res);      
    })
    .catch(err => {
        console.log(err);
    });
};