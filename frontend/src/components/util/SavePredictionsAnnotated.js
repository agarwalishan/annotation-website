import React from 'react';
//import Configs from 'json-loader../../../../../globalconfig.json';
const superagent = require('superagent');
//const route =  Configs.apiRoot + '/files/';

const getGreatGrandParentId = (data) => {
    const ancestorsArray = data.ancestors;
    if (ancestorsArray.length < 3) {
        console.log("Prediction file was saved at wrong place");
        return null;
    }
    return ancestorsArray.slice(-3)[0].id;
}

export const SavePredictionsAnnotated = (params) => {
    const updatedFile = params.File;
    const Id = params.id;
    // const Fname = params.name;
    // const ParentId = params.parentId;
    const route = params.route;

    const data;
    superagent.get(route + Id)
    .then(res => {
        console.log(res);
        data = res;
    })
    .catch(err => {
        console.log(err);
    });

    const GreatGrandParentId = getGreatGrandParentId(data);
    if (!GreatGrandParentId) {
        return false;
    }

    const predictedFname = data.name;
    let tindex = predictedFname.lastIndexOf(".predictions.json");
    if (tindex === -1) {
        console.log("File name does not ends with .predictions.json");
        return false;
    }

    const annotatedFname = predictedFname.slice(0, tindex) + '.annotated.json';
    const rawFname = predictedFname.slice(0, tindex) + '.raw.json';

    let childrenArray;
    superagent.get(route + GreatGrandParentId + '/children')
    .then(res => {
        console.log(res);
        childrenArray = res.items;
    })
    .catch(err => {
        console.log(err);
    });

    let rawFnameId = null;
    for (let i = 0; i < childrenArray.length; i++ ) {
        let currelement = childrenArray[i];
        if (currelement.type == 'file' && currelement.name === rawFname) {
            rawFnameId = currelement.id;
        }
    }

    if (rawFnameId !== null) {
        superagent.delete(route + rawFnameId)
        .then(res => {
            console.log("DELETED",res);
        })
        .catch(err => {
            console.log(err);
        });
    }

    superagent.post(route).field('type', 'file').field('parentId', GreatGrandParentId)
        .attach('files', updatedFile, annotatedFname)
        .then(res => {
            console.log("POST",res);
        })
        .catch(err => {
            console.log(err);
        });

    return true;
};

