import React , { Component } from 'react';
import { withRouter } from 'react-router';
import classes from '../UI/Layout.module.css';
import { DropdownButton, Dropdown } from 'react-bootstrap';

class ProblemSideOptionPanel extends Component {

  problemreposelect = (modelid, modelname) => {
    this.props.history.push(this.props.match.url + '/' + modelid + '/' + modelname);
  }

  render() {
    const models = this.props.parentstate.models;
    return (
      <React.Fragment>
      <div className={classes.sidenav}>
        <DropdownButton id="dropdown-basic-button" title="Go to Model" size='sm' variant={"btn btn-outline-dark"}>
          {models.map(model => {
            return <Dropdown.Item key={model.id} onClick={() => this.problemreposelect(model.id, model.name)}>{model.name}</Dropdown.Item>
          })}
        </DropdownButton>
      </div>
    </React.Fragment>
    );
  }
}

export default withRouter(ProblemSideOptionPanel);