import React from 'react';
import CheckboxTree from 'react-checkbox-tree';
import Button from '@material-ui/core/Button';
import axios from 'axios';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import { connect } from 'react-redux';
import * as actionTypes from '../../store/actions';

class Tree extends React.Component {
  constructor() {
    super();

    this.state = {
      checked: [],
      expanded: [],
      nodes :[]
    };
  }
  onMove = () => {
    console.log(this.state.checked)
    axios({
      method: 'post',
      url: 'http://127.0.0.1:8000/move/document/files',
      data: {
        probId: this.props.probId,
        files : this.state.checked
      }
    })
    .then(() => {
      this.props.onDocumentsImported();
    });
    this.props.handleClose();
  }

  componentDidMount() {
    var problemId = this.props.probId;
    axios.get('http://127.0.0.1:8000/document/files/' + problemId )
      .then(response => {
        var nodes = [];
        nodes.push(response.data);
        console.log(response.data )
        this.setState({nodes:nodes});
      })
  };
  render() {
    
    return (
      <React.Fragment>
        <DialogContent>
          <div>
            Files selected {this.state.checked.length}
          </div>
          <CheckboxTree
            nodes={this.state.nodes}
            checked={this.state.checked}
            expanded={this.state.expanded}
            onCheck={checked => this.setState({ checked })}
            onExpand={expanded => this.setState({ expanded })}
          />
        </DialogContent>
        <DialogActions>
          <Button disabled={this.state.checked.length > 0 ? false: true} onClick={this.onMove} color="primary" autoFocus>
            Move
          </Button>
        </DialogActions>
      </React.Fragment>

    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
      onDocumentsImported: () => dispatch({type: actionTypes.DOCUMENTS_IMPORTED})
  }
};

export default connect(null, mapDispatchToProps)(Tree);
