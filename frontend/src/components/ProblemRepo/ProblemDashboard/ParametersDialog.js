import React from 'react';
import Button from '@material-ui/core/Button';
//import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import classes from '../../UI/Layout.module.css';

export default function ParametersDialog(props) {

    const makeParametername = (name) => {
        console.log(name);
        if (name === 'crf') {
            return 'Output Layer';
        }
        let newname = ''
        for ( var i=0; i < name.length; i++) {
            if (name.charAt(i) === '_') {
                newname+= ' '
            }
            else {
                if (i == 0) {
                    newname+= name.charAt(i).toUpperCase();
                }
                else {
                    newname+= name.charAt(i);
                }
            }
        }
//        newname = name.replace(/_/g,' ');
        return newname;
    }

    const makeParametervalue = (name, value) => {
        if (name === 'crf') {
            if (value == 'False')
                return 'Softmax Layer';
            return 'Conditional Random Fields'
        }
        return value
    }
    let dialogtype = null;
    if (props.hyperparameters) {
        let parameters = props.hyperparameters;
        dialogtype = (
            <div>
            <Dialog open={props.open} onClose={props.handleClose} aria-labelledby="form-dialog-title">
            <DialogContent className={classes.Parametersdialog}>
              <DialogContentText>
                  {Object.entries(parameters).map(([parametername, parametervalue]) => (
                    <div key={parametername}>
                        <span className={classes.Parametername}>{makeParametername(parametername)}: </span>
                        <span className={classes.Parametervalue}>{makeParametervalue(parametername, parametervalue)}</span>
                    </div>
                  ))}
              </DialogContentText>
              </DialogContent>
              <DialogActions>
                  <Button onClick={props.handleClose} color="primary">
                      Okay
                  </Button>
              </DialogActions>
            </Dialog>
          </div>
          );
    }
  return (
    <React.Fragment>
        {dialogtype}
    </React.Fragment>
  );
}