import React, { Component } from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TablePagination from '@material-ui/core/TablePagination';
import { withStyles } from '@material-ui/core/styles';
import Spinner from '../../UI/Spinner'
import { withRouter } from 'react-router';
import Button from '@material-ui/core/Button';
import ParametersDialog from './ParametersDialog';

const StyledTableCell = withStyles((theme) => ({
    head: {
      backgroundColor: '#034f84',
      color: theme.palette.common.white
    },
    body: {
      fontSize: 14,
    },
    root: {
      // borderBottom: '0px',
      // padding: '1px 24px 4px 16px',
      //padding: '5px 0px 5px 0px',
      //textAlign: 'center',
      //maxWidth: '0px'
      textAlign: 'center'
    }
  }))(TableCell);

const StyledTableRow = withStyles((theme) => ({
  root: {
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.action.hover,
    },
    // '&:nth-of-type(even)': {
    //   backgroundColor: 'rgba(0, 0, 0, 0.01)'
    // }
  },
}))(TableRow);

const StyledButton = withStyles((theme) => ({
  root: {
    padding: '2px 0px 2px 0px',
    textAlign: 'center',
    fontSize: '0.7rem',
    textTransform: 'none',
    maxWidth: '0px',
    letterSpacing: '0.08857em',
    textTransform: 'capitalize'
  }
}))(Button);

//TODO return version name
function calculateBestVersion (modelobj) {
  const versionArray = modelobj.version;
  if (versionArray.length == 0 ) {
    return '-';
  }
  let bestversionname = versionArray[0].name;
  let bestf1_score = versionArray[0].f1_score;
  for (var i=1; i < versionArray.length; i++) {
    if (versionArray[i].f1_score > bestf1_score) {
      bestversionname = versionArray[i].name;
      bestf1_score = versionArray[i].f1_score;
    }
  }
  return bestversionname;
} 

class DisplayModels extends Component {
  state = {
    // page: 0,
    // rowsPerPage: 3,
    opendialog: false,
    hyperparameters: null
  }

  handleDialogClose = () => {
    this.setState({opendialog: false});
    this.setState({hyperparameters: null});
  }

  // handleChangeRowsPerPage = (event) => {
  //   this.setState({ rowsPerPage: parseInt(event.target.value) });
  //   this.setState({ page: 0 });
  // };

  // handleChangePage = (event, newPage) => {
  //   this.setState({ page: newPage });
  // };
 
  showParameters = (e, hyperparameters) => {
    this.setState({hyperparameters: hyperparameters});
    this.setState({opendialog: true});
  }

  render() {
    const models = this.props.grandparentstate.models;
    console.log(models);
    // const rowsPerPage = this.state.rowsPerPage;
    // const page = this.state.page;

    let loadedTable = <Spinner />;
    if (this.props.grandparentstate.error) {
        loadedTable = <h1> Models data can't be loaded </h1>;
    }
    if (this.props.grandparentstate.dataLoaded) {
        loadedTable = (
        <div>
        <React.Fragment>
          {/* <Toolbar /> */}
          <Table size="small" aria-label="dense table" style={{borderCollapse: 'separate', borderSpacing: '1px'}}>
            <TableHead>
              <StyledTableRow>
                <StyledTableCell >Model</StyledTableCell>
                <StyledTableCell >No of versions</StyledTableCell>
                <StyledTableCell >Best Version</StyledTableCell>
                <StyledTableCell >Used Files</StyledTableCell>
                <StyledTableCell >Unused Files</StyledTableCell>
                <StyledTableCell >Model Parameters</StyledTableCell>
              </StyledTableRow>
            </TableHead>
            <TableBody>
              {models.map((model) => (
                <StyledTableRow
                  align="left"
                  key={model.id}>
                  <StyledTableCell >{model.name}</StyledTableCell>
                  <StyledTableCell >{model.version.length}</StyledTableCell>
                  <StyledTableCell >{calculateBestVersion(model)}</StyledTableCell>
                  <StyledTableCell >{model.version.length > 0 ? model.version.slice(-1)[0].training_data : 0}</StyledTableCell>
                  <StyledTableCell >{this.props.grandparentstate.totalannotatedfiles - (model.version.length > 0 ? model.version.slice(-1)[0].training_data : 0)}</StyledTableCell>
                  <StyledTableCell >
                  <StyledButton onClick={(e) => this.showParameters(e, model.hyperparameters)} variant="contained" color="primary">
                    View
                  </StyledButton>
                  </StyledTableCell>
                </StyledTableRow>
              ))}
            </TableBody>
          </Table>
        </React.Fragment>
        {/* <TablePagination
          rowsPerPageOptions={[3]}
          component="div"
          count={models.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onChangePage={this.handleChangePage}
          onChangeRowsPerPage={this.handleChangeRowsPerPage}
        /> */}
      </div>
        );
    }    
    return (
        <React.Fragment>
            {loadedTable}
            <ParametersDialog 
              open={this.state.opendialog} 
              handleClose={this.handleDialogClose} 
              hyperparameters={this.state.hyperparameters}
            />
        </React.Fragment>
    )
    }
};

export default DisplayModels;