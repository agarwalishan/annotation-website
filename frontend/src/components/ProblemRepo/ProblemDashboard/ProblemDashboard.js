import React from 'react'
import clsx from 'clsx'
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import DisplayModels from './DisplayModels'
import Chart from './Chart' 
import FileExplorer from '../../FileExplorer';

import useMyStyle from '../../UI/Dashboardstyles';
import Lclasses from '../../UI/Layout.module.css';

export default function MasterDashboard(props) {
    const classes = useMyStyle();
    const fixedHeightPaper = clsx(classes.paper, classes.fixedHeight);
    const fixedHeightPaperExp = clsx(classes.paper, classes.fixedHeightExp);

    return (
        <React.Fragment>
            <div className={Lclasses.DashboardContainer}>
                <Grid container spacing={3}>
                    <Grid item xs={12} lg={6} md={6}>
                        {/* {FileExplorer} */}
                        <Paper className={fixedHeightPaperExp}>
                            <FileExplorer parentContextFolder={props.parentContextFolder} probId={props.probId}  limitedCap={true}/>
                        </Paper>
                    </Grid>
                    <Grid item xs={12} lg={6} md={6}>
                        <Grid container spacing={1}>
                            {/* {stats of models and versions} */}
                            <Grid item xs={12}>
                                <Paper className={fixedHeightPaper}>
                                    <DisplayModels grandparentstate={props.parentstate}/>
                                </Paper>
                            </Grid>
                            {/* {charts} */}
                            <Grid item xs={12}>
                                <Paper className={fixedHeightPaper}>
                                    <Chart grandparentstate={props.parentstate}/>
                                </Paper>
                            </Grid>
                        </Grid>    
                    </Grid>
                </Grid>
            </div>
        </React.Fragment>
    );
}
