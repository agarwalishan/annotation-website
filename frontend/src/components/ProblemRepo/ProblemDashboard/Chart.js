import React from 'react';
import { useTheme } from '@material-ui/core/styles';
import { LineChart, Line, XAxis, YAxis, Label, ResponsiveContainer } from 'recharts';
import Title from './Title';
import { useState, useEffect } from 'react';
import { withRouter } from 'react-router';

// Generate Sales Data
function calculateBestVersionScore(modelobj) {
  const versionArray = modelobj.version;
  if (versionArray.length === 0 ) {
    return 0;
  }
  //let bestversionid = versionArray[0].id;
  let bestf1_score = versionArray[0].f1_score;
  for (var i=1; i < versionArray.length; i++) {
    if (versionArray[i].f1_score > bestf1_score) {
      // bestversionid = versionArray[i].id;
      bestf1_score = versionArray[i].f1_score;
    }
  }
  return bestf1_score;
} 

function createData(model) {
  let f1_score = calculateBestVersionScore(model)
  let modelname = model.name
  return { modelname, f1_score };
}

function Chart(props) {
  const theme = useTheme();

  let data = [];

  data = props.grandparentstate.models.map((model) => {
    return createData(model)
  })

  return (
    <React.Fragment>
      <Title>F1_SCORES</Title>
      <ResponsiveContainer>
        <LineChart
          data={data}
          margin={{
            top: 16,
            right: 16,
            bottom: 0,
            left: 24,
          }}
        >
          <XAxis dataKey="modelname" stroke={theme.palette.text.secondary} />
          <YAxis stroke={theme.palette.text.secondary}>
            <Label
              angle={270}
              position="left"
              style={{ textAnchor: 'middle', fill: theme.palette.text.primary }}
            >
              F1Score (%)
            </Label>
          </YAxis>
          <Line type="monotone" dataKey="f1_score" stroke={theme.palette.primary.main} dot={false} />
        </LineChart>
      </ResponsiveContainer>
    </React.Fragment>
  );
}

export default Chart;