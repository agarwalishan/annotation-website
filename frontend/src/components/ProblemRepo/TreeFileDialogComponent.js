import React from 'react';
import IconButton from '@material-ui/core/IconButton';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';
import FileCopyIcon from '@material-ui/icons/FileCopy';
import TreeFileExplorer from './TreeFileExplorer';
import ImportExportIcon from '@material-ui/icons/ImportExport';
import Lclasses from '../UI/Layout.module.css'
import Importicon from '../UI/Importiconblue.png';

export default function FileTreeComponent(props) {
  const [open, setOpen] = React.useState(false);
  const theme = useTheme();

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div>
      <div onClick={handleClickOpen} className={Lclasses.Addpagebutton}>
        {/* <ImportExportIcon style={{width: '20', height:'20'}} className={Lclasses.Subtoolbarimporticon}/> Import documents */}
        <div style={{float: 'left'}}>
          {/* <ImportExportIcon style={{width: '20', height:'20'}} className={Lclasses.Subtoolbarimporticon}/> */}
          <img src={Importicon} height={20} width={20} />
        </div> 
        <div style={{float: 'left', marginLeft: '2px', marginTop: '2px'}}>
            Import documents
        </div>
      </div>
      <Dialog
        open={open}
        onClose={handleClose}
        fullWidth
      >
        <DialogTitle id="alert-dialog-title">{"Select documents from Master document folder"}</DialogTitle>
            <TreeFileExplorer probId={props.probId} handleClose={handleClose}/>
       
      </Dialog>
    </div>
  );
}