// To add view and add labels to existing problem repo
import React, { Component, useEffect } from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import TextField from '@material-ui/core/TextField';
import Tclasses from '../UI/Toolbar.module.css'
import {useState} from 'react';
import axios from 'axios';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import { withRouter } from 'react-router';
import Lclasses from '../UI/Layout.module.css'
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import { withStyles } from '@material-ui/core/styles';

const StyledTableCell = withStyles((theme) => ({
    head: {
      backgroundColor: 'rgb(29, 127, 172)',
      color: theme.palette.common.white
    },
    body: {
      fontSize: 14,
    },
    root: {
        padding: '7px',
        borderBottom: '0px',
        textAlign: 'center'
    }
  }))(TableCell);

const StyledTableRow = withStyles((theme) => ({
  root: {
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.action.hover,
    },
  },
}))(TableRow);

const StyledTable = withStyles((theme) => ({
    root: {
        width: '90%',
        margin: 'auto'
    },
  }))(Table);

//   const StyledButton = withStyles((theme) => ({
//     root: {
//         minWidth: '4px'
//     },
//   }))(Button);

function AddNewLabels(props) {
    
    const [opendialog, setdialog] = useState(false);
    const [errors, setError] = useState([{value: ''}]);
    const [labels, setLabel] = useState([{ value: null }]);
    const [existinglabels, setExistinglabels] = useState({'names':[], 'descriptions': []});
    const [finalerrormessage, setFinalErrormessage] = useState(null);
    const [descriptions, setDescription] = useState([{ value: null }]);

    // To finally update labels if form submitted successfully
    const putlabelsdata = (labels) => {
        console.log(labels);
        axios.put('http://127.0.0.1:8000/api/problem/' + props.match.params.pid + '/', labels)
        .then(response => {
            console.log("new labels added to current problem repo", response);
            // When new problem repo's labels are updated in Rdatabase we also have to edit it's related csv file in upload folder
            // based on the response returned
            let newpath = '?problemid=' + response.data.id;
            axios.get('http://127.0.0.1:8000/addnewlabels/' + newpath)
            .then(response => {
                console.log(response);
            })
            .catch(err => {
                console.log(err, 'in adding new labels to upload folder');
            })
            //To Do update/re-render related componenets
        })
        .catch((err) => {
            console.log(err, 'in adding new labels to RDBMS');
        })
    }

    const loadexistinglabels = () => {
        axios.get('http://127.0.0.1:8000/api/problem/' + props.match.params.pid + '/')
        .then(response => {
            //console.log(response);
            let existinglabel = response.data.labels;
            console.log(existinglabel);
            setExistinglabels(existinglabel);
            setdialog(true);
            setLabel([{ value: null }]);
            setError([{value: ''}]);
            setFinalErrormessage(null);
            setDescription([{ value: null }]);
        })
    }

    const isproblemformvalid = () => {
        setFinalErrormessage(null);
        // if no label entered
        if (labels.length == 0) {
            setFinalErrormessage('Enter atleast one label');
            return false;
        }
        // if label error value is not empty string
        for (var i=0; i< errors.length; i++) {
            if (errors[i].value.length > 0) {
                return false;
            }
        }
        // if any label's value is null
        for (var i=0; i<labels.length; i++) {
            if (labels[i].value == null || labels[i].value == '') {
                setFinalErrormessage("Label name cant be empty");
                return false;        
            }
        }
        return true;
    }

    const handleSubmit = () => {
        if (!isproblemformvalid()) {
            return
        }
        setdialog(false);
        // make json data to send
        let labelsData = {};
        let labelArray = [];
        let DescArray = [];
        // add existing labelsdata
        labelArray = [...existinglabels["names"]]
        DescArray = [...existinglabels["descriptions"]]

        for ( var i=0; i < labels.length ; i++) {
            labelArray.push(labels[i].value);
            if (descriptions[i].value == null) {
                DescArray.push("");
            }
            else {
                DescArray.push(descriptions[i].value);
            }
        }
        labelsData["names"] = labelArray;
        labelsData["descriptions"] = DescArray;
        let FinallabelsData = {};
        FinallabelsData["labels"] = JSON.stringify(labelsData);
        console.log(FinallabelsData); 
        putlabelsdata(FinallabelsData);
    }
    // to add new label and initialize it's error
    function handleAdd() {
        const values = [...labels];
        values.push({ value: null });
        setLabel(values);

        const error = [...errors];
        error.push({value: ''});
        setError(error);
        
        const desc = [...descriptions];
        desc.push({ value: null });
        setDescription(desc);
    }
    // to remove label and associated error
    function handleRemove(i) {
        const values = [...labels];
        values.splice(i, 1);
        setLabel(values);

        const error = [...errors];
        error.splice(i, 1);
        setError(error);

        const desc = [...descriptions];
        desc.splice(i, 1);
        setDescription(desc);
    }
    // to check if label name entered is unique or not
    function newvalueunique(values, index, newvalue) {
        let valid = true;
        for( var i=0; i < values.length; i++ ) {
            if (index != i && values[i].value === newvalue) {
                return false;
            }
        }
        for ( var i=0; i< existinglabels['names'].length; i++) {
            if (existinglabels['names'][i] === newvalue) {
                return false;
            }
        }
        return valid;
    }
    // to change value of label and simultaneously check for error
    function handleChange(i, event) {
        event.preventDefault();
        const values = [...labels];
        values[i].value = event.target.value;
        setLabel(values);

        const value = event.target.value;
        let error = [...errors];

        error[i].value = '';
        const fixed_error_message = 'Label name must be unique and only alphabets are allowed';
        var letters = /^[A-Za-z]+$/;
        if ((!value=='' && !value.match(letters)) || !newvalueunique(values, i, value)) {
            error[i].value = fixed_error_message;
        }

        setError(error);
    }

    function handleDescChange(i, event) {
        event.preventDefault();
        const desc = [...descriptions];
        desc[i].value = event.target.value;
        setDescription(desc);
    }

        let buttonstyles = {
            //background: 'linear-gradient(45deg, #2196F3 30%, #21CBF3 90%)',
            borderRadius: 3,
            border: 0,
            //color: 'white',
            height: 25,
            padding: '0px 0px 0px 0px',
            //boxShadow: '0 2px 3px 1px rgba(33, 203, 243, .3)',
            marginBottom: '20px',
            textTransform: 'Capitalize',
            minWidth: 70,
            marginTop: '15px',
            fontSize: '0.8em'
          }
        
        let Names = [...existinglabels["names"]];
        let Descs = [...existinglabels["descriptions"]];
        let addnewlabelsdialog = (
            <div>
                <div onClick={loadexistinglabels} className={Lclasses.Addpagebutton}>
                {/* <span className={Lclasses.Subtoolbarplusicon}>+</span> Add new labels */}
                    <div style={{float: 'left'}}>
                        <span className={Lclasses.Subtoolbarplusicon}>+</span>
                    </div> 
                    <div style={{float: 'left', marginLeft: '2px', marginTop: '2px'}}>
                        Add/View Labels
                    </div>
                </div>
                <Dialog open={opendialog} onClose={() => setdialog(false)} fullWidth={true}
                    maxWidth = {'sm'}>
                    <DialogContent className={Tclasses.Addproblemdialog}>
                        <DialogContentText>
                        {/* <div style={{textAlign: 'left', marginBottom: '20px', fontSize: '15px', color: '#000000'}}>
                            Existing labels
                        </div> */}
                        <div style={{textAlign: 'left', marginBottom: '10px', fontSize: '15px', color: '#000000'}}>
                            <StyledTable >
                            <TableHead>
                            <StyledTableRow>
                                <StyledTableCell >#</StyledTableCell>
                                <StyledTableCell >Name</StyledTableCell>
                                <StyledTableCell >Description</StyledTableCell>
                            </StyledTableRow>
                            </TableHead>
                            <TableBody>
                            {Names.map((name, index) => (
                                <StyledTableRow
                                key={index}>
                                    <StyledTableCell >{index+1}</StyledTableCell>
                                    <StyledTableCell >{name}</StyledTableCell>
                                    <StyledTableCell >{Descs[index]}</StyledTableCell>
                                </StyledTableRow>
                            ))}
                            </TableBody>
                        </StyledTable>
                        </div>
                        {/* <div style={{textAlign: 'left', marginBottom: '20px', fontSize: '15px', color: '#000000'}}>
                            Add new labels
                        </div> */}
                        <div style={{marginLeft: '30px'}}>
                        <Button variant="outlined" color="primary" type="button" onClick={() => handleAdd()} style={buttonstyles}>
                            Add new
                        </Button>
                    {labels.map((label, idx) => {
                        return (
                        <div key={`${label}-${idx}`}>
                            <TextField
                                autoFocus
                                margin="dense"
                                type="text"
                                placeholder="Enter label name"
                                value={label.value || ""}
                                onChange={e => handleChange(idx, e)}
                            />
                        <TextField
                                autoFocus
                                margin="dense"
                                type="text"
                                placeholder="Enter Description"
                                // value={descriptions[idx].value || ""}
                                onChange={e => handleDescChange(idx, e)}
                                style={{marginLeft: '35px'}}
                            />
                        
                        <IconButton aria-label="delete" type="button" onClick={() => handleRemove(idx)} style={{marginLeft: '20px'}}>
                            <DeleteIcon fontSize="medium" />
                        </IconButton>
                        <div className={Tclasses.helperText}>{idx ? errors[idx].value: ''}</div>
                        </div>
                        );
                    })}
                    </div>
                        </DialogContentText>
                    </DialogContent>
                <DialogActions>
                    <div style={{textAlign: 'left', marginRight: '5px', marginBottom: '1px', fontSize: '12px', color:'#FF0000'}}>
                            {finalerrormessage}
                    </div>
                    <Button onClick={() => setdialog(false)} color="primary">
                      Okay
                  </Button>
                  <Button onClick={handleSubmit} color="primary">
                      Add
                  </Button>
              </DialogActions>
            </Dialog>
          </div>)
        return (
            <React.Fragment>
                {addnewlabelsdialog}
            </React.Fragment>
        );
}

export default withRouter(AddNewLabels);