import React, { Component } from 'react';
import Dashboard from './ProblemDashboard/ProblemDashboard';
import SideOptionPanel from './ProblemSideOptionPanel';
import classes from '../UI/Layout.module.css';
import { withRouter } from 'react-router';
import axios from 'axios';
import TreeFileDialogComponent from './TreeFileDialogComponent';
import AddModelButton from '../AddModelButton';
import AddNewLabels from './AddNewLabels';
import { connect } from 'react-redux';
var path = require('path');

class ProblemRepo extends Component {

  state = {
    models: [],
    dataLoaded: false,
    error: null,
    totalannotatedfiles: 0
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (this.props.store_isnewmodeladded !== prevProps.store_isnewmodeladded) {
      axios.get('http://127.0.0.1:8000/api/problem/'+ this.props.match.params.pid + '/')
      .then(response => {
        this.setState({ models: response.data.model});
        this.setState({totalannotatedfiles: response.data.annotated_files_count})
        this.setState({dataLoaded: true});
      })
      .catch((err) => {
          this.setState({error: err});
      });
    }
  }

  componentDidMount () {
    axios.get('http://127.0.0.1:8000/api/problem/'+ this.props.match.params.pid + '/')
    .then(response => {
      console.log(response);
      this.setState({ models: response.data.model});
      this.setState({totalannotatedfiles: response.data.annotated_files_count})
      this.setState({dataLoaded: true});
    })
    .catch((err) => {
        this.setState({error: err});
    });
  }

  render() {
    console.log('[in problemrepo]',this.props);
    var parentContextFolder = path.join("/",this.props.match.params.mid ,this.props.match.params.pid ,'documents');
    return (
      <div className={classes.Pages}>
        <div className={classes.Subtoolbar}>
          <SideOptionPanel parentstate={this.state} />
          <div >
            <TreeFileDialogComponent probId={this.props.match.params.pid} />
          </div>
          <div >
            <AddModelButton/>
          </div>
          <div>
            <AddNewLabels />
          </div>
        </div>
        <div className={classes.Dashboard}>
            <Dashboard parentstate={this.state} parentContextFolder={parentContextFolder} probId={this.props.match.params.pid}/>
        </div>
      </div>
    );
  }
};

const mapStateToProps = state => {
  return {
    store_isnewmodeladded: state.isnewmodeladded
  }
};

export default connect(mapStateToProps)(withRouter(ProblemRepo));
