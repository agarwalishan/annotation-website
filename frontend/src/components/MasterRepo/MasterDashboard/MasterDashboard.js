import React from 'react'
import clsx from 'clsx'
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import FileExplorer from '../../FileExplorer';
import useMyStyle from '../../UI/Dashboardstyles';
import Lclasses from '../../UI/Layout.module.css';
import DisplayProblems from './DisplayProblems';

export default function MasterDashboard(props) {
    const classes = useMyStyle();
    const fixedHeightPaperExp = clsx(classes.paper, classes.fixedHeightExp);
    const fixedHeightPaper = clsx(classes.paper, classes.fixedHeight);

    return (
        <React.Fragment>
            <div className={Lclasses.DashboardContainer}>
            <Grid container spacing={3}>
                <Grid item xs={12} lg={6} md={6}>
                    <Paper>
                        <FileExplorer parentContextFolder={props.parentContextFolder}/>
                    </Paper>
                </Grid>
                <Grid item xs={12} lg={6} md={6}>
                    <Paper className={fixedHeightPaper}>
                        <DisplayProblems grandparentstate={props.parentstate}/>
                    </Paper>
                </Grid>
            </Grid>
            </div>
        </React.Fragment>
    );
}
