import React, { Component } from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TablePagination from '@material-ui/core/TablePagination';
import { withStyles } from '@material-ui/core/styles';
import Spinner from '../../UI/Spinner'
import { withRouter } from 'react-router';

const StyledTableCell = withStyles((theme) => ({
    head: {
      backgroundColor: '#034f84',
      color: theme.palette.common.white
    },
    body: {
      fontSize: 14,
    },
    root: {
        textAlign: 'center'
    }
  }))(TableCell);

const StyledTableRow = withStyles((theme) => ({
  root: {
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.action.hover,
    }
  },
}))(TableRow);


class DisplayProblems extends Component {
 
  render() {
    const problems = this.props.grandparentstate.problemrepos;
    console.log(problems);

    let loadedTable = <Spinner />;
    if (this.props.grandparentstate.error) {
        loadedTable = <h1> Problems data can't be loaded </h1>;
    }
    if (this.props.grandparentstate.dataLoaded) {
        loadedTable = (
        <div>
        <React.Fragment>
          {/* <Toolbar /> */}
          <Table size="small" aria-label="dense table" style={{borderCollapse: 'separate', borderSpacing: '1px'}}>
            <TableHead>
              <StyledTableRow>
                <StyledTableCell >ProblemRepo</StyledTableCell>
                <StyledTableCell >Unannotated Files</StyledTableCell>
                <StyledTableCell >Annotated Files</StyledTableCell>
              </StyledTableRow>
            </TableHead>
            <TableBody>
              {problems.map((problem) => (
                <StyledTableRow
                  align="left"
                  key={problem.id}>
                  <StyledTableCell >{problem.name}</StyledTableCell>
                  <StyledTableCell >{problem.raw_files_count}</StyledTableCell>
                  <StyledTableCell >{problem.annotated_files_count}</StyledTableCell>
                </StyledTableRow>
              ))}
            </TableBody>
          </Table>
        </React.Fragment>
      </div>
        );
    }    
    return (
        <React.Fragment>
            {loadedTable}
        </React.Fragment>
    )
    }
};

export default DisplayProblems;