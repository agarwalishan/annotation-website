import React , { Component } from 'react';
import { withRouter } from 'react-router'
import classes from '../UI/Layout.module.css';
import { DropdownButton, Dropdown } from 'react-bootstrap';
import '../UI/tempcss.css';

class MasterSideOptionPanel extends Component {

  problemreposelect = (problemrepoid, problemreponame) => {
    this.props.history.push(this.props.match.url + '/' + problemrepoid + '/' + problemreponame);
  }

  render() {
    const problemrepos = this.props.parentstate.problemrepos;
    return (
      <React.Fragment>
        <div className={classes.sidenav}>
          <DropdownButton id="dropdown-basic-button" title="Go to ProblemRepo" size='sm' variant={"btn btn-outline-dark"}>
            {problemrepos.map(problemrepo => {
                return <Dropdown.Item key={problemrepo.id} onClick={() => this.problemreposelect(problemrepo.id, problemrepo.name)}>{problemrepo.name}</Dropdown.Item>
            })}
          </DropdownButton>
        </div>
      </React.Fragment>
    );
  }
}

export default withRouter(MasterSideOptionPanel);