import React, { Component } from 'react'
import Dashboard from './MasterDashboard/MasterDashboard';
import SideOptionPanel from './MasterSideOptionPanel';
import classes from '../UI/Layout.module.css';
import {withRouter} from 'react-router';
import axios from 'axios';
import AddProblemButton from '../AddProblemButton';
import { connect } from 'react-redux';
var path = require('path');

class MasterRepo extends Component {

  state = {
    problemrepos: [],
    dataLoaded: false,
    error: false
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (this.props.store_isnewproblemadded !== prevProps.store_isnewproblemadded) {
      axios.get('http://127.0.0.1:8000/api/master/' + this.props.match.params.mid + '/')
      .then(response => {
        this.setState({ problemrepos: response.data.problem });
        this.setState({dataLoaded: true});
      })
      .catch((err) => {
          this.setState({error: err});
      });  
    }
  }

  componentDidMount() {
    axios.get('http://127.0.0.1:8000/api/master/' + this.props.match.params.mid + '/')
      .then(response => {
        this.setState({ problemrepos: response.data.problem });
        this.setState({dataLoaded: true});
      })
      .catch((err) => {
          this.setState({error: err});
      });
  };


  render() {
    var parentContextFolder = path.join("/",this.props.match.params.mid ,'documents');
    return (
      <div className={classes.Pages}>
        <div className={classes.Subtoolbar}>
          <SideOptionPanel parentstate={this.state} />
          <div>
            <AddProblemButton />
          </div>
        </div>
        <div className={classes.Dashboard}>
            <Dashboard parentContextFolder={parentContextFolder} parentstate={this.state}/>
        </div>
      </div>
    );
  }
}; 

const mapStateToProps = state => {
  return {
    store_isnewproblemadded: state.isnewproblemadded
  }
};

export default connect(mapStateToProps)(withRouter(MasterRepo));
