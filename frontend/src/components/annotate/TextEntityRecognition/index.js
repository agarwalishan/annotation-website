import React, { useState } from "react"
import NLPAnnotator from "react-nlp-annotate/components/NLPAnnotator"
import axios from 'axios';
import { connect } from 'react-redux';
import * as actionTypes from '../../../store/actions'

const udt2OurFormat = (result) => {
  let paragraphs = result.document.split(/\r?\n/);

  //Fix for extra line because of split an extra line is introduced
  paragraphs.pop()
  //

  let paraOffsetStart = 0;
  let final_json = [];
  paragraphs.forEach(para => {
    let labels = [];
    let para_offset_end = paraOffsetStart + para.length;
    result.entities.forEach(entity => {

      if (entity['start'] >= paraOffsetStart && entity['end'] <= para_offset_end) {
        let label_start_with_para_offset = entity["start"] - paraOffsetStart;
        let label_end_with_para_offset = entity["end"] - paraOffsetStart;
        labels.push([label_start_with_para_offset, label_end_with_para_offset, entity["label"]]);

      }
    });

    paraOffsetStart += para.length + 1;
    let para_json = (
      {
        "text": para,
        "labels": labels
      });
    final_json.push(para_json);
  });
  let finalDownloadFileAsString = "";
  final_json.forEach(jsonElement => {
    finalDownloadFileAsString += JSON.stringify(jsonElement) + '\n';
  });
  return finalDownloadFileAsString;
}
const handleFileSaveAfterFinish = (content, filePath, isCompleted, isValidate, versionId, probId, AnnotationDone, ValidationDone) => {
  if (isValidate) {
    if (isCompleted) {
      console.log("File is validating")
      axios({
        method: 'post',
        url: 'http://127.0.0.1:8000/validate',
        data: {
          fileContent: content,
          filePath: filePath,
          versionId: versionId,
          probId : probId
        }
      })
      .then(() => {
        ValidationDone();
      });
    }
  }
  else {
    if (isCompleted) {
      if (filePath.includes("raw.json")) {
        axios({
          method: 'delete',
          url: 'http://127.0.0.1:8000/file',
          data: {
            filePath: filePath,
            probId : probId,
            doexport: false
          }
        });
      }
      var newFilename = filePath.replace("raw.json", "annotated.json");
      console.log("New file name is ",newFilename);
      axios({
        method: 'post',
        url: 'http://127.0.0.1:8000/file',
        data: {
          fileContent: content,
          filePath: newFilename,
          probId :probId,
          doexport: true
        }
      })
      .then(() => {
        AnnotationDone();
      });
    }
    else {

      axios({
        method: 'post',
        url: 'http://127.0.0.1:8000/file',
        data: {
          fileContent: content,
          filePath: filePath,
          probId :probId,
          doexport: false
        }
      })
      .then(() => {
        //
      });
    }
  }
}
const simpleSequenceToEntitySequence = (simpleSeq) => {
  const entSeq = []
  let charsPassed = 0
  for (const seq of simpleSeq) {
    if (seq.label) {
      entSeq.push({
        text: seq.text,
        label: seq.label,
        start: charsPassed,
        end: charsPassed + seq.text.length,
      })
    }
    charsPassed += seq.text.length
  }
  return entSeq
}

const entitySequenceToSimpleSeq = (doc, entSeq) => {
  if (!entSeq) return undefined
  const simpleSeq = []
  entSeq = [...entSeq]
  entSeq.sort((a, b) => a.start - b.start)
  let nextEntity = 0
  for (let i = 0; i < doc.length; i++) {
    if ((entSeq[nextEntity] || {}).start === i) {
      simpleSeq.push({
        text: entSeq[nextEntity].text,
        label: entSeq[nextEntity].label,
      })
      i = entSeq[nextEntity].end - 1
      nextEntity += 1
    } else {
      if (simpleSeq.length === 0 || simpleSeq[simpleSeq.length - 1].label) {
        simpleSeq.push({ text: doc[i] })
      } else {
        simpleSeq[simpleSeq.length - 1].text += doc[i]
      }
    }
  }
  return simpleSeq
}

const TextEntityRecognition = (props) => {
  const [currentSampleIndex, changeCurrentSampleIndex] = useState(0)
  const initialSequence = props.content.content.samples[currentSampleIndex].annotation
    ? entitySequenceToSimpleSeq(
      props.content.content.samples[currentSampleIndex].document,
      props.content.content.samples[currentSampleIndex].annotation.entities
    )
    : undefined
  return (
    <NLPAnnotator
      key={0}
      type="label-sequence"
      labels={props.content.content.interface.labels || props.content.content.interface.availableLabels}
      multipleLabels={false}
      initialSequence={initialSequence}
      document={props.content.content.samples[currentSampleIndex].document}
      onFinish={(result, isCompleted) => {
        var finalFileContent = udt2OurFormat({
          entities: simpleSequenceToEntitySequence(result),
          document: props.content.content.samples[currentSampleIndex].document
        });
        var filePath = props.content.filePath;
        var isValidate = props.validate;
        var versionId = props.content.versionId;
        var probId = props.content.probId;
        handleFileSaveAfterFinish(finalFileContent, filePath, isCompleted, isValidate, versionId, probId, props.onAnnotationDone, props.onValidationDone);
      }}
      dialogHandleClose={props.handleCloseDialog}
      isValidationTask={props.validate}
    />
  )
}

const mapDispatchToProps = dispatch => {
  return {
      onValidationDone: () => dispatch({type: actionTypes.VALIDATION}),
      onAnnotationDone: () => dispatch({type: actionTypes.ANNOTATION})
  }
};

export default connect(null, mapDispatchToProps)(TextEntityRecognition);