//Helper function to convert from our custo format to udt's format

const customToUdt = (content, uniqueLabels) => {

    var dataset;
    let sequences = content.split(/\r?\n/);
    let document = "";
    let documentOffset = 0;
    let annotaionEntities = [];
    try {


        sequences.forEach((sequence) => {
            sequence = JSON.parse(sequence);
            document = document + sequence.text + '\n';
            if (sequence.labels) {
                let labelsList = sequence.labels;
                labelsList.forEach(label => {
                    let start = label[0] + documentOffset;
                    let end = label[1] + documentOffset;
                    let text = document.slice(start, end);
                    let entity = {
                        "text": text,
                        "label": label[2],
                        "start": start,
                        "end": end
                    };
                    annotaionEntities.push(entity);
                });
                documentOffset += sequence.text.length + 1;
            }
        });

    }
    catch (e) {
        //console.log("Error while parsing bert format " + e);
    }
    let labels = [];
    uniqueLabels.forEach(label => {
        labels.push({
            "id": label,
            "displayName": label,
            "description": label
        });
    });

    let finalJson = {
        "interface": {
            "type": "text_entity_recognition",
            "overlapAllowed": false,
            "labels": labels
        },
        "samples": [{
            "document": document,
            "annotation": {
                "entities": annotaionEntities
            }
        }]
    }

    dataset = finalJson;
    return dataset;

};



export default customToUdt;