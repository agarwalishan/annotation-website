import React from 'react';
import { NavLink, withRouter } from 'react-router-dom';
import Routee from 'route-parser';
import classes from './UI/Layout.module.css';

const isFunction = value => typeof value === 'function';

const getPathTokens = pathname => {
  const paths = ['/'];

  if (pathname === '/') return paths;
  //const paths = [];
  pathname.split('/').reduce((prev, curr) => {
    const currPath = `${prev}/${curr}`;
    paths.push(currPath);
    return currPath;
  });

  return paths;
};

// it sees every new param and returns true if path contains it
function getRouteMatch(routes, path) {
  return Object.keys(routes)
    .map(key => {
      const params = new Routee(key).match(path);
      return {
        didMatch: params !== false,
        params,
        key
      };
    })
    .filter(item => item.didMatch)[0];
}

function getBreadcrumbs({ routes, match, location }) {
  //console.log(routes, match, location);
  const pathTokens = getPathTokens(location.pathname);
  return pathTokens.map((path, i) => {
    const routeMatch = getRouteMatch(routes, path);
    if (routeMatch) {
      const routeValue = routes[routeMatch.key];
      const name = isFunction(routeValue)
        ? routeValue(routeMatch.params)
        : routeValue;
      return { name, path };  
    }
    else {
      return null
    }
  })
  .filter(item => item!==null);
}

function Breadcrumbs({ routes, match, location }) {
  const breadcrumbs = getBreadcrumbs({ routes, match, location });

  return (
    <div>
      {/* Breadcrumb:{' '} */}
      {breadcrumbs.map((breadcrumb, i) =>
        <span key={breadcrumb.path}>
          {i < breadcrumbs.length - 1 ?
          <NavLink to={breadcrumb.path} className={classes.BreadName}>
            {breadcrumb.name}
          </NavLink>
          :
          <span className={classes.BreadName}>
            {breadcrumb.name}
          </span>
          }
          
          {i < breadcrumbs.length - 1 ? ' / ' : ''}
        </span>
      )}
    </div>
  );
}

export default withRouter(Breadcrumbs);
