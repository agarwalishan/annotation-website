import React, { Component } from 'react'
import Dashboard from './ModelDashboard/ModelDashboard';
import SideOptionPanel from './ModelSideOptionPanel';
import classes from '../UI/Layout.module.css';
import {withRouter} from 'react-router';
import axios from 'axios';
import AddVersionButton from '../AddVersionButton';
import { connect } from 'react-redux';
var path = require('path');

class ModelPage extends Component {

    state = {
        versions: [],
        dataLoaded: false,
        error: false
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.props.store_isnewversionadded !== prevProps.store_isnewversionadded
            || this.props.store_ispredictiondone !== prevProps.store_ispredictiondone
            || this.props.store_isvalidationdone !== prevProps.store_isvalidationdone
            || this.props.store_isscoringdone !== prevProps.store_isscoringdone) {
            axios.get('http://127.0.0.1:8000/api/model/' + this.props.match.params.moid + '/')
            .then(response => {
                this.setState({ versions: response.data.version });
                this.setState({dataLoaded: true});
            })
            .catch((err) => {
                this.setState({error: err});
            });
        }
      }

    componentDidMount () {
        axios.get('http://127.0.0.1:8000/api/model/' + this.props.match.params.moid + '/')
        .then(response => {
            this.setState({ versions: response.data.version });
            this.setState({dataLoaded: true});
        })
        .catch((err) => {
            this.setState({error: err});
        });
    }
  
    render() {
    console.log('[In modelpage]',this.props);
    var parentContextFolder = path.join("/",this.props.match.params.mid ,this.props.match.params.pid ,'documents');
    return (
        <div className={classes.Pages}>
        <div className={classes.Subtoolbar}>
          {/* <SideOptionPanel parentstate={this.state} /> */}
          <div>
            <AddVersionButton />
          </div>
        </div>
          <div className={classes.Dashboard}>
              <Dashboard 
                parentstate={this.state} 
                parentContextFolder={parentContextFolder} 
                probId={this.props.match.params.pid}
                />
          </div>
        </div>
    );
    }
}; 

const mapStateToProps = state => {
    return {
      store_isnewversionadded: state.isnewversionadded,
      store_ispredictiondone: state.ispredictiondone,
      store_isvalidationdone: state.isvalidationdone,
      store_isscoringdone: state.isscoringdone
    }
};
  
export default connect(mapStateToProps)(withRouter(ModelPage));