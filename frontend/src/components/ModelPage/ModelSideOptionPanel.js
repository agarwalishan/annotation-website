import React , {Component} from 'react';
import { withRouter } from 'react-router'
import classes from '../UI/Layout.module.css';
import { Button, DropdownButton, Dropdown } from 'react-bootstrap';

class ModelSideOptionPanel extends Component {

  versionselect = (versionid, versionname) => {
    // this.props.history.push(this.props.match.url + '/' + versionid + '/' + versionname);
  }

  render() {
    const versions = this.props.parentstate.versions;
    return (
      <React.Fragment>
      <div className={classes.sidenav}>
        <DropdownButton id="dropdown-basic-button" title="Go to Version" size='sm' variant={"btn btn-outline-dark"}>
          {versions.map(version => {
            return <Dropdown.Item key={version.id} onClick={() => this.versionselect(version.id, version.name)}>{version.name}</Dropdown.Item>
          })}
        </DropdownButton>
      </div>
    </React.Fragment>
    );
  }
}

export default withRouter(ModelSideOptionPanel);