import React from 'react';
import { useTheme } from '@material-ui/core/styles';
import { LineChart, Line, XAxis, YAxis, Label, ResponsiveContainer } from 'recharts';
import Title from './Title';
import { useState, useEffect } from 'react';
import { withRouter } from 'react-router';

// Generate Sales Data
function createData(versionname, f1_score) {
  return { versionname, f1_score };
}

const Chart = withRouter((props) => {
  const theme = useTheme();
  
  let data;

  data = props.grandparentstate.versions.map((version) => {
    return createData(version.name, version.f1_score)
  })

  return (
    <React.Fragment>
      <Title>F1_Score Chart</Title>
      <ResponsiveContainer>
        <LineChart
          data={data}
          margin={{
            top: 16,
            right: 16,
            bottom: 0,
            left: 24,
          }}
        >
          <XAxis dataKey="versionname" stroke={theme.palette.text.secondary} />
          <YAxis stroke={theme.palette.text.secondary}>
            <Label
              angle={270}
              position="left"
              style={{ textAnchor: 'middle', fill: theme.palette.text.primary }}
            >
              F1_Score (%)
            </Label>
          </YAxis>
          <Line type="monotone" dataKey="f1_score" stroke={theme.palette.primary.main} dot={false} />
        </LineChart>
      </ResponsiveContainer>
    </React.Fragment>
  );
});

export default Chart;