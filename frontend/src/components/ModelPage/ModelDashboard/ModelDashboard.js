import React from 'react'
import clsx from 'clsx'
import { useState } from 'react'
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import DisplayVersions from './DisplayVersions';
import Chart from './Chart';
import FileExplorer from '../../../components/FileExplorer';
import ScoringResults from './ModelShowVersionScores'
import Lclasses from '../../UI/Layout.module.css';
import useMyStyle from '../../UI/Dashboardstyles';
import TrainingResults from './ModelShowVersionTrainingResults';

export default function MasterDashboard(props) {
    const classes = useMyStyle();
    const fixedHeightPaper = clsx(classes.paper, classes.fixedHeight);
    const fixedHeightPaperExp = clsx(classes.paper, classes.fixedHeightExp);
    const fixedHeightPaperVersionScores = clsx(classes.paper);

    const [Scoreversionid, setscoreversionid] = useState(null);
    const [Trainversionid, settrainversionid] = useState(null)  
    
    const showVersionScores = (versionid) => {
        setscoreversionid(versionid);
    }
    const showVersionTrainingResults = (versionid) => {
        settrainversionid(versionid);
    }

    return (
        <React.Fragment>
            <div className={Lclasses.DashboardContainer}>
            <Grid container spacing={3}>
                    <Grid item xs={12} lg={6} md={6}>
                        {/* {FileExplorer} */}
                        <Paper className={fixedHeightPaperExp}>
                            <FileExplorer 
                                parentContextFolder={props.parentContextFolder} 
                                probId={props.probId} 
                                limitedCap={true} 
                            />
                        </Paper>
                    </Grid>
                    <Grid item xs={12} lg={6} md={6}>
                        <Grid container spacing={1}>
                            {/* {stats of models and versions} */}
                            <Grid item xs={12}>
                                <Paper className={fixedHeightPaper}>
                                    <DisplayVersions 
                                        showVersionScores={showVersionScores} 
                                        showVersionTrainingResults={showVersionTrainingResults}
                                        grandparentstate={props.parentstate}
                                    />
                                </Paper>
                            </Grid>
                            {/* {charts} */}
                            <Grid item xs={12}>
                                <Paper className={fixedHeightPaper}>
                                    <Chart grandparentstate={props.parentstate} />
                                </Paper>
                            </Grid>
                        </Grid>    
                    </Grid>
                    <Grid item xs={12} lg={12} md={6}>
                        {/* {Scores panel} */}
                        <ScoringResults 
                            versionId={Scoreversionid} 
                            fixedHeightPaperVersionScores={fixedHeightPaperVersionScores}
                        />
                    </Grid>
                    <Grid item xs={12} lg={12} md={6}>
                        {/* {Training Results panel} */}
                        <TrainingResults 
                            versionId={Trainversionid} 
                            fixedHeightPaperVersionScores={fixedHeightPaperVersionScores}
                        />
                    </Grid>
                </Grid>
            </div>
        </React.Fragment>
    );
}
