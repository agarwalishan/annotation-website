import React from 'react';
import Button from '@material-ui/core/Button';
//import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';

export default function ResourceNotAvailableDialog(props) {

  let dialogtype = (
    <div>
    <Dialog open={props.open} onClose={props.handleClose} aria-labelledby="form-dialog-title">
    <DialogContent>
      <DialogContentText>
    "One process is already running.<br/> 
    You can't do {props.message} at the moment. <br/>
    Please wait for previous process to finish"
      </DialogContentText>
      </DialogContent>
      <DialogActions>
          <Button onClick={props.handleClose} color="primary">
              Okay
          </Button>
      </DialogActions>
    </Dialog>
  </div>
  );
  return (
    <React.Fragment>
        {dialogtype}
    </React.Fragment>
  );
}