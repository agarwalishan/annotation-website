import React from 'react';
import Button from '@material-ui/core/Button';
//import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';

export default function ScoringDialog(props) {

  let dialogtype = (
    <div>
    <Dialog open={props.open} onClose={props.handleClose} aria-labelledby="form-dialog-title">
    <DialogContent>
      <DialogContentText>
          {props.scoring_response}
      </DialogContentText>
      </DialogContent>
      <DialogActions>
          <Button onClick={props.handleClose} color="primary">
              Okay
          </Button>
      </DialogActions>
    </Dialog>
  </div>
  );
  return (
    <React.Fragment>
        {dialogtype}
    </React.Fragment>
  );
}