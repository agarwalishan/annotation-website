import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';

export default function PredictionDialog(props) {

  let dialogtype = (
    <div>
    <Dialog open={props.open} onClose={props.handleClose}>
    <DialogContent>
      <DialogContentText>
          NO more files available to be predicted by selected version
      </DialogContentText>
      </DialogContent>
      <DialogActions>
          <Button onClick={props.handleClose} color="primary">
              Okay
          </Button>
      </DialogActions>
    </Dialog>
  </div>
  );

  if (props.available_raw > 0) {
    dialogtype = (
        <div>
        <Dialog open={props.open} onClose={props.handleClose}>
        <DialogContent>
                  <DialogContentText>
                      {props.available_raw} files are available that can be predicted with specified version.<br />
                      You can specify if you want to predict on less number of files.
                  </DialogContentText>
                  <TextField
                      autoFocus
                      margin="dense"
                      id="dialogtext"
                      label="Number of files to be predicted"
                      type="number"
                      defaultValue={props.available_raw}
                      onChange={props.handleInputCount}
                      max={props.available_raw}
                      fullWidth
                  />
              </DialogContent>
              <DialogActions>
                  <Button onClick={props.SubmitPredictionInput} color="primary">
                      Submit
                  </Button>
              </DialogActions>
        </Dialog>
      </div>
        );
    }

  return (
    <React.Fragment>
        {dialogtype}
    </React.Fragment>
  );
}