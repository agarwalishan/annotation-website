import React, { Component } from 'react';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import { withStyles} from '@material-ui/core/styles';

import axios from 'axios'

const StyledTableCell = withStyles((theme) => ({
    head: {
      backgroundColor: theme.palette.common.black,
      color: theme.palette.common.white,
    },
    body: {
      fontSize: 14,
    },
  }))(TableCell);

const StyledTableRow = withStyles((theme) => ({
  root: {
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.action.hover,
    },
  },
}))(TableRow);

export default class ShowVersionTrainingResults extends Component {
    state = {
        versionData: null,
        dataLoaded: false,
        error: false
    }
    
    componentDidUpdate (prevProps, prevState, snapshot) {
      //console.log("[In componenetdidupdate] with ",prevProps.versionId, this.props.versionId);
      if (this.props.versionId !== prevProps.versionId) {
        axios.get('http://127.0.0.1:8000/api/version/' + this.props.versionId + '/')
        .then(response => {
          //console.log(response.data)
          this.setState({ versionData: response.data});
          this.setState({dataLoaded: true})
          //console.log(this.state.versionData);
        })
        .catch((err) => {
            this.setState({error: err});
        });
      }
    }

    render () {
        let scoresData = null;
        if (this.props.versionId && this.state.dataLoaded) {
            let class_report_data = this.state.versionData.class_report.data    // data is array in which rows are stored
            let class_report_entity = this.state.versionData.class_report.index  // index is array stores name of labels

            scoresData = (
                <Paper className={this.props.fixedHeightPaperVersionScores}>
                    <div style={{textAlign: 'center', marginBottom: '4px'}}>
                      Training Results for {this.state.versionData.name}.
                    </div>
                    <div>
                    <Table size="small" aria-label="simple table" style={{height: "500px", width: "300px", float: "left"}}>
                        <TableHead>
                        <StyledTableRow>
                            <StyledTableCell align="right">Entity</StyledTableCell>
                            <StyledTableCell align="right">Precision</StyledTableCell>
                            <StyledTableCell align="right">Recall</StyledTableCell>
                            <StyledTableCell align="right">F1-score</StyledTableCell>
                            <StyledTableCell align="right">Support</StyledTableCell>
                        </StyledTableRow>
                        </TableHead>
                        <TableBody>
                        {class_report_data.map((data, index) => (
                        <StyledTableRow
                            align="left"
                            key={class_report_entity[index]}>
                            <StyledTableCell align="right" >{class_report_entity[index]}</StyledTableCell>
                            <StyledTableCell align="right">{data[0]}</StyledTableCell>
                            <StyledTableCell align="right">{data[1]}</StyledTableCell>
                            <StyledTableCell align="right">{data[2]}</StyledTableCell>
                            <StyledTableCell align="right">{data[3]}</StyledTableCell>
                        </StyledTableRow>
                        ))}
                        </TableBody>
                    </Table>
                    <img src={this.state.versionData.conf_matrix} height={650} width={680} style={{float: "left"}}/>
                    </div>
                </Paper>
            );
        }
        return (
            <React.Fragment>
                {scoresData}
            </React.Fragment>
        );
    }
}