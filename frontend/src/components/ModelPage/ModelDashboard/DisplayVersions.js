import React, { Component } from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TablePagination from '@material-ui/core/TablePagination';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import axios from 'axios';
import Button from '@material-ui/core/Button';
import Spinner from '../../UI/Spinner'
import { withRouter } from 'react-router';
import PredictionDialog from './PredictionDialog';
//import ScoringDialog from './ScoringDialog';
import customToUdt from './../../annotate/util'
import DiaologContainer from './../../annotate/DiaologContainer'
import { connect } from 'react-redux';
import * as actionTypes from '../../../store/actions';
import ResourceNotAvailableDialog from './ResourceNotAvailableDialog';
// import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles'
// import { red, blue, green } from '@material-ui/core/colors'

// const redTheme = createMuiTheme({ palette: { primary: green } })
//const blueTheme = createMuiTheme({ palette: { primary: blue } })

const StyledTableCell = withStyles((theme) => ({
    root: {
      padding: '2px 0px 2px 5px',
      textAlign: 'center'
    },
    head: {
      backgroundColor: '#034f84',
      color: theme.palette.common.white,
      maxWidth: '0px'
    },
    body: {
      fontSize: 14,
      padding: '5px 0px 5px 0px',
      textAlign: 'center',
      maxWidth: '0px'
    },
  }))(TableCell);

const StyledTableRow = withStyles((theme) => ({
  root: {
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.action.hover
    },
  }
}))(TableRow);

const StyledTableHead = withStyles((theme) => ({
  body: {
    padding: '2px 2px 2px 5px',
    textAlign: 'center'
  }
}))(TableHead);

const StyledButton = withStyles((theme) => ({
  root: {
    padding: '2px 0px 2px 0px',
    textAlign: 'center',
    fontSize: '0.7rem',
    textTransform: 'none'
  }
}))(Button);

// const useStyles = makeStyles({
//     table: {
//       maxWidth: 200,
//     },
//   });

//This is wrong method must change it later
function calculateLength (validated_files) {
  return validated_files["files"].length
}

class DisplayVersions extends Component {
  state = {
    // page: 0,
    // rowsPerPage: 3,
    //scoring_response: null,
    //openScoringdialog: false,
    openResourcenotavailabledialog: false,
    resourcedialogmessage: '',
    available_raw: 0,
    count_recieved_from_user: 0,
    openPredictiondialog: false,
    tempvid: null,
    openValidationdialog: false,
    fileData: {}
  }

  // handleChangeRowsPerPage = (event) => {
  //   this.setState({ rowsPerPage: parseInt(event.target.value) });
  //   this.setState({ page: 0 });
  // };

  // handleChangePage = (event, newPage) => {
  //   this.setState({ page: newPage });
  // };

  handleResourcenotavailableDialogClose = () => {
    this.setState({openResourcenotavailabledialog: false})
    //this.setState({resourcedialogmessage: ''})
  }

  handlePredictionDialogClose = () => {
    this.setState({openPredictiondialog: false});
  };

  handleInputCount = (e) => {
    this.setState({count_recieved_from_user: e.target.value});
  }

  SubmitPredictionInput = () => {
    // if (this.state.count_recieved_from_user > this.state.available_raw) {
    //   return
    // }
    this.handlePredictionDialogClose();
    let newpath = '?versionid=' + this.state.tempvid + '&count_raw=' + this.state.count_recieved_from_user;
    axios.get('http://127.0.0.1:8000/prediction/' + newpath)
    .then(response => {
      this.setState({tempvid: null})
      this.setState({count_recieved_from_user: 0})
      this.setState({available_raw: 0})
      this.props.onPredictionDone();
    })
    .catch(err => console.log(err));
  }

  callPrediction = (e, versionid) => {
    axios.get('http://127.0.0.1:8000/api/master/?user_id=' + localStorage.getItem('UserId'))
    .then(res => {
      console.log(res,res.data, res.data.isprocessrunning)
      if (res.data[0].isprocessrunning) {
        this.setState({resourcedialogmessage: 'Prediction'})
        this.setState({openResourcenotavailabledialog: true})
      }
      else {
        // tempvid for sending current versionid in SubmitPredictionInput
        this.setState({tempvid: versionid});
        let newpath = '?versionid='+ versionid;
        axios.get('http://127.0.0.1:8000/checkrawcount/' + newpath)
        .then(response => {
          this.setState({available_raw: response.data});
          this.setState({count_recieved_from_user: this.state.available_raw});
          this.setState({openPredictiondialog: true});
        });
      }
    })
  };
  
  // handleScoringDialogClose = () => {
  //   this.setState({openScoringdialog: false});
  //   this.setState({scoring_response: null});
  // };

  callScoring = (e, versionid) => {
    let newpath = '?versionid='+ versionid;
    axios.get('http://127.0.0.1:8000/scoring/' + newpath)
    .then(response => {
      //console.log(response);
      // this.setState({scoring_response: response.data['message']});
      // this.setState({openScoringdialog: true});
      this.props.onScoringDone();
    });
  };
  
  handleValidationDialogClose = () => {
    this.setState({openValidationdialog: false});
  };
  
  callValidate = (e, versionId) => {
    axios.get('http://localhost:8000/version/prediction/files/' + versionId)
    .then(response=>{
      if (Object.keys(response.data).length !== 0) {
        axios.get('http://127.0.0.1:8000/api/problem/' + this.props.match.params.pid + '/')
        .then(response2=>{
          var content ={};
          var uniqueLabels = response2.data.labels.names;
          content["content"] = customToUdt(response.data.content, uniqueLabels);
          content["filePath"] = response.data.filePath;
          content["versionId"] = versionId;
          content["probId"] = this.props.match.params.pid;
          this.setState({openValidationdialog: true, fileData : content})
          console.log(response);
        }); 
      }
    })
  }

  displayversionscore = (e, versiondata) => {
    if (versiondata.has_score) {
      this.props.showVersionScores(versiondata.id);
    }
    this.props.showVersionTrainingResults(versiondata.id);
  }

  render() {
    const versions = this.props.grandparentstate.versions;
    console.log(versions);
    // const rowsPerPage = this.state.rowsPerPage;
    // const page = this.state.page;

    // let dialogbox = null;
    let loadedTable = <Spinner />;
    if (this.props.grandparentstate.error) {
        loadedTable = <h1> Versions data can't be loaded </h1>;
    }
    if (this.props.grandparentstate.dataLoaded) {
        loadedTable = (
        <div>
        <React.Fragment>
          {/* <Toolbar /> */}
          <Table >
            <StyledTableHead >
              <StyledTableRow>
                <StyledTableCell >VersionId</StyledTableCell>
                <StyledTableCell >Training Data</StyledTableCell>
                <StyledTableCell >F1-score</StyledTableCell>
                <StyledTableCell >Validated Files</StyledTableCell>
                <StyledTableCell >Unvalidated Files</StyledTableCell>
                <StyledTableCell ></StyledTableCell>
                <StyledTableCell ></StyledTableCell>
                <StyledTableCell ></StyledTableCell>
              </StyledTableRow>
            </StyledTableHead>
            <TableBody>
              {versions.map((version) => (
                <StyledTableRow
                  align="left"
                  key={version.id}>
                  <StyledTableCell ><a style={{cursor: 'pointer'}} onClick={(e) => this.displayversionscore(e,version)}>{version.name}</a></StyledTableCell>
                  <StyledTableCell >{version.training_data}</StyledTableCell>
                  <StyledTableCell >{version.f1_score}</StyledTableCell>
                  <StyledTableCell >{calculateLength(version.validated_files)}</StyledTableCell>
                  <StyledTableCell >{calculateLength(version.total_predicted_files) - calculateLength(version.validated_files)}</StyledTableCell>
                  <StyledTableCell >
                    <StyledButton onClick={(e) => this.callPrediction(e, version.id)} variant="contained" color="secondary">
                      Predict
                    </StyledButton>
                  </StyledTableCell>
                  <StyledTableCell >
                    <StyledButton onClick={(e) => this.callValidate(e, version.id)} variant="contained" color="primary">
                     Validate
                    </StyledButton>
                    <DiaologContainer open={this.state.openValidationdialog} fileData={this.state.fileData} handleClose={this.handleValidationDialogClose} validate={true}/>
                  </StyledTableCell>
                  <StyledTableCell >
                    <StyledButton onClick={(e) => this.callScoring(e, version.id)} variant="contained" color="secondary">
                     Score
                    </StyledButton>
                  </StyledTableCell>
                </StyledTableRow>
              ))}
            </TableBody>
          </Table>
        </React.Fragment>
        {/* <TablePagination
          rowsPerPageOptions={[3]}
          component="div"
          count={versions.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onChangePage={this.handleChangePage}
          onChangeRowsPerPage={this.handleChangeRowsPerPage}
        /> */}
      </div>
        );
    }    
    return (
        <React.Fragment>
            {loadedTable}
            <PredictionDialog 
              open={this.state.openPredictiondialog} 
              handleClose={this.handlePredictionDialogClose} 
              available_raw={this.state.available_raw} 
              handleInputCount={(e) => this.handleInputCount(e)}
              SubmitPredictionInput={this.SubmitPredictionInput}
            />
            {/* <ScoringDialog 
              open={this.state.openScoringdialog} 
              handleClose={this.handleScoringDialogClose}
              scoring_response={this.state.scoring_response} 
            /> */}
            <ResourceNotAvailableDialog 
              open={this.state.openResourcenotavailabledialog} 
              handleClose={this.handleResourcenotavailableDialogClose}
              message={this.state.resourcedialogmessage} 
            />

        </React.Fragment>
    )
    }
};

const mapDispatchToProps = dispatch => {
  return {
      onPredictionDone: () => dispatch({type: actionTypes.PREDICTION}),
      onScoringDone: () => dispatch({type: actionTypes.SCORING}),
      //onValidationDone: () => dispatch({type: actionTypes.VALIDATION})
  }
};

export default connect(null, mapDispatchToProps)(withRouter(DisplayVersions));