import React, { Component } from 'react';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import { withStyles} from '@material-ui/core/styles';

import axios from 'axios'

const StyledTableCell = withStyles((theme) => ({
    head: {
      backgroundColor: theme.palette.common.black,
      color: theme.palette.common.white,
    },
    body: {
      fontSize: 14,
    },
  }))(TableCell);

const StyledTableRow = withStyles((theme) => ({
  root: {
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.action.hover,
    },
  },
}))(TableRow);

export default class ShowVersionScores extends Component {
    state = {
        versionData: null,
        dataLoaded: false,
        error: false
    }
    
    componentDidUpdate (prevProps, prevState, snapshot) {
      //console.log("[In componenetdidupdate] with ",prevProps.versionId, this.props.versionId);
      if (this.props.versionId !== prevProps.versionId) {
        axios.get('http://127.0.0.1:8000/api/version/' + this.props.versionId + '/')
        .then(response => {
          //console.log(response.data)
          this.setState({ versionData: response.data});
          this.setState({dataLoaded: true})
          //console.log(this.state.versionData);
        })
        .catch((err) => {
            this.setState({error: err});
        });
      }
    }

    render () {
        let scoresData = null;
        if (this.props.versionId && this.state.dataLoaded) {
            let class_report_data = this.state.versionData.score_class_report.data
            let class_report_entity = this.state.versionData.score_class_report.index
            let docs_used_for_scoring = this.state.versionData.docs_used_for_scoring

            scoresData = (
                <Paper className={this.props.fixedHeightPaperVersionScores}>
                    <div style={{textAlign: 'center', marginBottom: '4px'}}>
                      Scoring Results for {this.state.versionData.name} calculated on {docs_used_for_scoring} documents.
                    </div>
                    <div>
                    <Table size="small" aria-label="simple table" style={{height: "500px", width: "500px", float: "left"}}>
                        <TableHead>
                        <StyledTableRow>
                            <StyledTableCell align="right">Entity</StyledTableCell>
                            <StyledTableCell align="right">Precision</StyledTableCell>
                            <StyledTableCell align="right">Recall</StyledTableCell>
                            <StyledTableCell align="right">F1-score</StyledTableCell>
                            <StyledTableCell align="right">Support</StyledTableCell>
                        </StyledTableRow>
                        </TableHead>
                        <TableBody>
                        {class_report_data.map((data, index) => (
                        <StyledTableRow
                            align="left"
                            key={class_report_entity[index]}>
                            <StyledTableCell align="right" >{class_report_entity[index]}</StyledTableCell>
                            <StyledTableCell align="right">{data[0]}</StyledTableCell>
                            <StyledTableCell align="right">{data[1]}</StyledTableCell>
                            <StyledTableCell align="right">{data[2]}</StyledTableCell>
                            <StyledTableCell align="right">{data[3]}</StyledTableCell>
                        </StyledTableRow>
                        ))}
                        </TableBody>
                    </Table>
                    <img src={this.state.versionData.score_conf_matrix} height={650} width={680} />
                    </div>
                </Paper>
            );
        }
        return (
            <React.Fragment>
                {scoresData}
            </React.Fragment>
        );
    }
}