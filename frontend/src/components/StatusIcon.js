import React from 'react';
import { useState , useEffect } from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import axios from 'axios';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import Lclasses from './UI/Layout.module.css';
import CircularProgress from '@material-ui/core/CircularProgress';
import Tooltip from '@material-ui/core/Tooltip';
import NotificationsRoundedIcon from '@material-ui/icons/NotificationsRounded';
import Badge from '@material-ui/core/Badge';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    '& > * + *': {
      marginLeft: theme.spacing(2),
    },
  },
}));

const StyledTableCell = withStyles((theme) => ({
    head: {
      backgroundColor: theme.palette.common.black,
      color: theme.palette.common.white,
      textAlign: 'center'
    },
    body: {
      fontSize: 14,
    },
    root: {
        padding: '1px 20px 1px 10px'
    }
  }))(TableCell);

const StyledTableRow = withStyles((theme) => ({
  root: {
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.action.hover,
    },
  }
}))(TableRow);

const StyledDialogContent = withStyles((theme) => ({
    root: {
        '&:first-child': {
            paddingTop: '0px',
        },
        padding: '0px 0px 0px 0px',
    }
}))(DialogContent);

export default function StatusDialog(props) {
    const classes = useStyles();
    const [opendialog, setDialog] = useState(false) 
    const [statusdata, setStatus] = useState([])
    const [dataloaded, setDataloaded] = useState(false)
    const [progresscount, setProgresscount] = useState(0)
    
    let intervalId = null;

    const makeTable = (data) => {
        //tasktype, taskstatus, message, associated versionname (if prediction or scoring), modelname, problemname, masterrepo
        //console.log(data);
        data.sort((a, b) => {
            return b.processid - a.processid;
        });
        //console.log(data);
        let count = 0;
        for (var i=0; i < data.length; i++) {
            if (data[i].taskstatus === 'INPROGRESS') {
                count = count + 1;
            }
        }
        setProgresscount(count);
        return data
    }

    const loadData = () => {
        setDataloaded(false)
        setStatus([])
        //load current status
        axios.get('http://127.0.0.1:8000/api/status/?user_id=' + localStorage.getItem('UserId'))
        .then(response => {
            console.log(response);
            let processedData = makeTable(response.data);
            setStatus(processedData);
            setDataloaded(true);
            //setDialog(true);
        })
        .catch(err => {
            console.log(err,'current status data cant be fetched');
        })
    }

    useEffect(() => {
        //load current status
        loadData();
        intervalId = setInterval(loadData, 30000);
        return () => clearInterval(intervalId);
    }, []);

    let dialog = null;
    // if (dataloaded && statusdata.length == 0) {
    //     dialog = (
    //         <div>
    //             No actions performed yet
    //         </div>
    //     )
    // }
    if (dataloaded) {
        dialog = (
            <div>
                <Dialog 
                    open={opendialog} 
                    onClose={() => setDialog(false)}
                    fullWidth={true}
                    maxWidth = {'md'}
                    style={{height: '600px'}}>
                    <StyledDialogContent className={Lclasses.Statusdialog}>
                        <DialogContentText >
                        <Table >
                            <TableHead >
                            <StyledTableRow>
                                <StyledTableCell >Problem name</StyledTableCell>
                                <StyledTableCell >Model name</StyledTableCell>
                                <StyledTableCell >Version name</StyledTableCell>
                                <StyledTableCell >Task</StyledTableCell>
                                <StyledTableCell >Status</StyledTableCell>
                                <StyledTableCell >Result</StyledTableCell>
                            </StyledTableRow>
                            </TableHead>
                            <TableBody>
                            {statusdata.map((status) => (
                                <StyledTableRow
                                //align="left"
                                key={status.processid}>
                                <StyledTableCell >{status.problemname}</StyledTableCell>
                                <StyledTableCell >{status.modelname}</StyledTableCell>
                                <StyledTableCell >{status.versionname}</StyledTableCell>
                                <StyledTableCell >{status.tasktype}</StyledTableCell>
                                <StyledTableCell >
                                    {status.taskstatus === 'INPROGRESS' ? 
                                        <div className={classes.root}>
                                            <CircularProgress />
                                        </div>
                                    : 
                                    status.taskstatus === 'DONE' ? 
                                    <span style={{fontSize: '30px', color: 'green', marginLeft: '5px'}}>&#10003;</span> 
                                    :
                                    <span style={{fontSize: '25px', color: 'red', marginLeft: '5px'}}>&#10006;</span>}
                                </StyledTableCell>
                                <StyledTableCell >{status.message}</StyledTableCell>
                                </StyledTableRow>
                            ))}
                            </TableBody>
                        </Table>
                        </DialogContentText>
                    </StyledDialogContent>
                </Dialog>
            </div>
        );
    }
    
    return (
        <React.Fragment>
            <Tooltip title="View current status">
                <Badge color="secondary" badgeContent={progresscount}>
                    <NotificationsRoundedIcon fontSize='medium' onClick={() => setDialog(true)} style={{fill: 'honeydew'}}/>
                </Badge>
            </Tooltip>
            {dialog}
        </React.Fragment>
    );
}
