import React from 'react';
import { Link as RouterLink, useHistory } from 'react-router-dom';
import * as Yup from 'yup';
import { Formik, useFormik } from 'formik';
import axios from 'axios';
import {
    Box,
    Button,
    Checkbox,
    Container,
    FormHelperText,
    Link,
    TextField,
    Typography,
    makeStyles,
    Snackbar
} from '@material-ui/core';
import Alert from '@material-ui/lab/Alert';


const useStyles = makeStyles((theme) => ({
    root: {
        backgroundColor: theme.palette.background.dark,
        height: '100%',
        paddingBottom: theme.spacing(3),
        paddingTop: theme.spacing(3)
    }
}));

const validationSchema = Yup.object({
    email: Yup.string().email('Must be a valid email').max(255).required('Email is required'),
    username: Yup.string().max(255).required('First name is required'),
    password: Yup.string().max(255).required('password is required'),
    confirmPassword: Yup
        .string()
        .required()
        .label('Confirm password')
        .test('passwords-match', 'Passwords does not match', function (value) {
            return this.parent.password === value;
        })
});

const RegisterView = () => {
    const classes = useStyles();
    const [openSnackBar, setOpenSnackbar] = React.useState(false);
    const [snackBarMsg, setSnackBarMsg] = React.useState("");
    const [snackBarAlertType, setSnackBarType] = React.useState("success");
    const handleClickSnackBar = () => {
        setOpenSnackbar(true);
    };

    const handleCloseSnackBar = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }

        setOpenSnackbar(false);
    };
    const history = useHistory();

    const formik = useFormik({

        initialValues: {
            email: '',
            username: '',
            password: '',
            confirmPassword: ''
        },
        onSubmit: values => {
            console.log('Submmitting', values);

            axios.post('http://127.0.0.1:8000/users/create',
                {
                    user: {
                        username: values.username,
                        password: values.password,
                        email: values.email
                    }
                }).then(response => {
                    console.log(response)
                    if (response.status == 200) {
                        if (response.data.response == "error") {
                            var errorMsg = JSON.stringify(response.data.message);
                            setSnackBarMsg(errorMsg);
                            setSnackBarType("error");
                            handleClickSnackBar();
                        }
                        else {
                            const data = {
                                desc: "master" ,
                                user_id : response.data.userId
                            };
                              
                              const options = {
                                headers: {
                                    Authorization: 'JWT ' + response.data.token
                                }
                              };

                            axios.post('http://127.0.0.1:8000/api/master/',data,options
                                ).then(history.push('/login'))
                        }
                    }
                })

        },
        handleSubmit: () => {
            console.log("handle")
        },
        validationSchema

    });
    return (

        <Box
            display="flex"
            flexDirection="column"
            height="100%"
            justifyContent="center"
        >
            <Container maxWidth="sm">

                <form onSubmit={formik.handleSubmit}>
                    <Box mb={3}>
                        <Typography
                            color="textPrimary"
                            variant="h5"
                        >
                            Create new account
                  </Typography>
                    </Box>
                    <TextField
                        error={Boolean(formik.touched.username && formik.errors.username)}
                        fullWidth
                        helperText={formik.touched.username && formik.errors.username}
                        label="User name"
                        margin="normal"
                        name="username"
                        onBlur={formik.handleBlur}
                        onChange={formik.handleChange}
                        value={formik.values.username}
                        variant="outlined"
                    />
                    <TextField
                        error={Boolean(formik.touched.email && formik.errors.email)}
                        fullWidth
                        helperText={formik.touched.email && formik.errors.email}
                        label="Email Address"
                        margin="normal"
                        name="email"
                        onBlur={formik.handleBlur}
                        onChange={formik.handleChange}
                        type="email"
                        value={formik.values.email}
                        variant="outlined"
                    />
                    <TextField
                        error={Boolean(formik.touched.password && formik.errors.password)}
                        fullWidth
                        helperText={formik.touched.password && formik.errors.password}
                        label="Password"
                        margin="normal"
                        name="password"
                        onBlur={formik.handleBlur}
                        onChange={formik.handleChange}
                        type="password"
                        value={formik.values.password}
                        variant="outlined"
                    />
                    <TextField
                        error={Boolean(formik.touched.confirmPassword && formik.errors.confirmPassword)}
                        fullWidth
                        helperText={formik.touched.confirmPassword && formik.errors.confirmPassword}
                        label="Confirm Password"
                        margin="normal"
                        name="confirmPassword"
                        onBlur={formik.handleBlur}
                        onChange={formik.handleChange}
                        type="password"
                        value={formik.values.confirmPassword}
                        variant="outlined"
                    />
                    <Box
                        alignItems="center"
                        display="flex"
                        ml={-1}
                    >


                    </Box>

                    <Box my={2}>
                        <Button
                            color="primary"
                            //disabled={formik.isSubmitting}
                            fullWidth
                            size="large"
                            type="submit"
                            variant="contained"
                        >
                            Sign up now
                  </Button>
                    </Box>
                    <Typography
                        color="textSecondary"
                        variant="body1"
                    >
                        Have an account?
                  {' '}
                        <Link
                            component={RouterLink}
                            to="/login"
                            variant="h6"
                        >
                            Sign in
                  </Link>
                    </Typography>
                </form>
                <Snackbar open={openSnackBar} autoHideDuration={4000} onClose={handleCloseSnackBar}>
                    <Alert onClose={handleCloseSnackBar} severity={snackBarAlertType}>
                        {snackBarMsg}
                    </Alert>
                </Snackbar>
            </Container>
        </Box>
    );
};

export default RegisterView;