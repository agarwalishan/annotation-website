import React from 'react';
import { Link as RouterLink, useHistory} from 'react-router-dom';
import * as Yup from 'yup';
import { Formik, useFormik } from 'formik';
import {
  Box,
  Button,
  Container,
  Grid,
  Link,
  TextField,
  Typography,
  makeStyles
} from '@material-ui/core';
import axios from 'axios';
const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    height: '100%',
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3)
  }
}));

const LoginView = () => {
  const classes = useStyles();
  const history = useHistory();
  const formik = useFormik({

    initialValues: {
      username: '',
      password: ''
    },/* 
    validationSchema: {Yup.object().shape({
      email: Yup.string().email('Must be a valid email').max(255).required('Email is required'),
      password: Yup.string().max(255).required('Password is required')
    })}, */
    onSubmit: values => {
      console.log('Submmitting', values);
      axios.post('http://127.0.0.1:8000/api/token/',
        {
          username:values.username,
          password:values.password
        
      }).then(response=>{
        console.log(response)
        if(response.status==200){
          axios.get('http://127.0.0.1:8000/api/master/?user_id=' + response.data.user.id,{
            headers: {
              Authorization: 'JWT ' + response.data.token //the token is a variable which holds the token
            }
          }).then(
            response2 =>{
              localStorage.setItem('token', response.data.token);
              localStorage.setItem('loggedIn',true);
              localStorage.setItem('UserId', response.data.user.id);
              if (response2.data){
                //console.log(response2);
                var masterRepoId = response2.data[0].id;
                var redirectUrl = '/' + masterRepoId + '/master'
                localStorage.setItem('mid', masterRepoId);
                history.push(redirectUrl);
              }
            }
          )
        }
      })
      //navigate('/app/dashboard', { replace: true });

    },
    handleSubmit: () => {
      //console.log("handle")
    }

  });
  //const navigate = useNavigate();
  return (
    <Box
      display="flex"
      flexDirection="column"
      height="100%"
      justifyContent="center"
    >
      <Container maxWidth="sm">

        <form onSubmit={formik.handleSubmit}>
          <Box mb={3}>
            <Typography
              align="center"
              color="textSecondary"
              variant="body1"
            >
              Login with User Name
                  </Typography>
          </Box>
          <TextField
            error={Boolean(formik.touched.username && formik.errors.username)}
            fullWidth
            helperText={formik.touched.username && formik.errors.username}
            label="User Name"
            margin="normal"
            name="username"
            onBlur={formik.handleBlur}
            onChange={formik.handleChange}
            value={formik.values.username}
            variant="outlined"
          />
          <TextField
            error={Boolean(formik.touched.password && formik.errors.password)}
            fullWidth
            helperText={formik.touched.password && formik.errors.password}
            label="Password"
            margin="normal"
            name="password"
            onBlur={formik.handleBlur}
            onChange={formik.handleChange}
            type="password"
            value={formik.values.password}
            variant="outlined"
          />
          <Box my={2}>
            <Button
              color="primary"
              fullWidth
              size="large"
              type="submit"
              variant="contained"
            >
              Sign in now
                  </Button>
          </Box>
          <Typography
            color="textSecondary"
            variant="body1"
          >
            Don&apos;t have an account?
                  {' '}
            <Link
              component={RouterLink}
              to="/register"
              variant="h6"
            >
              Sign up
                  </Link>
          </Typography>
        </form>

      </Container>
    </Box>
  );
};

export default LoginView;