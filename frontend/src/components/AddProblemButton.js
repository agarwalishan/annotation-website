// To add new problem repo for current master repository
import React, { Component, useEffect } from 'react';
import AddIcon from '@material-ui/icons/Add';
import Fab from '@material-ui/core/Fab';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import TextField from '@material-ui/core/TextField';
import Tclasses from './UI/Toolbar.module.css'
import {useState} from 'react';
import axios from 'axios';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward';
import { makeStyles } from '@material-ui/core/styles';
import Tooltip from '@material-ui/core/Tooltip';
import { withRouter } from 'react-router';
import Lclasses from './UI/Layout.module.css';
import { connect } from 'react-redux';
import * as actionTypes from '../store/actions';

const useStyles = makeStyles((theme) => ({
  margin: {
    //margin: theme.spacing(1),
  },
  extendedIcon: {
    //marginRight: theme.spacing(1),
  },
}));

function AddProblemButton(props) {
    
    const classes = useStyles();
    const [opendialog, setdialog] = useState(false);
    const [errors, setError] = useState([{value: ''}]);
    const [labels, setLabel] = useState([{ value: null }]);
    const [newreponame, setNewreponame] = useState(null);
    const [newrepoerror, setNewrepoerror] = useState('');
    const [existingproblemrepos, setExistingproblemrepos] = useState([]);
    const [finalerrormessage, setFinalErrormessage] = useState(null);
    const [descriptions, setDescription] = useState([{ value: null }]);

    const postproblemdata = (problemdata) => {
            axios.post('http://127.0.0.1:8000/api/problem/', problemdata)
            .then(response => {
                console.log('problem added in both rdbms and upload', response);
                props.onProblemAdded();
            })
            .catch(err => {
                console.log(err, 'in making problem repo');
            })
    }

    const loadexistingproblemrepodata = () => {
        axios.get('http://127.0.0.1:8000/api/master/' + props.match.params.mid + '/')
        .then(response => {
            let existingproblemdata = response.data.problem;
            let existingproblemrepo = []
            for (var i=0; i < existingproblemdata.length; i++) {
                existingproblemrepo.push(existingproblemdata[i].name);
            }
            setExistingproblemrepos(existingproblemrepo);
            setNewrepoerror('');
            setNewreponame(null);
            setLabel([{ value: null }]);
            setError([{value: ''}]);
            setFinalErrormessage(null);
            setDescription([{ value: null }]);
            setdialog(true);
        })
    }

    const isproblemformvalid = () => {
        //let valid = true;
        setFinalErrormessage(null);
        // if no label entered
        if (labels.length == 0) {
            setFinalErrormessage('Enter atleast one label');
            return false;
        }
        // if reponame is not entered
        if (!newreponame || newreponame == '') {
            setFinalErrormessage('Problem repo name is must');
            return false;
        }
        // if newrepoerror value is not empty string
        if (newrepoerror.length > 0) {
            return false;
        }
        // if label error value is not empty string
        for (var i=0; i< errors.length; i++) {
            if (errors[i].value.length > 0) {
                return false;
            }
        }
        // if any label's value is null
        for (var i=0; i<labels.length; i++) {
            if (labels[i].value == null || labels[i].value == '') {
                setFinalErrormessage("Label name cant be empty");
                return false;        
            }
        }
        return true;
    }

    const handleSubmit = () => {
        if (!isproblemformvalid()) {
            return
        }
        setdialog(false);
        // make json data to send
        let problemdata = {};
        problemdata['name'] = newreponame
        let labelsData = {};
        let labelArray = [];
        let DescArray = [];
        console.log(labels, descriptions);
        for ( var i=0; i < labels.length ; i++) {
            labelArray.push(labels[i].value);
            if (descriptions[i].value == null) {
                DescArray.push("");
            }
            else {
                DescArray.push(descriptions[i].value);
            }
        }
        labelsData["names"] = labelArray;
        labelsData["descriptions"] = DescArray;
        problemdata["labels"] = JSON.stringify(labelsData);
        // this is important
        problemdata['master'] = parseInt(props.match.params.mid);
        console.log(problemdata); 
        postproblemdata(problemdata);
    }

    function ifreponameexists (newreponame) {
        for (var i=0; i < existingproblemrepos.length; i++) {
            if (existingproblemrepos[i] === newreponame) {
                return true;
            }
        }
        return false;
    }
    // to handle value change for problem name
    function handleNameChange (e) {
        let value = e.target.value;
        setNewreponame(value);
        var letterNumber = /^[0-9a-zA-Z]+$/;
        let repoerror = ''
        if (ifreponameexists(value)) {
            repoerror = 'Repo name already exists '
        }
        if (value!='' && !value.match(letterNumber)) {
            repoerror += 'Only Alphabets and numbers allowed'
        }
        setNewrepoerror(repoerror);
    }
    // to add new label and initialize it's error
    function handleAdd() {
        const values = [...labels];
        values.push({ value: null });
        setLabel(values);

        const error = [...errors];
        error.push({value: ''});
        setError(error);

        const desc = [...descriptions];
        desc.push({ value: null });
        setDescription(desc);
    }
    // to remove label and associated error
    function handleRemove(i) {
        const values = [...labels];
        values.splice(i, 1);
        setLabel(values);

        const error = [...errors];
        error.splice(i, 1);
        setError(error);

        const desc = [...descriptions];
        desc.splice(i, 1);
        setDescription(desc);
    }
    // to check if label name entered is unique or not
    function newvalueunique(values, index, newvalue) {
        let valid = true;
        for( var i=0; i < values.length; i++ ) {
            if (index != i && values[i].value === newvalue) {
                valid = false;
                break;
            }
        }
        return valid;
    }
    // to change value of label and simultaneously check for error
    function handleChange(i, event) {
        event.preventDefault();
        const values = [...labels];
        values[i].value = event.target.value;
        setLabel(values);

        const value = event.target.value;
        let error = [...errors];

        error[i].value = '';
        const fixed_error_message = 'Label name must be unique and only alphabets are allowed';
        var letters = /^[A-Za-z]+$/;
        if ((!value=='' && !value.match(letters)) || !newvalueunique(values, i, value)) {
            error[i].value = fixed_error_message;
        }

        setError(error);
    }

    function handleDescChange(i, event) {
        event.preventDefault();
        const desc = [...descriptions];
        desc[i].value = event.target.value;
        setDescription(desc);
    }

        let buttonstyles = {
            background: 'linear-gradient(45deg, #2196F3 30%, #21CBF3 90%)',
            borderRadius: 3,
            border: 0,
            color: 'white',
            height: 40,
            padding: '0px 5px 0px 5px',
            boxShadow: '0 3px 5px 2px rgba(33, 203, 243, .3)',
            marginBottom: '30px'
          }

        let addproblemdialog = (
            <div>
                <div onClick={loadexistingproblemrepodata} className={Lclasses.Addpagebutton}>
                {/* <span className={Lclasses.Subtoolbarplusicon}>+</span> Make new problemrepo */}
                    <div style={{float: 'left'}}>
                        <span className={Lclasses.Subtoolbarplusicon}>+</span>
                    </div> 
                    <div style={{float: 'left', marginLeft: '2px', marginTop: '2px'}}>
                        problemrepo
                    </div>
                </div>
                <Dialog open={opendialog} onClose={() => setdialog(false)} fullWidth={true}
                    maxWidth = {'sm'}>
                    <DialogContent className={Tclasses.Addproblemdialog}>
                        <DialogContentText>
                        <div style={{textAlign: 'left', marginBottom: '0px', fontSize: '15px', color: '#000000'}}>
                            Enter New Problem Repo Name
                        </div>
                        <TextField
                            autoFocus
                            margin="dense"
                            type="text"
                            placeholder="Enter name"
                            onChange={e => handleNameChange(e)}
                            // helperText={newrepoerror}
                            style={{textAlign: 'left', marginBottom: '20px'}}
                        />
                        <div style={{fontSize: '0.60em', marginTop: '-20px', color: 'red'}}>{newrepoerror}</div>
                        <div style={{textAlign: 'left', marginBottom: '20px', fontSize: '15px', color: '#000000',marginTop: '10px'}}>
                            Please Add Labels for Your New Problem Repo
                        </div>
                        <Button variant="contained" color="secondary" type="button" onClick={() => handleAdd()} style={buttonstyles}>
                            Add label
                        </Button>
                    {labels.map((label, idx) => {
                        return (
                        <div key={`${label}-${idx}`}>
                            <TextField
                                autoFocus
                                margin="dense"
                                type="text"
                                placeholder="Enter label name"
                                value={label.value || ""}
                                onChange={e => handleChange(idx, e)}
                                // helperText={errors[idx].value}
                            />

                            <TextField
                                autoFocus
                                margin="dense"
                                type="text"
                                placeholder="Enter Description"
                                //value={descriptions[idx].value || ""}
                                onChange={e => handleDescChange(idx, e)}
                                style={{marginLeft: '35px'}}
                            />
                        
                        <IconButton aria-label="delete" type="button" onClick={() => handleRemove(idx)} style={{marginLeft: '20px'}}>
                            <DeleteIcon fontSize="medium" />
                        </IconButton>
                        <div className={Tclasses.helperText}>{idx ? errors[idx].value: ''}</div>
                        </div>
                        );
                    })}
                        {/* <div style={{textAlign: 'left', marginTop: '20px', fontSize: '15px'}}>
                            {finalerrormessage}
                        </div> */}
                        </DialogContentText>
                    </DialogContent>
                <DialogActions>
                    <div style={{textAlign: 'left', marginRight: '5px', marginBottom: '1px', fontSize: '12px', color:'#FF0000'}}>
                            {finalerrormessage}
                    </div>
                  <Button onClick={handleSubmit} color="primary">
                      Submit
                  </Button>
              </DialogActions>
            </Dialog>
          </div>)
        return (
            <React.Fragment>
                {addproblemdialog}
            </React.Fragment>
        );
}

const mapDispatchToProps = dispatch => {
  return {
      onProblemAdded: () => dispatch({type: actionTypes.NEW_PROBLEM_ADDED})
  }
};

export default connect(null, mapDispatchToProps)(withRouter(AddProblemButton));