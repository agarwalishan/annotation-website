// To add new model for current problem repository
import React from 'react';
import AddIcon from '@material-ui/icons/Add';
import Fab from '@material-ui/core/Fab';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import TextField from '@material-ui/core/TextField';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import classes from './UI/Toolbar.module.css'
import {useState} from 'react';
import axios from 'axios';
import Tooltip from '@material-ui/core/Tooltip';
import {withRouter} from 'react-router'; 
import Lclasses from './UI/Layout.module.css';
import { connect } from 'react-redux';
import * as actionTypes from '../store/actions';

function AddModelButton(props) {

    const [CRF, setCRF] = useState("False");
    const [opendialog, setdialog] = useState(false);
    const [mystate, setState] = useState({
        max_seq_length: 512,
        train_batch_size: 6,
        learning_rate: 2e-5,
        num_train_epochs: 3,
        warmup_proportion: 0.1,
        trainingpercent: 80, 
        validationpercent: 5
    });
    const [errors, setError] = useState({
        max_seq_length: '',
        train_batch_size: '',
        learning_rate: '',
        num_train_epochs: '',
        warmup_proportion: '',
        trainingpercent: '', 
        validationpercent: ''
    })
    const [newmodelname, setNewmodelname] = useState(null);
    const [newmodelerror, setNewmodelerror] = useState('');
    const [existingmodels, setExistingmodels] = useState([]);
    const [finalerrormessage, setFinalErrormessage] = useState(null);
    
    const postmodeldata = (modeldata) => {
            axios.post('http://127.0.0.1:8000/api/model/', modeldata)
            .then(response => {
                console.log("new model added in both rdbms and upload", response);
                props.onModelAdded();
            })
            .catch(err => {
                console.log(err, 'in making new model');
            })
    }

    const loadexistingmodeldata = () => {
        axios.get('http://127.0.0.1:8000/api/problem/' + props.match.params.pid + '/')
        .then(response => {
            let existingmodeldata = response.data.model;
            let existingmodel = []
            for (var i=0; i < existingmodeldata.length; i++) {
                existingmodel.push(existingmodeldata[i].name);
            }
            console.log(existingmodel);
            setExistingmodels(existingmodel);
            setdialog(true);
            setNewmodelerror('');
            setNewmodelname(null);
            setFinalErrormessage(null);
            setState({
                max_seq_length: 512,
                train_batch_size: 6,
                learning_rate: 2e-5,
                num_train_epochs: 3,
                warmup_proportion: 0.1,
                trainingpercent: 80, 
                validationpercent: 5
            })
            setError({
                max_seq_length: '',
                train_batch_size: '',
                learning_rate: '',
                num_train_epochs: '',
                warmup_proportion: '',
                trainingpercent: '', 
                validationpercent: ''
            })
        })
    }

    const ismodelformvalid = () => {
        setFinalErrormessage(null);
        // if modelname is not entered
        if (!newmodelname || newmodelname === '') {
            setFinalErrormessage('Model name is must');
            return false;
        }
        // if newmodelerror value is not empty string
        if (newmodelerror.length > 0) {
            return false;
        }
        // if parameters error value is not empty string
        for (var error in errors) {
            if (errors[error].length > 0) {
                return false;
            }
          }
        return true;
    }

    const handlemodelSubmit = () => {
        if (!ismodelformvalid()) {
            return
        }
        setdialog(false);
        // make json data to send
        let modeldata = {};
        modeldata['name'] = newmodelname;
        let hyperparameters = {...mystate};
        hyperparameters['crf'] = CRF;
        modeldata['hyperparameters'] = JSON.stringify(hyperparameters);
        // this is important
        modeldata['problem'] = props.match.params.pid;
        //console.log(modeldata) 
        postmodeldata(modeldata);
    }

    function ifmodelnameexists (newmodelname) {
        for (var i=0; i < existingmodels.length; i++) {
            if (existingmodels[i] === newmodelname) {
                return true;
            }
        }
        return false;
    }
    // to handle value change for model name
    function handleNameChange (e) {
        let value = e.target.value;
        setNewmodelname(value);
        var letterNumber = /^[0-9a-zA-Z]+$/;
        let modelerror = ''
        if (ifmodelnameexists(value)) {
            modelerror = 'Model name already exists '
        }
        if (value!=='' && !value.match(letterNumber)) {
            modelerror += 'Only Alphabets and numbers allowed'
        }
        setNewmodelerror(modelerror);
    }

    const handlemodelchange = (event) => {
        event.preventDefault();
        const name = event.target.id;
        const value = parseFloat(event.target.value);
        let error = errors;
        let oldmystate = {...mystate};

        switch (name) {
        case 'max_seq_length': 
            error.max_seq_length = value ? !(value >=128 && value <=512) 
                ? 'Must be from 128 to 512!'
                : ''
                : '';
            break;
        case 'train_batch_size': 
            error.train_batch_size = value ? !(value >=2 && value <=32) 
            ? 'Must be from 2 to 32!'
            : ''
            : '';
            break;
        case 'learning_rate': 
            error.learning_rate = value ? !(value >=0 && value <=1) 
            ? 'Must be from 0 and 1!'
            : ''
            : '';
            break;
        case 'num_train_epochs': 
            error.num_train_epochs = value ? !(value >=1 && value <=10) 
            ? 'Must be from 1 to 10!'
            : ''
            : '';
            break;
        case 'warmup_proportion': 
            error.warmup_proportion = value ? !(value >=0 && value <=1) 
            ? 'Must be from 0 to 1!'
            : ''
            : '';
            break;
        case 'trainingpercent': 
            error.trainingpercent = value ? !(value >=20 && value <=90) 
            ? 'Must be from 20 to 90!'
            : ''
            : '';
            break;
        case 'validationpercent': 
            error.validationpercent = value ? !(value >=0 && value <=75) 
            ? 'Must be from 0 to 75!'
            : ''
            : '';
            break;
        default:
            break;
        }
        
        setError({...error});
        setState({...oldmystate, [name]: value});
      }

        let addmodeldialog = (
            <div >
                <div onClick={loadexistingmodeldata} className={Lclasses.Addpagebutton}>
                    <div style={{float: 'left'}}>
                        <span className={Lclasses.Subtoolbarplusicon}>+</span>
                    </div> 
                    <div style={{float: 'left', marginLeft: '2px', marginTop: '2px'}}>
                        Add new model
                    </div>
                </div>
            <Dialog open={opendialog} onClose={() => setdialog(false)}>
            <DialogContent className={classes.Addmodeldialog}>
              <DialogContentText>
                <div style={{textAlign: 'left', marginBottom: '0px', fontSize: '15px', color: '#000000'}}>
                    Enter New Model Name
                </div>
                <TextField
                        autoFocus
                        margin="dense"
                        type="text"
                        placeholder="Enter name"
                        onChange={e => handleNameChange(e)}
                        helperText={newmodelerror}
                        style={{textAlign: 'left', marginBottom: '20px'}}
                    />
                <div style={{textAlign: 'left', marginBottom: '20px', fontSize: '15px', color: '#000000'}}>
                    Please Specify Model Parameters
                </div>
                <TextField
                      autoFocus
                      margin="dense"
                      id="max_seq_length"
                      label="Maximum Seq length"
                      type="number"
                      defaultValue={512}
                      onChange={(e) => handlemodelchange(e)}
                      helperText={errors.max_seq_length}
                      style={{marginRight: '20px'}}
                  />
                  <TextField
                      margin="dense"
                      id="train_batch_size"
                      label="Batch Size"
                      type="number"
                      defaultValue={6}
                      onChange={(e) => handlemodelchange(e)}
                      helperText={errors.train_batch_size}
                  />
                  <TextField
                      margin="dense"
                      id="learning_rate"
                      label="Learning Rate"
                      type="number"
                      defaultValue={2e-5}
                      onChange={(e) => handlemodelchange(e)}
                      helperText={errors.learning_rate}
                      style={{marginRight: '20px'}}
                  />
                  <TextField
                      margin="dense"
                      id="num_train_epochs"
                      label="Training Epochs"
                      type="number"
                      defaultValue={3}
                      onChange={(e) => handlemodelchange(e)}
                      helperText={errors.num_train_epochs}
                  />
                  <TextField
                      margin="dense"
                      id="warmup_proportion"
                      label="Warmup-Proportion"
                      type="number"
                      defaultValue={0.1}
                      onChange={(e) => handlemodelchange(e)}
                      helperText={errors.warmup_proportion}
                  />
                  {/* <br></br><br /> */}
                  {/* <FormLabel component="legend">Output Layer</FormLabel>
                    <RadioGroup aria-label="crf" name="crf" value={CRF} onChange={(e) => setCRF(e.target.value)}>
                        <FormControlLabel value="True" control={<Radio />} label="Conditional Random Field" />
                        <FormControlLabel value="False" control={<Radio />} label="Softmax Layer" />
                    </RadioGroup> */}
                <TextField
                      margin="dense"
                      id="trainingpercent"
                      label="Percentage of data to be used for training"
                      type="number"
                      defaultValue={80}
                      onChange={(e) => handlemodelchange(e)}
                      helperText={errors.trainingpercent}
                      fullWidth
                  />
                <TextField
                      margin="dense"
                      id="validationpercent"
                      label="Percentage of data to be used for validation"
                      type="number"
                      defaultValue={5}
                      onChange={(e) => handlemodelchange(e)}
                      helperText={errors.validationpercent}
                      fullWidth
                  />
              </DialogContentText>
            </DialogContent>
              <DialogActions>
                <div style={{textAlign: 'left', marginRight: '5px', marginBottom: '1px', fontSize: '12px', color:'#FF0000'}}>
                    {finalerrormessage}
                </div>
                  <Button onClick={handlemodelSubmit} color="primary">
                      Submit
                  </Button>
              </DialogActions>
            </Dialog>
          </div>)
        return (
            <React.Fragment>
                {addmodeldialog}
            </React.Fragment>
        );
}

const mapDispatchToProps = dispatch => {
    return {
        onModelAdded: () => dispatch({type: actionTypes.NEW_MODEL_ADDED})
    }
};
  
export default connect(null, mapDispatchToProps)(withRouter(AddModelButton));