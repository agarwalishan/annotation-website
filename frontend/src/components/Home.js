import React, { Component } from 'react'
import { Switch, Route, Redirect, NavLink, Link, withRouter } from 'react-router-dom'
import Masterrepo from './MasterRepo/MasterRepo';
import ProblemRepo from './ProblemRepo/ProblemRepo'
import ModelPage from './ModelPage/ModelPage'
import ToolBar from './ToolBar';
import { routes } from './Routes';
import Breadcrumbs from './Breadcrumbs';
import classes from './UI/Layout.module.css';

import LoginView from './Auth/LoginView';
import RegisterView from './Auth/RegisterView';
import decode from 'jwt-decode';
import axios from 'axios';
import PrivateRoute from './Auth/PrivateRoute';

class Home extends Component {
    componentDidMount() {
        this.props.history.listen(() => {
            // Send user back if they try to navigate back
            if (localStorage.getItem('loggedIn') === 'true') {
                this.props.history.go(1);
            }
          });
      }

    render() {
        return (
            <React.Fragment>
                <ToolBar />
                <div className={classes.home}>
                    <div className={classes.Breadcrumbs}>
                        <Breadcrumbs routes={routes} />
                    </div>
                    <div>

                        <Switch>
                        <Route exact path='/' render={() => localStorage.getItem('loggedIn') === 'true'
                                 ? <Redirect to={'/' + localStorage.getItem('mid') + '/master'}/> 
                                 : <Redirect to="/login" />}/>
                        <Route path='/login' component={LoginView} />
                        <Route path='/register' component={RegisterView} />
                        <PrivateRoute authed={localStorage.getItem('loggedIn')} path='/:mid/:mastername' component={Masterrepo} exact/>
                        <PrivateRoute authed={localStorage.getItem('loggedIn')} path='/:mid/:mastername/:pid/:problemname' component={ProblemRepo} exact/>
                        <PrivateRoute authed={localStorage.getItem('loggedIn')} path='/:mid/:mastername/:pid/:problemname/:moid/:modelname' component={ModelPage} exact/>
                        {/* <Redirect from='/' to="/login" /> */}
                        </Switch>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}; 

export default withRouter(Home)