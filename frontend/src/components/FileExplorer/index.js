import React, { useEffect } from 'react';
import { FileManager, FileNavigator } from '@opuscapita/react-filemanager';
import connectorNodeV1 from '@opuscapita/react-filemanager-connector-node-v1';
import AnnotationEditor from './../annotate/DiaologContainer'
import customToUdt from './../annotate/util'
import axios from 'axios';
import { connect } from 'react-redux';

function FileExplorer(props) {
  const [open, setOpen] = React.useState(false);
  const [fileData, setFileData] = React.useState({});
  const handleClose = () => {
    setOpen(false);
  };

  const apiOptions = {
    ...connectorNodeV1.apiOptions,
    //Ishan change this to global config
    apiRoot: `http://localhost:3020`, // Or you local Server Node V1 installation.
    parentContextFolder: new Buffer(props.parentContextFolder).toString('base64'),
    annotate: function (file, content, filePath) {
      
      axios.get('http://127.0.0.1:8000/api/problem/' + props.probId + '/')
      .then(response => {
          var uniqueLabels = response.data.labels.names;
          content = customToUdt(content,uniqueLabels);
          var fileData = {
            content: content,
            filePath: filePath,
            probId:props.probId
          };
          setFileData(fileData);
          setOpen(file);
        })
        .catch((err) => {
          console.log(err);
        });

    }
  }

  return (
    <React.Fragment>
      <div>
        <main >
          <AnnotationEditor open={open} fileData={fileData} handleClose={handleClose} validate={false} />

          <div />
          <div style={{ height: '508px', width: '100%' }}>
            <FileManager>
              <FileNavigator
                id="filemanager-1"
                api={connectorNodeV1.api}
                apiOptions={apiOptions}
                capabilities={props.limitedCap?connectorNodeV1.limCapabilities:connectorNodeV1.capabilities}
                listViewLayout={connectorNodeV1.listViewLayout}
                viewLayoutOptions={connectorNodeV1.viewLayoutOptions}
                sample="sample"
              />
            </FileManager>
          </div>
        </main>
      </div>

    </React.Fragment>
  );
}


// have to re-render it on annotation, validation and on documents importing
const mapStateToProps = state => {
  return {
    store_isannotationdone: state.isannotationdone,
    store_isvalidationdone: state.isvalidationdone,
    store_isdocumentsimported: state.isdocumentsimported
  }
}

export default connect(mapStateToProps)(FileExplorer);