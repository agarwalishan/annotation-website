import React from 'react';

export const routes = {
    '/': '',
    '/:mid/:mastername': params => params.mastername,
    '/:mid/:mastername/:pid/:problemname': params => params.problemname,
    '/:mid/:mastername/:pid/:problemname/:moid/:modelname': params => params.modelname
};