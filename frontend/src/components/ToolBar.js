import React, { useEffect} from 'react';
import classes from './UI/Toolbar.module.css';
import StatusIcon from './StatusIcon';
import Button from '@material-ui/core/Button';
import { useHistory } from 'react-router-dom';

export default function Toolbar() {
  
    const history = useHistory();
    // const [loggedIn, setLoggedIn] = React.useState(false);
    
    // useEffect(() => {
    //   setLoggedIn(localStorage.getItem('loggedIn'));
    // });

    const handleClickLogout = () => {
      console.log("logging out of the system")
      localStorage.removeItem('token');
      localStorage.removeItem('loggedIn');
      localStorage.removeItem('UserId');
      localStorage.removeItem('mid');
      history.replace('/login');
    };
    return (
      <header className={classes.toolbar}>
        <nav className={classes.toolbar__navigation}>
          <div className={classes.toolbar__logo}>QuickAnnotator</div>
          <div className={classes.spacer} />
          {localStorage.getItem('loggedIn') === 'true'?
              <div className={classes.toolbar_navigation_items}>
                <div style={{margin: '0px 7px'}}>
                  <StatusIcon />
                </div>
                <Button variant="outline-dark" color="primary" onClick={handleClickLogout} style={{color: 'honeydew', border: '1px solid honeydew', padding: '3px 6px', fontSize: '12px'}}>
                  Log Out
              </Button>
              </div>
            : <div></div>}
        </nav>
      </header>
    );
  
}


