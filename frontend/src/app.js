import 'bootstrap/dist/css/bootstrap.min.css';
import 'react-checkbox-tree/lib/react-checkbox-tree.css';
import React, { Component } from 'react'
import { BrowserRouter, HashRouter } from 'react-router-dom'
import Home from './components/Home'
import 'font-awesome/css/font-awesome.min.css';
import './axios-middleware';
export class App extends Component {
    render() {
        return (
          <HashRouter>
            <Home />
          </HashRouter>
        )
    }
}

export default App;
