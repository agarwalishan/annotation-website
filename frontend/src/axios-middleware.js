import React from 'react';
import axios from 'axios';
import {BrowserHistory} from "react-router-dom";
import decode from 'jwt-decode';
// Add a request interceptor
axios.interceptors.request.use(function (config) {
  // Do something before request is sent
  if(localStorage.getItem('token')){
    var token = localStorage.getItem('token');
    config.headers.Authorization = 'JWT ' + token;
    const { exp } = decode(token);
    var date = new Date();
    var currentRequestTime = date.getTime()/1000;
    if(currentRequestTime > exp){
      localStorage.setItem('loggedIn',false);
      localStorage.removeItem('UserId');
      localStorage.removeItem('token');
      localStorage.removeItem('mid');
      window.location.href= '/';
    }
    }
    return config;
  }, function (error) {
    // Do something with request error
    return Promise.reject(error);
  });

// Add a response interceptor
axios.interceptors.response.use(function (response) {
    // Do something with response data
    return response;
  }, function (error) {
    // Do something with response error
    /* console.log("Response is", error.response)
    if(error.response.status===401){
      localStorage.setItem('loggedIn',false);
      localStorage.removeItem('token')
      //localStorage.rItem('')
    } */
    return Promise.reject(error);
  });

export default axios;