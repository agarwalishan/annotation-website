# nlp-tool
Active learning Nlp tool 



## Node File server 
Run :-
```
npm install 

node fileserver/server.js
```


## React Front-end
Run :-
`
npm install && npm run
`
## Django backend
Run :-
```
python manage.py runserver 
python manage.py makemigrations 
python manage.py migrate 
pythoon manage.py show_urls           #Lists all the Apis 
```

### Packaging steps

## backend packaging steps
# We are using pyinstaller, (no cross compile)
It requires .spec file in which we have to give path to the packages that pyinstaller could not pick by itself, these paths vary from system to system depending on where these packages are installed in system/pc.
So we have also added a .spec file in master branch which can be modified accordingly
This backendonefile.spec is made on office system ubuntu debian
Install (globally if possible)
```
pip install pyinstaller
```
if backendonefile.spec not present then run below command otherwise modify it
Note: importscript.py is added just to help pyinstaller know the modules/packages required, we will remove it later as it is of no need
Run :-
```
pyi-makespec --one-file backend/manage.py backend/importscript.py
```

after modifying .spec file

Run :-
```
pyinstaller backendonefile.spec
```
This will create a backend.exe in dist folder and a temporary folder named 'build'.
for now we have to migrate before running server for the first time.
To run server on linux
Run :-
```
cd dist
./backend runserver
```

## fileserver packaging steps
# We are using pkg, it can cross compile
Run :-
```
npm install -g pkg
```

For packaging js we need to modify package.json and in case of fileserver we have modified it 
To package using pkg 2 keys are necessary 'bin', 'main' in package.json
Their value is the 'index.js' i.e. the server

Run :-
```
pkg fileserver
```

We may specify target but if we do not specify then it makes 3 exe's for windows, linux and mac.
eg. fileserver-linux

To run fileserver
```
./fileserver-linux
```

##Note: we need a upload folder which should be kept parallel to fileserver and backend so to remove this constraint we are using environment variable named 'UPLOAD_PATH'. This should be set before running project when in production mode

## frontend packaging steps
# We are using electron-builder (can cross compile to some extent)
electron-builder is used to make destop app. It can only convert an electronjs project to app.
So we have have converted our reactjs to electronjs. It does not make much difference.
To convert reactjs to electronjs we have to just make a main.js file in which we have specified the path to index.html which is the starting point of our react app.
We have to also modify package.json and add some development dependencies in it.

(We have already followed this steps and they are all committed in master)
Steps for packaging:
-Make a main.js file in frontend/public (if not already created)
-Modify package.json

Then Run:-
```
npm run build
npm run electron-pack
```

This will create a dist folder which contains installable setup and an unpacked destop app which we can use for testing.

#Note: In frontend we are using HashRouter because BrowserRouter does not works with desktop app

### After packaging all these individually...
