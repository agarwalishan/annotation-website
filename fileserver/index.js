const path = require('path');

let config = {
  fsRoot: process.env.UPLOAD_PATH || path.join(__dirname,'..','upload'),
  rootName: 'Root folder',
  port: process.env.PORT || '3020',
  host: process.env.HOST || 'localhost'
};
//console.log("current directory is",process.env.UPLOAD_PATH);
let filemanager = require('@opuscapita/filemanager-server');
filemanager.server.run(config);
